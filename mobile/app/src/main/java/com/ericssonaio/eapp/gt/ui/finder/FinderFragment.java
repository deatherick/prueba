package com.ericssonaio.eapp.gt.ui.finder;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;
import com.google.android.material.textfield.TextInputEditText;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static android.widget.Toast.LENGTH_LONG;

public class FinderFragment extends Fragment {

    private FinderViewModel finderViewModel;
    ProgressDialog progressDialog;

    String url2 = "https://restapi.ericsson.net/iam/v1/people/SIGNUMERICSSON?format=json&givenName=NAMEERICSSON&sn=APELLIDOERICSSON&mail=CORREOERICSSON&listing=full&limit=1&offset=0&sort=eriGivenName";

    String urlSignum = "https://restapi.ericsson.net/iam/v1/people/SIGNUMERICSSON?format=json";
    String urlName = "https://restapi.ericsson.net/iam/v1/people/?format=json&givenName=NAMEERICSSON&listing=full&limit=1&offset=0&sort=eriGivenName";
    String urlApellido = "https://restapi.ericsson.net/iam/v1/people/?format=json&sn=APELLIDOERICSSON&listing=full&limit=1&offset=0&sort=eriGivenName";
    String ulrNombreCompleto = "https://restapi.ericsson.net/iam/v1/people/?format=json&givenName=NAMEERICSSON&sn=APELLIDOERICSSON&listing=full&limit=1&offset=0&sort=eriGivenName";
    String urlCorreo = "https://restapi.ericsson.net/iam/v1/people/?format=json&mail=CORREOERICSSON&listing=full&limit=1&offset=0&sort=eriGivenName";

    final String url = "https://login2.ericsson.net/login/login.fcc?TYPE=33554433&REALMOID=06-e5f06c28-8f45-420f-b46d-e4d8c8fc0fdc&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=WXQcX1g2i6wdFIhKlrlaS6ByBwfYT1qNnMuH2DyA1OYBRzZjgm5ra7Bhw6YGxcke&TARGET=$SM$http://restapi.ericsson.net/iam/v1/peopleFinder.html";
    final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";

    TextView textSignum, textNombre, textApellido, textPuesto, textCorreo, textPais;
    TextInputEditText inputSignum, inputName, inputLast, inputEmail;

    SharedPreferences prefs;
    static String signum, pass = null;
    Button search, clean;
    String cookie = null;
    static String firstName, lastName, mail, id, city, title;
    static String nameE, lastE, mailE, signumE;
    int resultSendRequest = 0;

    public static FinderFragment newInstance() {
        return new FinderFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
         finderViewModel =
                ViewModelProviders.of(this).get(FinderViewModel.class);
        View root = inflater.inflate(R.layout.fragment_finder, container, false);

        /*final TextView textView = root.findViewById(R.id.text_finder);
        finderViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("People Finder");
        progressDialog = new ProgressDialog(getContext());

        textSignum = (TextView) root.findViewById(R.id.text_signum_exit);
        textNombre = (TextView) root.findViewById(R.id.text_name_exit);
        textApellido = (TextView) root.findViewById(R.id.text_apellido_exit);
        textPuesto = (TextView) root.findViewById(R.id.text_puesto_exit);
        textCorreo = (TextView) root.findViewById(R.id.text_correo_exit);
        textPais = (TextView) root.findViewById(R.id.text_pais_exit);
        search = (Button) root.findViewById(R.id.btn_finder_people);
        clean = (Button) root.findViewById(R.id.btn_clean_people);

        inputSignum = (TextInputEditText ) root.findViewById(R.id.input_signum);
        inputName = (TextInputEditText ) root.findViewById(R.id.input_name);
        inputLast = (TextInputEditText ) root.findViewById(R.id.input_apellido);
        inputEmail = (TextInputEditText ) root.findViewById(R.id.input_correo);

        prefs = getContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        //sp= getContext().getSharedPreferences("profilePicture",MODE_PRIVATE);

        if(!prefs.getString("signume","").equals("")) {
            //byte[] data = Base64.decode(prefs.getString("signume", ""), Base64.DEFAULT);
            //signum = new String(data, StandardCharsets.UTF_8);
            signum = prefs.getString("signume", "");
        }else{signum="edejuli";}

        if(!prefs.getString("passe","").equals("")) {
            //byte[]data = Base64.decode(prefs.getString("passe", ""), Base64.DEFAULT);
            //pass = new String(data, StandardCharsets.UTF_8);
            pass = prefs.getString("passe", "");
        }else{pass="Usuario@123";}

        clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputEmail.getText().clear();
                inputLast.getText().clear();
                inputName.getText().clear();
                inputSignum.getText().clear();
                textSignum.setText("Id:");
                textNombre.setText("Nombre:");
                textApellido.setText("Apellido:");
                textPuesto.setText("Puesto:");
                textPais.setText("País:");
                textCorreo.setText("Correo:");
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (inputName.getText().toString().equals("") && inputLast.getText().toString().equals("")
                && inputSignum.getText().toString().equals("") && inputEmail.getText().toString().equals("")) {
                    AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
                    builderOK.setTitle("Información");
                    builderOK.setMessage("Debe ingresar información en al menos uno de los campos.");
                    // add the buttons
                    builderOK.setPositiveButton("Aceptar", null);
                    AlertDialog dialogOK = builderOK.create();
                    dialogOK.show();
                }else {
                    nameE = inputName.getText().toString();
                    lastE = inputLast.getText().toString();
                    signumE = inputSignum.getText().toString();
                    System.out.println(signumE);
                    mailE = inputEmail.getText().toString();

                    textSignum.setText("Id:");
                    textNombre.setText("Nombre:");
                    textApellido.setText("Apellido:");
                    textPuesto.setText("Puesto:");
                    textPais.setText("País:");
                    textCorreo.setText("Correo:");

                    ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                    if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {

                        PeopleFinder buscarInformacion = new PeopleFinder();
                        buscarInformacion.execute();
                    } else {
                        // No hay conexión a Internet en este momento
                        Toast.makeText(getContext(), "Revisa tú conexión a internet e intenta nuevamente.", LENGTH_LONG).show();
                    }
                }
            }
        });

        return root;
    }

    private class PeopleFinder extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialogWithTitle("Información","Buscando datos...");
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected Void doInBackground(Void... voids) {
            resultSendRequest = 0;
            try {
                Document document = Jsoup.connect(url).get();

                Connection.Response loginPageResponse =
                        Jsoup.connect(url)
                                .referrer(url)
                                .userAgent("Mozilla/5.0")
                                .timeout(10 * 1000)
                                .followRedirects(true)
                                .execute();

                Map<String, String> mapLoginPageCookies = loginPageResponse.cookies();

                Connection.Response loginResponse = Jsoup.connect(url)
                        .userAgent(USER_AGENT)
                        .data("USER", signum)
                        .data("PASSWORD", pass)
                        .method(Connection.Method.POST)
                        .execute();

                Map<String, String> mapLoginPageCookies2 = loginResponse.cookies();

                //System.out.println("cookies: " + mapLoginPageCookies2.toString());
                //System.out.println("SMSESSION: " + loginResponse.cookie("SMSESSION"));

                cookie = loginResponse.cookie("SMSESSION");

                //System.out.println("Consulta datos...");

                String urlSearch = url2.replace("SIGNUMERICSSON", signumE);
                urlSearch = urlSearch.replace("NAMEERICSSON", nameE);
                urlSearch = urlSearch.replace("APELLIDOERICSSON", lastE);
                urlSearch = urlSearch.replace("CORREOERICSSON", mailE);

                System.out.println(urlSearch);

                Document doc = Jsoup.connect(urlSearch)
                        .ignoreContentType(true)
                        .cookies(mapLoginPageCookies2)
                        .get();

                //System.out.println(doc.outerHtml());

                //String data = doc.select("body").first().toString();

                //System.out.println(Jsoup.parse(doc.outerHtml(), "ISO-8859-1").select("body").text());

                JSONParser parse = new JSONParser();

                Object obj = parse.parse(Jsoup.parse(doc.outerHtml(), "ISO-8859-1").select("body").text());

                JSONArray employeeList = new JSONArray();
                employeeList.add(obj);
                //System.out.println(employeeList);

                if (employeeList.size()>0) {

                    employeeList.forEach(emp -> {
                        parseEmployeeObject((JSONObject) emp);
                    });
                }else{
                    resultSendRequest = 1;
                }
            } catch (IOException | ParseException e ) {
                e.printStackTrace();
                resultSendRequest = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgressDialogWithTitle();

            if (resultSendRequest == 0 && firstName != "") {

                inputEmail.getText().clear();
                inputLast.getText().clear();
                inputName.getText().clear();
                inputSignum.getText().clear();

                textNombre.setText("Nombre: " + firstName);
                textApellido.setText("Apellido: " + lastName);
                textCorreo.setText("Correo: " + mail);
                textSignum.setText("Id: " + id);
                textPais.setText("País: " + city);
                textPuesto.setText("Puesto: " + title);

                Toast.makeText(getContext(), "Busqueda realizada con éxito.", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
                builderOK.setTitle("Información");
                builderOK.setMessage("Busqueda realizada con éxito.");
                // add the buttons
                builderOK.setPositiveButton("Aceptar", null);
                AlertDialog dialogOK = builderOK.create();
                dialogOK.show();
            }else if (resultSendRequest == 1){
                textSignum.setText("Id:");
                textNombre.setText("Nombre:");
                textApellido.setText("Apellido:");
                textPuesto.setText("Puesto:");
                textPais.setText("País:");
                textCorreo.setText("Correo:");

                inputEmail.getText().clear();
                inputLast.getText().clear();
                inputName.getText().clear();
                inputSignum.getText().clear();

                Toast.makeText(getContext(), "No se encontro información.", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
                builderOK.setTitle("Información");
                builderOK.setMessage("No se encontro información de la persona solicitada.");
                // add the buttons
                builderOK.setPositiveButton("Aceptar", null);
                AlertDialog dialogOK = builderOK.create();
                dialogOK.show();
            } else {
                textSignum.setText("Id:");
                textNombre.setText("Nombre:");
                textApellido.setText("Apellido:");
                textPuesto.setText("Puesto:");
                textPais.setText("País:");
                textCorreo.setText("Correo:");

                inputEmail.getText().clear();
                inputLast.getText().clear();
                inputName.getText().clear();
                inputSignum.getText().clear();

                /*Toast.makeText(getContext(), "Error en la busqueda.", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderError = new AlertDialog.Builder(getContext());
                builderError.setTitle("Error en la busqueda.");
                builderError.setMessage("La busqueda presento problemas. Intente de nuevo.");
                // add the buttons
                builderError.setPositiveButton("Aceptar", null);
                AlertDialog dialogError = builderError.create();
                dialogError.show();*/
                Toast.makeText(getContext(), "No se encontro información.", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
                builderOK.setTitle("Información");
                builderOK.setMessage("No se encontro información de la persona solicitada.");
                // add the buttons
                builderOK.setPositiveButton("Aceptar", null);
                AlertDialog dialogOK = builderOK.create();
                dialogOK.show();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            hideProgressDialogWithTitle();
            resultSendRequest = 1;

            textSignum.setText("Id:");
            textNombre.setText("Nombre:");
            textApellido.setText("Apellido:");
            textPuesto.setText("Puesto:");
            textPais.setText("País:");
            textCorreo.setText("Correo:");

            inputEmail.getText().clear();
            inputLast.getText().clear();
            inputName.getText().clear();
            inputSignum.getText().clear();
        }
    }

    private static void parseEmployeeObject(JSONObject employee)
    {
        //Get employee object within list
        System.out.println("employee: " + employee.size());

        System.out.println("employee: " + employee);

        if (employee.size() == 1) {

            JSONObject employeeObject = (JSONObject) employee.get("result");
            System.out.println("employeeObject: " + employeeObject);

            if (employeeObject != null){
                //Get employee first name
                firstName = (String) employeeObject.get("eriGivenName");
                System.out.println(firstName);

                //Get employee last name
                lastName = (String) employeeObject.get("eriSn");
                System.out.println(lastName);

                //Get employee website name
                mail = (String) employeeObject.get("mail");
                System.out.println(mail);

                city = (String) employeeObject.get("eriCountry");
                System.out.println(city);

                title = (String) employeeObject.get("title");
                System.out.println(title);

                id = (String) employeeObject.get("employeeNumber");
                System.out.println(id);
            }else{
                firstName = "";
                //Get employee last name
                lastName = "";
                //Get employee website name
                mail = "";
                city = "";
                title = "";
                id = "";
            }
        }else{
            JSONArray arr = (JSONArray) employee.get("result");
            System.out.println("arr: " + arr);
            if (arr != null && !arr.isEmpty()) {
                for (Object o : arr) {
                    System.out.println(o);
                    convertRequest((JSONObject) o);
                }
            }else{
                firstName = "";
                //Get employee last name
                lastName = "";
                //Get employee website name
                mail = "";
                city = "";
                title = "";
                id = "";
            }
        }
    }

    private static void convertRequest(JSONObject obj){
        System.out.println("obj: " + obj);

        if (obj != null) {
            //Get employee first name
            firstName = (String) obj.get("eriGivenName");
            System.out.println(firstName);

            //Get employee last name
            lastName = (String) obj.get("eriSn");
            System.out.println(lastName);

            //Get employee website name
            mail = (String) obj.get("mail");
            System.out.println(mail);

            city = (String) obj.get("eriCountry");
            System.out.println(city);

            title = (String) obj.get("title");
            System.out.println(title);

            id = (String) obj.get("employeeNumber");
            System.out.println(id);
        }else{
            firstName = "";

            //Get employee last name
            lastName = "";

            //Get employee website name
            mail = "";

            city = "";

            title = "";

            id = "";
        }
    }

    private void showProgressDialogWithTitle(String title,String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }
}
