package com.ericssonaio.eapp.gt.ui.newfinder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ericssonaio.eapp.gt.R;

import java.util.ArrayList;

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.PeopleViewHolder> {

    private Context mContext;
    private ArrayList<PeopleItem> mPeopleList;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }

    public PeopleAdapter(Context context, ArrayList<PeopleItem> peopleList){
        mContext = context;
        mPeopleList = peopleList;
    }

    @NonNull
    @Override
    public PeopleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.people_item, parent, false);
        return new PeopleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PeopleViewHolder holder, int position) {
        PeopleItem peopleItem = mPeopleList.get(position);

        String peopleSignum = peopleItem.getPeopleSignum();
        String peopleName = peopleItem.getPeopleName();
        String peopleLastName = peopleItem.getPeopleLastName();
        String peopleEmail = peopleItem.getPeopleEmail();
        String peopleJob = peopleItem.getPeopleJob();
        String peopleCountry = peopleItem.getPeopleCountry();
        int peopleImg = peopleItem.getImgPeople();

        holder.mPeopleSignum.setText(peopleSignum);
        holder.mPeopleName.setText(peopleName);
        holder.mPeopleLastName.setText(peopleLastName);
        holder.mPeopleJob.setText(peopleJob);
        holder.mPeopleEmail.setText(peopleEmail);
        holder.mPeopleCountry.setText(peopleCountry);
        holder.mPeopleImg.setImageResource(peopleImg);
    }

    @Override
    public int getItemCount() {
        return mPeopleList.size();
    }

    public class PeopleViewHolder extends RecyclerView.ViewHolder{

        public TextView mPeopleSignum;
        public TextView mPeopleName;
        public TextView mPeopleLastName;
        public TextView mPeopleJob;
        public TextView mPeopleEmail;
        public TextView mPeopleCountry;
        public ImageView mPeopleImg;

        public PeopleViewHolder(@NonNull View itemView) {
            super(itemView);
            mPeopleSignum = itemView.findViewById(R.id.people_item_text_signum);
            mPeopleName = itemView.findViewById(R.id.people_item_text_name);
            mPeopleLastName = itemView.findViewById(R.id.people_item_text_last_name);
            mPeopleJob = itemView.findViewById(R.id.people_item_text_job);
            mPeopleEmail = itemView.findViewById(R.id.people_item_text_email);
            mPeopleCountry = itemView.findViewById(R.id.people_item_text_country);
            mPeopleImg = itemView.findViewById(R.id.img_people_list_flag);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            onItemClickListener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
