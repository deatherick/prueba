package com.ericssonaio.eapp.gt.ui.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;

import static android.content.Context.MODE_PRIVATE;

public class SettingsFragment extends Fragment {

    private SettingsViewModel settingsViewModel;
    Switch switch_change;
    SharedPreferences prefs;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        settingsViewModel =
                ViewModelProviders.of(this).get(SettingsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        /*final TextView textView = root.findViewById(R.id.text_settings);
        settingsViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("");

        prefs = getContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        switch_change = (Switch) root.findViewById(R.id.switch_day_nigth);
        switch_change.setChecked(prefs.getBoolean("isCheckedNightMode", false));

        if(switch_change.isChecked()){
            switch_change.setText("Oscuro ");
        }else{
            switch_change.setText("Claro ");
        }

        Log.d("isCheckedNightMode", "isCheckedNightMode: " + prefs.getBoolean("isCheckedNightMode", false));

        switch_change.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    startActivity(new Intent(getContext(), MainActivity.class));
                    SharedPreferences.Editor editor = prefs.edit();
                    switch_change.setText(getString(R.string.msg_setting_light));
                    editor.putBoolean("isCheckedNightMode", switch_change.isChecked());
                    editor.commit();

                    //getActivity().finish();
                }else{
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    startActivity(new Intent(getContext(), MainActivity.class));
                    SharedPreferences.Editor editor = prefs.edit();
                    switch_change.setText(getString(R.string.msg_setting_dark));
                    editor.putBoolean("isCheckedNightMode", switch_change.isChecked());
                    editor.commit();

                    //getActivity().finish();
                }
            }
        });

        return root;
    }
}