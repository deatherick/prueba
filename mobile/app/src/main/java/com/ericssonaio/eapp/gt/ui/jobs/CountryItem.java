package com.ericssonaio.eapp.gt.ui.jobs;

public class CountryItem {
    private String countryName;

    public CountryItem(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
