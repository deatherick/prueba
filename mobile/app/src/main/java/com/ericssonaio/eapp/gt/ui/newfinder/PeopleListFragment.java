package com.ericssonaio.eapp.gt.ui.newfinder;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;
import com.ericssonaio.eapp.gt.ui.jobs.CountryItemFlagResource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


public class PeopleListFragment extends Fragment implements PeopleAdapter.OnItemClickListener {

    ProgressDialog progressDialog;
    private PeopleListViewModel peopleListViewModel;
    CountryItemFlagResource countryItemFlagResource;

    private RecyclerView recyclerView;
    private PeopleAdapter peopleAdapter;
    private ArrayList<PeopleItem> peopleItemArrayList;
    private RequestQueue requestQueue;

    final String url = "https://login2.ericsson.net/login/login.fcc?TYPE=33554433&REALMOID=06-e5f06c28-8f45-420f-b46d-e4d8c8fc0fdc&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=WXQcX1g2i6wdFIhKlrlaS6ByBwfYT1qNnMuH2DyA1OYBRzZjgm5ra7Bhw6YGxcke&TARGET=$SM$http://restapi.ericsson.net/iam/v1/peopleFinder.html";
    String url2 = "https://restapi.ericsson.net/iam/v1/people/SIGNUMERICSSON?format=json&givenName=NAMEERICSSON&sn=APELLIDOERICSSON&mail=CORREOERICSSON&listing=full&limit=60&offset=0&sort=eriGivenName";

    final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
    SharedPreferences prefs;
    static String signum, pass, cookie = null;
    public static String EXTRA_UID_APPLY = "uid";
    String resultSearch;

    public static PeopleListFragment newInstance() {
        return new PeopleListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        peopleListViewModel =
                ViewModelProviders.of(this).get(PeopleListViewModel.class);
        View root = inflater.inflate(R.layout.fragment_people_list, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("");
        countryItemFlagResource = new CountryItemFlagResource();

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        prefs = getContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        //sp= getContext().getSharedPreferences("profilePicture",MODE_PRIVATE);

        if (!prefs.getString("signume", "").equals("")) {
            //byte[] data = Base64.decode(prefs.getString("signume", ""), Base64.DEFAULT);
            //signum = new String(data, StandardCharsets.UTF_8);
            signum = prefs.getString("signume", "");
        } else {
            signum = "edejuli";
        }

        if (!prefs.getString("passe", "").equals("")) {
            //byte[]data = Base64.decode(prefs.getString("passe", ""), Base64.DEFAULT);
            //pass = new String(data, StandardCharsets.UTF_8);
            pass = prefs.getString("passe", "");
        } else {
            pass = "Usuario@123";
        }

        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view_people);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        peopleItemArrayList = new ArrayList<>();

        requestQueue = Volley.newRequestQueue(getContext());

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            parseJSON(bundle.getString("name"), bundle.getString("last_name"),
                    bundle.getString("signum"), bundle.getString("email"));
        }
        return root;
    }

    public void parseJSON(String nameBundle, String lastNameBundle, String signumBundle, String emailBundle) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Document document = Jsoup.connect(url).get();

                    Connection.Response loginPageResponse =
                            Jsoup.connect(url)
                                    .referrer(url)
                                    .userAgent("Mozilla/5.0")
                                    .timeout(10 * 1000)
                                    .followRedirects(true)
                                    .execute();

                    Map<String, String> mapLoginPageCookies = loginPageResponse.cookies();

                    Connection.Response loginResponse = Jsoup.connect(url)
                            .userAgent(USER_AGENT)
                            .data("USER", signum)
                            .data("PASSWORD", pass)
                            .method(Connection.Method.POST)
                            .execute();

                    Map<String, String> mapLoginPageCookies2 = loginResponse.cookies();

                    cookie = loginResponse.cookie("SMSESSION");

                    String urlSearch = url2.replace("SIGNUMERICSSON", signumBundle);
                    urlSearch = urlSearch.replace("NAMEERICSSON", nameBundle);
                    urlSearch = urlSearch.replace("APELLIDOERICSSON", lastNameBundle);
                    urlSearch = urlSearch.replace("CORREOERICSSON", emailBundle);

                    System.out.println("URL search: " + urlSearch);

                    Document doc = Jsoup.connect(urlSearch)
                            .ignoreContentType(true)
                            .cookies(mapLoginPageCookies2)
                            .get();

                    JSONParser parse = new JSONParser();

                    Object obj = parse.parse(Jsoup.parse(doc.outerHtml(), "ISO-8859-1").select("body").text());

                    JSONObject jsonObject = new JSONObject(obj.toString());
                    System.out.println("x-jsonObject: " + jsonObject);

                    if(jsonObject.has("result") && !jsonObject.isNull("result")) {
                        if (jsonObject.length() == 1) {
                            resultSearch = String.valueOf(jsonObject.length());
                            JSONObject jsonObjectJob = jsonObject.getJSONObject("result");
                            fillPeopleItemArray(jsonObjectJob);
                        } else {
                            JSONArray jsonArray = (JSONArray) jsonObject.get("result");
                            resultSearch = String.valueOf(jsonArray.length());
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObjectList = jsonArray.getJSONObject(i);
                                fillPeopleItemArray(jsonObjectList);
                            }
                        }
                        peopleAdapter = new PeopleAdapter(getContext(), peopleItemArrayList);
                        recyclerView.setAdapter(peopleAdapter);
                        peopleAdapter.setOnItemClickListener(PeopleListFragment.this);
                    }else{resultSearch="0";}
                    sendMessageInformationSearch(getString(R.string.msg_information_title),getString(R.string.msg_people_result)+ resultSearch);
                } catch (JSONException | IOException |
                        ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void fillPeopleItemArray(JSONObject objt) throws JSONException {
        String creatorName = objt.getString("eriGivenName");
        String creatorLastName = objt.getString("eriSn");
        String creatorSignum = objt.getString("uid");
        String creatorJob = objt.getString("title");
        String creatorEmail = objt.getString("mail");
        String creatorCountry = objt.getString("eriCountry");
        int creatorImg = countryItemFlagResource.countryItemFlagResources(creatorCountry);

        peopleItemArrayList.add(new PeopleItem(creatorName, creatorLastName,
                creatorSignum, creatorJob, creatorEmail, creatorCountry, creatorImg));

    }

    private void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void showProgressDialogWithTitle(String title, String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @Override
    public void onItemClick(int position) {
        PeopleItem clickedItem = peopleItemArrayList.get(position);
        EXTRA_UID_APPLY = clickedItem.getPeopleSignum();

        Fragment fragment = new PeopleDetailFragment();
        replaceFragment(fragment);
    }

    private void sendMessageInformationSearch(String title, String message) {
        AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
        builderOK.setTitle(title);
        builderOK.setMessage(message);
        // add the buttons
        builderOK.setPositiveButton(getString(R.string.btn_ok), null);
        AlertDialog dialogOK = builderOK.create();
        dialogOK.show();
    }

}

