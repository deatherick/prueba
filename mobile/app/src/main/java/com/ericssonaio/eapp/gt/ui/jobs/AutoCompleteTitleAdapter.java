package com.ericssonaio.eapp.gt.ui.jobs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ericssonaio.eapp.gt.R;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteTitleAdapter extends ArrayAdapter<TitleItem> {

    private List<TitleItem> titleItemListFull;

    public AutoCompleteTitleAdapter(@NonNull Context context, @NonNull List<TitleItem> titleItemList) {
        super(context, 0, titleItemList);

        titleItemListFull = new ArrayList<>(titleItemList);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return titleFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.title_autocomplete_row, parent, false
            );
        }

        TextView textViewName = convertView.findViewById(R.id.text_view_title_name);

        TitleItem titleItem = getItem(position);

        if (titleItem != null){
            textViewName.setText(titleItem.getTitleName());
        }

        return convertView;
    }

    private Filter titleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<TitleItem> suggestions = new ArrayList<>();

            if(constraint == null || constraint.length() == 0){
                suggestions.addAll(titleItemListFull);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();

                for(TitleItem item : titleItemListFull){
                    if(item.getTitleName().toLowerCase().contains(filterPattern)){
                        suggestions.add(item);
                    }
                }
            }

            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((List)results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((TitleItem) resultValue).getTitleName();
        }
    };
}
