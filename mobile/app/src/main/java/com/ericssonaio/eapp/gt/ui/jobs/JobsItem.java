package com.ericssonaio.eapp.gt.ui.jobs;

public class JobsItem {

    private String mJobName;
    private String mJobCountry;
    private String mJobId;
    private String mJobPostedDate;
    private int img;

    public int getImg() {return img;}

    public void setImg(int img) {this.img = img;}


    public String getJobId() {
        return mJobId;
    }

    public void setJobId(String mJobId) {
        this.mJobId = mJobId;
    }

    public String getJobPostedDate() {
        return mJobPostedDate;
    }

    public void setJobPostedDate(String mJobPostedDate) {
        this.mJobPostedDate = mJobPostedDate;
    }

    public String getJobUrlApply() {
        return mJobUrlApply;
    }

    public void setJobUrlApply(String mJobUrlApply) {
        this.mJobUrlApply = mJobUrlApply;
    }

    private String mJobUrlApply;

    public JobsItem(String jobName, String jobCountry, String jobId, String jobPostedDate, String jobUrlApply, int imgJob){

        mJobName = jobName;
        mJobCountry = jobCountry;
        mJobId = jobId;
        mJobPostedDate = jobPostedDate;
        mJobUrlApply = jobUrlApply;
        img = imgJob;

    }

    public String getJobName() {
        return mJobName;
    }

    public void setJobName(String mJobName) {
        this.mJobName = mJobName;
    }

    public String getJobCountry() {
        return mJobCountry;
    }

    public void setJobCountry(String mJobCountry) {
        this.mJobCountry = mJobCountry;
    }
}
