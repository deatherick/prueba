package com.ericssonaio.eapp.gt.notification.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import com.ericssonaio.eapp.gt.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static android.content.Context.MODE_PRIVATE;

public class NotificationClass {

    SharedPreferences prefs;
    String token = null;

    public void sendNotification(String token, String message, Context context) throws IOException {

        System.out.println("Ericsson AIO API");

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpPost postRequest = new HttpPost(
                "https://fcm.googleapis.com/fcm/send");

        DataRequestModel dataRequestModel = new DataRequestModel();
        NotificationData notificationData = new NotificationData();
        NotificationRequestModel notificationRequestModel = new NotificationRequestModel();

        notificationData.setDetail(message);
        notificationData.setTitle(String.valueOf(R.string.notification_title));
        notificationData.setNotificationType(String.valueOf(R.string.notification_type));

        notificationRequestModel.setData(notificationData);

        dataRequestModel.setNotification(notificationData);
        dataRequestModel.setData(notificationData);

        prefs = context.getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        if(!prefs.getString("myToken","").equals("")){
            token = prefs.getString("myToken", "");
        }

        System.out.println("Token:" + token);

        dataRequestModel.setTo(token);

        Gson gson = new Gson();
        Type type = new TypeToken<DataRequestModel>() {}.getType();

        String json = gson.toJson(dataRequestModel, type);

        StringEntity input = new StringEntity(json);
        input.setContentType("application/json");
        postRequest.addHeader("Authorization", String.valueOf(R.string.notification_value_token));
        postRequest.setEntity(input);


        System.out.println("request:" + json);


        HttpResponse response = httpClient.execute(postRequest);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatusLine().getStatusCode());
        } else if (response.getStatusLine().getStatusCode() == 200) {

            System.out.println("response:" + EntityUtils.toString(response.getEntity()));

        }

    }

    public boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    public void sendEmailWithPDF(String user, String pass, String subject, String content, String to, String file){
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(user, pass);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(String.valueOf(R.string.pdf_email_notification)));
            message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText(content);
            message.setContent(content, "text/html");

            //DataSource source = new FileDataSource(file);
            //message.setDataHandler(new DataHandler(source));
            message.setFileName(file);

            Transport.send(message);
            Log.i("Email enviado", message.toString());
            } catch (MessagingException e) {
                Log.d("MailJob", e.getMessage());
            }
    }

    public void sendEmail(String user, String pass, String subject, String content, String to){
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(user, pass);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(String.valueOf(R.string.pdf_email_notification)));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText(content);

            Transport.send(message);
            Log.i("Email enviado", message.toString());
        } catch (MessagingException e) {
            Log.d("MailJob", e.getMessage());
        }
    }

    public void createPDF(String request, String status, String date, Context context){
        String path = String.valueOf((Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)));
        File dir = new File(path);
        if(!dir.exists())
            dir.mkdir();

        Document document = new Document();
        try
        {
            String filePath = path+"/"+request+"-"+status+"-"+date+".pdf";
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filePath));
            document.open();
            document.add(new Paragraph(String.valueOf(R.string.pdf_email_paragraph)));

            InputStream ims =  context.getAssets().open("ericsson_logo.png");
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image image = Image.getInstance(stream.toByteArray());
            image.scalePercent(30);
            image.setAlignment(Element.ALIGN_LEFT);
            document.add(image);

            document.add(new Paragraph(String.valueOf(R.string.pdf_email_body1) + request));
            document.add(new Paragraph(String.valueOf(R.string.pdf_email_body2) + status));
            document.add(new Paragraph(String.valueOf(R.string.pdf_email_body3) + date));

            //Set attributes here
            document.addAuthor(String.valueOf(R.string.notification_title));
            document.addCreationDate();
            document.addCreator(String.valueOf(R.string.pdf_email_creator));
            document.addTitle(String.valueOf(R.string.pdf_email_title));
            document.addSubject(String.valueOf(R.string.pdf_email_subject));

            document.close();
            writer.close();

            System.out.println("Enviando attachment: "+filePath);

            sendEmailWithPDF(String.valueOf(R.string.email_user), String.valueOf(R.string.email_pass), String.valueOf(R.string.pdf_email_notification_title), String.valueOf(R.string.email_notification_title)+ request +String.valueOf(R.string.email_notification_detail), String.valueOf(R.string.email_to), request+"-"+status+"-"+date+".pdf");

            //System.out.println("Done, documento creado: "+path+"/"+request+"-"+status+"-"+date+".pdf");
            System.out.println("Done, documento creado: "+filePath);
        } catch (DocumentException e)
        {
            e.printStackTrace();
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}