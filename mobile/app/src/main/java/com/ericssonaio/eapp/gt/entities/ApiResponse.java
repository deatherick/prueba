package com.ericssonaio.eapp.gt.entities;

public class ApiResponse<T> {
    private boolean error;
    private Integer code;
    private String message;
    private T data;

    public ApiResponse(){

    }

    public ApiResponse(boolean error, Integer code, String message, T data) {
        this.error = error;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
