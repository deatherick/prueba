package com.ericssonaio.eapp.gt.ui.request;

import java.io.Serializable;

public class Request implements Serializable {

    private String id;
    private String status;
    private String title;
    private String created_timestamp;
    private String total_days;
    private String img;

        public Request(String id, String status, String title, String created_timestamp, String total_days, String img) {
            super();
            this.id = id;
            this.status = status;
            this.title = title;
            this.created_timestamp = created_timestamp;
            this.total_days = total_days;
            this.img = img;
        }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getId() {
        return id;
    }

        public void setId(String id) {
        this.id = id;
    }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCreated_timestamp() {
            return created_timestamp;
        }

        public void setCreated_timestamp(String created_timestamp) {
            this.created_timestamp = created_timestamp;
        }

        public String getTotal_days() {
            return total_days;
        }

        public void setTotal_days(String total_days) {
            this.total_days = total_days;
        }
}
