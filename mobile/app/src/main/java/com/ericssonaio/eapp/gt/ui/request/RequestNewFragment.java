package com.ericssonaio.eapp.gt.ui.request;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;
import com.ericssonaio.eapp.gt.notification.model.NotificationClass;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static android.widget.Toast.LENGTH_LONG;

public class RequestNewFragment extends Fragment implements View.OnClickListener {

    private RequestNewViewModel requestNewViewModel;
    private CardView newRequestVacation, newRequestLaboralEmbajada, newRequestLaboral,
            newRequestBanco, newRequestGlasses, newRequestAdelantos;

    SharedPreferences prefs;
    static String signum = null;
    String url = "http://ongoing.com.gt/erick/Api/create_request";
    String textDays;
    String tittleRequestLaboral = null;
    String notesLaboral = null;

    String tittleRequestEmbajada = null;
    String notesEmbajada = null;

    String tittleRequestBanco = null;
    String notesBanco = null;

    String tittleRequestLentes = null;
    String notesLentes = null;

    String tittleRequestAdelantos = null;
    String notesAdelantos = null;

    int requestType = 1;
    int resultSendRequest = 0;

    String tittleCard;
    String noteCard;

    ProgressDialog progressDialog;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    NotificationClass notificationClass;

    String[] items = null;
    int checkedItem = 0;

    public static RequestNewFragment newInstance() {
        return new RequestNewFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        requestNewViewModel =
                ViewModelProviders.of(this).get(RequestNewViewModel.class);
        View root = inflater.inflate(R.layout.request_new_fragment, container, false);

        /*final TextView textView = root.findViewById(R.id.text_new_request);
        requestNewViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("");
        progressDialog = new ProgressDialog(getContext());

        prefs = getContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        byte[] data = Base64.decode(prefs.getString("signume", ""), Base64.DEFAULT);
        signum = new String(data, StandardCharsets.UTF_8);

        // defining Cards
        newRequestVacation = (CardView) root.findViewById(R.id.request_vacation);
        newRequestLaboralEmbajada = (CardView) root.findViewById(R.id.request_embajada);
        newRequestLaboral = (CardView) root.findViewById(R.id.request_laboral);
        newRequestBanco = (CardView) root.findViewById(R.id.request_bancaria);
        newRequestGlasses = (CardView) root.findViewById(R.id.request_lentes);
        newRequestAdelantos = (CardView) root.findViewById(R.id.request_adelantos);

        // Add Click listener to the cards
        newRequestVacation.setOnClickListener(this);
        newRequestLaboralEmbajada.setOnClickListener(this);
        newRequestLaboral.setOnClickListener(this);
        newRequestBanco.setOnClickListener(this);
        newRequestGlasses.setOnClickListener(this);
        newRequestAdelantos.setOnClickListener(this);

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here
        }

        return root;
    }

    @Override
    public void onClick(View v) {

        initViews();

        Fragment fragment = null;
        switch (v.getId()) {
            case R.id.request_vacation:
                fragment = new RequestVacationFragment();
                replaceFragment(fragment);
                break;
            case R.id.request_embajada:
                sendRequestCarta(tittleRequestEmbajada, notesEmbajada, v.getId());
                break;
            case R.id.request_bancaria:
                sendRequestCarta(tittleRequestBanco, notesBanco, v.getId());
                break;
            case R.id.request_laboral:
                sendRequestCarta(tittleRequestLaboral, notesLaboral, v.getId());
                break;
            case R.id.request_lentes:
                sendRequestCarta(tittleRequestLentes, notesLentes, v.getId());
            case R.id.request_adelantos:
                sendRequestCarta(tittleRequestAdelantos, notesAdelantos, v.getId());
            default:
                break;
        }
    }

    private void initViews(){
        tittleRequestLaboral = getString(R.string.pdf_title_laboral);
        notesLaboral = getString(R.string.pdf_detail_laboral);

        tittleRequestEmbajada = getString(R.string.pdf_title_embajada);
        notesEmbajada = getString(R.string.pdf_title_embajada);

        tittleRequestBanco = getString(R.string.pdf_title_banco);
        notesBanco = getString(R.string.pdf_title_banco);

        tittleRequestLentes = getString(R.string.pdf_title_lentes);
        notesLentes = getString(R.string.pdf_title_lentes);

        tittleRequestAdelantos = getString(R.string.pdf_title_adelanto);
        notesAdelantos = getString(R.string.pdf_title_adelanto);


        items = new String[]{getString(R.string.pdf_item_salary_10), getString(R.string.pdf_item_salary_50),
                getString(R.string.pdf_item_salary_100),
                getString(R.string.pdf_item_salary_aguinaldo),
                getString(R.string.pdf_item_salary_bono14)};
    }

    private void sendRequestCarta(String tittle, String note, int cardViewId) {

        tittleCard = tittle;
        noteCard = note;

        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {

            if (cardViewId == R.id.request_adelantos) {
                AlertDialog.Builder builderMulti = new AlertDialog.Builder(getContext());
                builderMulti.setTitle(getString(R.string.msg_send_request_detail) + " : " + tittleCard);
                builderMulti.setSingleChoiceItems(items, checkedItem, null);
                //builderMulti.setMessage("Está seguro que desea enviar está solicitud?");
                // add the buttons
                builderMulti.setPositiveButton(getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // do something like...
                        AsyncListViewLoader enviandoInformacion = new AsyncListViewLoader();
                        enviandoInformacion.execute(url);
                    }
                }).setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // do something like...
                        Toast.makeText(getContext(), getString(R.string.msg_cancel_request_detail), Toast.LENGTH_SHORT).show();
                    }
                });
                // create and show the alert dialog
                AlertDialog dialogMulti = builderMulti.create();
                dialogMulti.show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.msg_send_request_detail) + " : " + tittleCard);
                builder.setMessage(getString(R.string.msg_question_send_request));
                // add the buttons
                builder.setPositiveButton(getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // do something like...
                        AsyncListViewLoader enviandoInformacion = new AsyncListViewLoader();
                        enviandoInformacion.execute(url);
                    }
                }).setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // do something like...
                        Toast.makeText(getContext(), getString(R.string.msg_cancel_request_detail), Toast.LENGTH_SHORT).show();
                    }
                });
                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        } else {
            // No hay conexión a Internet en este momento
            Toast.makeText(getContext(), getString(R.string.msg_network), LENGTH_LONG).show();
        }
    }

    private void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private class AsyncListViewLoader extends AsyncTask<String, Void, List<Request>> {


        @Override
        protected void onPostExecute(List<Request> result) {
            super.onPostExecute(result);
            hideProgressDialogWithTitle();
            notificationClass = new NotificationClass();

            if (resultSendRequest == 0) {

                Toast.makeText(getContext(), getString(R.string.msg_success_send_request), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
                builderOK.setTitle(getString(R.string.msg_information_title));
                builderOK.setMessage(getString(R.string.msg_success_send_request));
                // add the buttons
                builderOK.setPositiveButton(getString(R.string.btn_ok), null);
                AlertDialog dialogOK = builderOK.create();
                dialogOK.show();

                //notificationClass.sendNotification("", getString(R.string.email_notification_title) + tittleCard + getString(R.string.email_notification_detail), getContext());
                notificationClass.sendEmail(getString(R.string.email_user), getString(R.string.email_pass), getString(R.string.email_subject), getString(R.string.email_notification_title) + tittleCard + getString(R.string.email_notification_detail), getString(R.string.email_to));
            } else {
                Toast.makeText(getContext(), getString(R.string.msg_error_sending_request_detail), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderError = new AlertDialog.Builder(getContext());
                builderError.setTitle(getString(R.string.msg_error_sending_request_title));
                builderError.setMessage(getString(R.string.msg_error_sending_request_detail));
                // add the buttons
                builderError.setPositiveButton(getString(R.string.btn_ok), null);
                AlertDialog dialogError = builderError.create();
                dialogError.show();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialogWithTitle(getString(R.string.msg_information_title), getString(R.string.msg_sending_request));
        }


        @Override
        protected List<Request> doInBackground(String... params) {
            List<Request> result = new ArrayList<Request>();
            String json = null; //"{\"key\":1}";;
            Date today = new Date();
            resultSendRequest = 0;

            try {

                URL u = new URL(params[0]);

                JSONObject jsonObjectDays = new JSONObject();
                jsonObjectDays.accumulate("days", "");

                Log.println(Log.INFO, "JSON request: ", jsonObjectDays.toString());

                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("created_by", "edejuli");
                jsonObject.accumulate("title", tittleCard);
                jsonObject.accumulate("request_type", requestType);
                jsonObject.accumulate("total_days", 0);
                jsonObject.accumulate("calendar", jsonObjectDays);
                jsonObject.accumulate("observations", noteCard);
                json = jsonObject.toString();

                Log.println(Log.INFO, "JSON request: ", json);

                HttpURLConnection conn = (HttpURLConnection) u.openConnection();
                conn.setConnectTimeout(5000);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.setAllowUserInteraction(false);
                conn.setConnectTimeout(3000);
                conn.setReadTimeout(3000);
                conn.setRequestMethod("GET");

                OutputStream os = conn.getOutputStream();
                os.write(json.getBytes("UTF-8"));
                os.close();

                conn.connect();

                //Log.println(Log.INFO, "JSON 1: ", JSONResp);

                InputStream in = new BufferedInputStream(conn.getInputStream());
                String results = inputStreamToString(in);

                Log.println(Log.INFO, "Result send request: ", results);

                JSONObject obj = new JSONObject(results);

                Log.println(Log.INFO, "Code result: ", obj.getString("code"));

                return result;
            } catch (Throwable t) {
                t.printStackTrace();
                resultSendRequest = 1;
            }
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            hideProgressDialogWithTitle();

            Toast.makeText(getContext(), getString(R.string.msg_error_sending_request_detail), Toast.LENGTH_SHORT).show();
            AlertDialog.Builder builderError = new AlertDialog.Builder(getContext());
            builderError.setTitle(getString(R.string.msg_error_sending_request_title));
            builderError.setMessage(getString(R.string.msg_error_sending_request_detail));
            // add the buttons
            builderError.setPositiveButton(getString(R.string.btn_ok), null);
            AlertDialog dialogError = builderError.create();
            dialogError.show();
        }
    }

    private String inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();

        InputStreamReader isr = new InputStreamReader(is);

        BufferedReader rd = new BufferedReader(isr);

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer.toString();
    }

    private void showProgressDialogWithTitle(String title, String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }
}
