package com.ericssonaio.eapp.gt.notification.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataRequestModel {
    @SerializedName("notification")
    private NotificationData mNotification;

    @SerializedName("data")
    private NotificationData mData;

    @SerializedName("to")
    private String mTo;

    public NotificationData getData() {
        return mData;
    }

    public void setData(NotificationData data) {
        mData = data;
    }

    public String getTo() {
        return mTo;
    }

    public void setTo(String to) {
        mTo = to;
    }

    public NotificationData getNotification() {
        return mNotification;
    }

    public void setNotification(NotificationData notification) {
        mNotification = notification;
    }
}
