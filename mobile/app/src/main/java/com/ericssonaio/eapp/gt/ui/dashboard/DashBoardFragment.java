package com.ericssonaio.eapp.gt.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;
import com.ericssonaio.eapp.gt.ui.certifications.CertificationFragment;
import com.ericssonaio.eapp.gt.ui.concur.ConcurFragment;
import com.ericssonaio.eapp.gt.ui.elearning.ElearningFragment;
import com.ericssonaio.eapp.gt.ui.fiori.FioriFragment;
import com.ericssonaio.eapp.gt.ui.jobs.JobsFragment;
import com.ericssonaio.eapp.gt.ui.newfinder.PeopleFragment;
import com.ericssonaio.eapp.gt.ui.personal.PersonalFragment;
import com.ericssonaio.eapp.gt.ui.request.RequestFragment;
import com.ericssonaio.eapp.gt.ui.salas.SalasFragment;

public class DashBoardFragment extends Fragment implements View.OnClickListener {

    private DashBoardViewModel dashboardViewModel;
    private CardView personalCard, fioriCard, certificationCard, concurCard,
            salasCard, elearningCard, requestCard, finderCard, jobsCard;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashBoardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        /*final TextView textView = root.findViewById(R.id.text_dashboard);
        dashboardViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("");

        // defining Cards
        personalCard = (CardView) root.findViewById(R.id.personal_card);
        fioriCard = (CardView) root.findViewById(R.id.fiori_card);
        certificationCard = (CardView) root.findViewById(R.id.certification_card);
        concurCard = (CardView) root.findViewById(R.id.concur_card);
        salasCard = (CardView) root.findViewById(R.id.salas_card);
        elearningCard = (CardView) root.findViewById(R.id.elearning_card);
        requestCard = (CardView) root.findViewById(R.id.request_card);
        finderCard = (CardView) root.findViewById(R.id.request_finder);
        jobsCard = (CardView) root.findViewById(R.id.jobs_card);
        // Add Click listener to the cards
        personalCard.setOnClickListener(this);
        fioriCard.setOnClickListener(this);
        certificationCard.setOnClickListener(this);
        concurCard.setOnClickListener(this);
        salasCard.setOnClickListener(this);
        elearningCard.setOnClickListener(this);
        requestCard.setOnClickListener(this);
        finderCard.setOnClickListener(this);
        jobsCard.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        switch (v.getId()) {
            case R.id.personal_card :
                fragment = new PersonalFragment();
                replaceFragment(fragment);
                break;
            case R.id.fiori_card :
                fragment = new FioriFragment();
                replaceFragment(fragment);
                break;
            case R.id.certification_card :
                fragment = new CertificationFragment();
                replaceFragment(fragment);
                break;
            case R.id.concur_card :
                fragment = new ConcurFragment();
                replaceFragment(fragment);
                break;
            case R.id.salas_card :
                fragment = new SalasFragment();
                replaceFragment(fragment);
                break;
            case R.id.elearning_card :
                fragment = new ElearningFragment();
                replaceFragment(fragment);
                break;
            case R.id.request_card :
                fragment = new RequestFragment();
                replaceFragment(fragment);
                break;
            case R.id.request_finder:
                fragment = new PeopleFragment();
                replaceFragment(fragment);
                break;
            case R.id.jobs_card:
                fragment = new JobsFragment();
                replaceFragment(fragment);
            default:break;
        }
    }

    private void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}