package com.ericssonaio.eapp.gt.notification.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class NotificationData {
    @SerializedName("body")
    private String mDetail;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("notificationType")
    private String mNotificationType;

    public String getDetail() {
        return mDetail;
    }

    public void setDetail(String detail) {
        mDetail = detail;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getNotificationType() {return mNotificationType;}

    public void setNotificationType(String notificationType) {mNotificationType = notificationType;}
}
