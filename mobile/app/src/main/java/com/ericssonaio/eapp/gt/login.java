package com.ericssonaio.eapp.gt;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ericssonaio.eapp.gt.entities.ApiResponse;
import com.ericssonaio.eapp.gt.entities.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.concurrent.Executor;

import static android.widget.Toast.LENGTH_LONG;

public class login extends AppCompatActivity {
    RelativeLayout rellay1, rellay2;
    LinearLayout checkboxLay;
    private EditText vusuario, vclave;
    private Button fPassword;
    private CheckBox chkRememberme;
    boolean debeGuardarDatos = false;
    Handler handler = new Handler();
    private Executor executor = new Executor() {
        @Override
        public void execute(Runnable command) {
            handler.post(command);
        }
    };
    BiometricManager biometricManager = BiometricManager.from(login.this);

    Runnable runnable;

    {
        runnable = new Runnable() {
            @Override
            public void run() {
                rellay1.setVisibility(View.VISIBLE);
                rellay2.setVisibility(View.VISIBLE);
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (Build.VERSION.SDK_INT > 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        rellay1 = (RelativeLayout) findViewById(R.id.rellay1);
        rellay2 = (RelativeLayout) findViewById(R.id.rellay2);
        chkRememberme = (CheckBox)findViewById(R.id.checkRememberme);
        checkboxLay = (LinearLayout) findViewById(R.id.linearrememberme);
        Button btn_login = findViewById(R.id.btn_login);
        vusuario = findViewById(R.id.usuario);
        vclave = findViewById(R.id.clave);

        SharedPreferences prefs = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        String tmpUser = prefs.getString("signume", "");
        String tmpPass = prefs.getString("passe", "");
        String tmpCheck = prefs.getString(("saved"),"");

        if (tmpUser != null  && !tmpUser.isEmpty() && tmpCheck.equals("S")){
            vusuario.setText(tmpUser);
            vclave.setText(tmpPass);
            chkRememberme.setChecked(true);
        }else {
            chkRememberme.setChecked(false);
        }



        chkRememberme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkRememberme.isChecked()){
                    debeGuardarDatos = true;
                }else{
                    savedResources("","","S");
                }
            }
        });

        // checkboxLay.setVisibility(View.GONE);

        handler.postDelayed(runnable, 2000); //2000 is the timeout for the splash


        if (chkRememberme.isChecked() && validaBiometricStatus(biometricManager).equals("OK")){
            final Dialog diag = new Dialog(login.this);

            diag.setContentView(R.layout.ui_dialoghuella);
            ImageView close = diag.findViewById(R.id.IV_Close);
            diag.show();
            showBiometricPrompt(diag, tmpUser,tmpPass);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    diag.dismiss();
                }
            });
            diag.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            diag.show();
        }

        fPassword = findViewById(R.id.forgotPass);
        /*   fPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog diag = new Dialog(login.this);
                diag.setContentView(R.layout.ui_dialoghuella);
                ImageView close = diag.findViewById(R.id.IV_Close);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        diag.dismiss();
                    }
                });
                diag.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                diag.show();

                //BIOMETRIC


                String bio = validaBiometricStatus(biometricManager);

            }
        });*/



        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ingresarSistema();
            }
        });


        vclave.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                System.out.println(actionId);
                boolean handled = false;
                if (actionId == 0){
                    ingresarSistema();
                    handled = true;
                }
                return handled;
            }
        });
    }

    private String validaBiometricStatus(BiometricManager pbiometricManager) {
        String retorno = "ERROR";
        switch (pbiometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                //Toast.makeText(login.this,"App can authenticate using biometrics.", LENGTH_LONG).show();
                retorno =  "OK";
                //return retorno;
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                //Toast.makeText(login.this,"No biometric features available on this device.", LENGTH_LONG).show();
                retorno =  "No biometric features available on this device.";
                // return retorno;
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                //Toast.makeText(login.this,"Biometric features are currently unavailable.", LENGTH_LONG).show();
                //diag.dismiss();
                retorno =  "Biometric features are currently unavailable.";
                // return retorno;
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                //Toast.makeText(login.this,"The user hasn't associated any biometric credentials with their account.", LENGTH_LONG).show();
                //diag.dismiss();
                retorno =  "The user hasn't associated any biometric credentials with their account.";
                //  return retorno;
                break;
        }
        return retorno;
    }

    private void showBiometricPrompt(final Dialog dialog, final String puser, final String ppassword) {
        BiometricPrompt.PromptInfo promptInfo =
                new BiometricPrompt.PromptInfo.Builder()
                        .setTitle("Biometric login for my app")
                        .setSubtitle("Log in using your biometric credential")
                        .setNegativeButtonText("Cancel")
                        .build();

        BiometricPrompt biometricPrompt = new BiometricPrompt(login.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        "Codigo de Enter: " + errorCode, LENGTH_LONG)
                        .show();
                if (errorCode == 13){dialog.dismiss();}
                Toast.makeText(getApplicationContext(),
                        "Authentication error: " + errString, LENGTH_LONG)
                        .show();
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                BiometricPrompt.CryptoObject authenticatedCryptoObject =
                        result.getCryptoObject();
                // User has verified the signature, cipher, or message
                // authentication code (MAC) associated with the crypto object,
                // so you can use it in your app's crypto-driven workflows.

                //Intent intent = new Intent(login.this, MainActivity.class);
                //startActivity(intent);

                ingresarSistema();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication failed",
                        LENGTH_LONG)
                        .show();
            }
        });

        // Displays the "log in" prompt.
        biometricPrompt.authenticate(promptInfo);
    }


    public void ingresarSistema() {
        RequestQueue mRequestQueue;
        final ProgressDialog pd = new ProgressDialog(login.this);
        pd.setMessage ("Please Wait, Logging in...");

        // Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap

        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());

        // Instantiate the RequestQueue with the cache and network.
        mRequestQueue = new RequestQueue(cache, network);

        // Start the queue
        mRequestQueue.start();
        if (vusuario != null && vclave != null ){
            if (vusuario.length() > 0 ) {
                if (vclave.length() > 0) {
                    try {
                        pd.show();
                        final JSONObject jsonBody = new JSONObject();
                        jsonBody.put("user", vusuario.getText().toString());
                        jsonBody.put("pass", vclave.getText().toString());

                        JsonObjectRequest stringRequest = new JsonObjectRequest(getString(R.string.loginUrl), jsonBody, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.i("LoginActivity", response.toString());
                                Gson  gson = new Gson();
                                Type responseType = new TypeToken<ApiResponse<User>>(){}.getType();
                                ApiResponse<User> userResponse = gson.fromJson(response.toString(), responseType);
                                if(!userResponse.isError()) {
                                    pd.dismiss();
                                    if (debeGuardarDatos) {
                                        savedResources(vusuario.getText().toString(),vclave.getText().toString(),"S");
                                    }
                                    Toast.makeText(login.this,"Welcome to Ericsson AIO", LENGTH_LONG).show();
                                    Intent intent;
                                    Bundle miBundle = new Bundle();
                                    miBundle.putString("Datos", response.toString());

                                    intent = new Intent(login.this,MainActivity.class);
                                    intent.putExtras(miBundle);
                                    startActivity(intent);

                                } else {
                                    Toast.makeText(login.this, userResponse.getMessage(), LENGTH_LONG).show();
                                    pd.dismiss();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Handle error
                                Log.e("LoginActivity: ", error.getMessage(), error);
                                pd.dismiss();
                                Toast.makeText(login.this, "Authentication error.", LENGTH_LONG).show();

                            }
                        });
                        System.out.println("stringRequest: " + stringRequest);
                        mRequestQueue.add(stringRequest);
                    } catch (Exception error) {
                        Log.e("LoginActivity", error.getMessage(), error);
                        pd.dismiss();
                    }
                } else {
                    Toast.makeText(login.this, "Invalid Password", LENGTH_LONG).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        vclave.getFocusable(); pd.dismiss();
                    }
                }
            } else {
                Toast.makeText(login.this, "Invalid Username", LENGTH_LONG).show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {vusuario.getFocusable();} pd.dismiss();
            }
        } else {
            Toast.makeText(login.this, "Invalid Username & Password", LENGTH_LONG).show();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {vusuario.getFocusable();} pd.dismiss();
        }

    }

    private void savedResources(String pusuario, String pclave, String pchecked) {
        SharedPreferences pref =getSharedPreferences("myPrefs",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=pref.edit();
        editor.putString("signume", pusuario);
        editor.putString("passe", pclave);
        editor.putString("saved",pchecked);
        editor.commit();
    }
}