package com.ericssonaio.eapp.gt.ui.jobs;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;

public class JobsDetailFragment extends Fragment {

    private JobsDetailViewModel jobsDetailViewModel;
    ProgressDialog progressDialog;
    WebView mWebView;

    public static JobsDetailFragment newInstance() {
        return new JobsDetailFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        jobsDetailViewModel =
                ViewModelProviders.of(this).get(JobsDetailViewModel.class);
        View root = inflater.inflate(R.layout.fragment_jobs_detail, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.msg_job_detail));

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        String url = JobListFragment.EXTRA_URL_APPLY;
        System.out.println("Ericsson URL Apply: " + url);

        progressDialog = new ProgressDialog(getContext());

        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            mWebView = (WebView) root.findViewById(R.id.webview_details_jobs);

            // Force links and redirects to open in the WebView instead of in a browser
            mWebView.setWebViewClient(new WebViewClient() {

                private volatile boolean timeout;
                private volatile String timeoutOnPageStartedURL;
                private volatile String timeoutCurrentURL;

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);

                    timeout = true;
                    timeoutOnPageStartedURL = url;
                    timeoutCurrentURL = view.getUrl();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (timeout) {
                                view.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        String currentURL = view.getUrl();
                                        if ((timeoutOnPageStartedURL.hashCode() == currentURL.hashCode()) ||
                                                (timeoutCurrentURL.hashCode() == currentURL.hashCode())) {
                                            // do what you want with UI
                                            showProgressDialogWithTitle(getString(R.string.msg_loading_title), getString(R.string.msg_job_loading_information_jobs));
                                        }
                                    }
                                });
                            }
                        }
                    }).start();
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    hideProgressDialogWithTitle();
                    timeout = false;
                }
            });
            // Enable Javascript
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            mWebView.loadUrl(url);
        } else {
            // No hay conexión a Internet en este momento
            Toast.makeText(getContext(), getString(R.string.msg_network), Toast.LENGTH_LONG).show();
        }


        return root;
    }

    private void showProgressDialogWithTitle(String title, String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }
}
