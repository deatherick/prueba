package com.ericssonaio.eapp.gt.ui.request;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ericssonaio.eapp.gt.R;

public class RequestSearchViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<String> mText;

    public RequestSearchViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue(String.valueOf(R.string.msg_request_find));
    }

    public LiveData<String> getText() {
        return mText;
    }
}
