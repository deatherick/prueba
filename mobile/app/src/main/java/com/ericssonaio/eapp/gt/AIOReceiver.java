package com.ericssonaio.eapp.gt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AIOReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //Toast.makeText(context, "Servicio iniciado desde BroadcastReceiver", Toast.LENGTH_SHORT).show();
        context.startService(new Intent(context, AIOReceiver.class));
    }
}
