package com.ericssonaio.eapp.gt.ui.jobs;

public class TitleItem {

    private String titleName;

    public TitleItem(String titleName) {
        this.titleName = titleName;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }
}
