package com.ericssonaio.eapp.gt.ui.jobs;

import com.ericssonaio.eapp.gt.R;

public class CountryItemFlagResource {
    public int countryItemFlagResources(String country) {
        int countryResource = 0;
        switch (country.toLowerCase()) {
            case "andorra":
                countryResource = R.drawable.ic_flag_ad;
            case "united arab emirates":
                countryResource = R.drawable.ic_flag_ae;
            case "afghanistan":
                countryResource = R.drawable.ic_flag_af;
                break;
            case "antigua and barbuda":
                countryResource = R.drawable.ic_flag_ag;
                break;
            case "anguilla":
                countryResource = R.drawable.ic_flag_ai;
                break;
            case "albania":
                countryResource = R.drawable.ic_flag_al;
                break;
            case "armenia":
                countryResource = R.drawable.ic_flag_am;
                break;
            case "netherlands antilles":
                countryResource = R.drawable.ic_flag_an;
                break;
            case "angola":
                countryResource = R.drawable.ic_flag_ao;
                break;
            case "antarctica":
                countryResource = R.drawable.ic_flag_aq;
                break;
            case "argentina":
                countryResource = R.drawable.ic_flag_ar;
                break;
            case "american samoa":
                countryResource = R.drawable.ic_flag_ar;
                //ic_flag_as
                break;
            case "austria":
                countryResource = R.drawable.ic_flag_at;
                break;
            case "australia":
                countryResource = R.drawable.ic_flag_au;
                break;
            case "aruba":
                countryResource = R.drawable.ic_flag_aw;
                break;
            case "aland islands":
                countryResource = R.drawable.ic_flag_ax;
                break;
            case "azerbaijan":
                countryResource = R.drawable.ic_flag_az;
                break;
            case "bosnia and herzegovina":
                countryResource = R.drawable.ic_flag_ba;
                break;
            case "barbados":
                countryResource = R.drawable.ic_flag_bb;
                break;
            case "bangladesh":
                countryResource = R.drawable.ic_flag_bd;
                break;
            case "belgium":
                countryResource = R.drawable.ic_flag_be;
                break;
            case "burkina faso":
                countryResource = R.drawable.ic_flag_bf;
                break;
            case "bulgaria":
                countryResource = R.drawable.ic_flag_bg;
                break;
            case "bahrain":
                countryResource = R.drawable.ic_flag_bh;
                break;
            case "burundi":
                countryResource = R.drawable.ic_flag_bi;
                break;
            case "benin":
                countryResource = R.drawable.ic_flag_bj;
                break;
            case "saint barthelemy":
                countryResource = R.drawable.ic_flag_bl;
                break;
            case "bermuda":
                countryResource = R.drawable.ic_flag_bm;
                break;
            case "brunei":
                countryResource = R.drawable.ic_flag_bn;
                break;
            case "bolivia":
                countryResource = R.drawable.ic_flag_bo;
                break;
            case "caribbean netherlands":
                countryResource = R.drawable.ic_flag_bq;
                break;
            case "brazil":
                countryResource = R.drawable.ic_flag_af;
                //error svg
                break;
            case "bahamas":
                countryResource = R.drawable.ic_flag_bs;
                break;
            case "bhutan":
                countryResource = R.drawable.ic_flag_bt;
                break;
            case "bouvet island":
                countryResource = R.drawable.ic_flag_bv;
                break;
            case "botswana":
                countryResource = R.drawable.ic_flag_bw;
                break;
            case "belarus":
                countryResource = R.drawable.ic_flag_by;
                break;
            case "belize":
                countryResource = R.drawable.ic_flag_bz;
                break;
            case "canada":
                countryResource = R.drawable.ic_flag_ca;
                break;
            case "cocos islands":
                countryResource = R.drawable.ic_flag_cc;
                break;
            case "democratic republic of the congo":
                countryResource = R.drawable.ic_flag_cd;
                break;
            case "central african republic":
                countryResource = R.drawable.ic_flag_cf;
                break;
            case "congo":
                countryResource = R.drawable.ic_flag_cg;
                break;
            case "switzerland":
                countryResource = R.drawable.ic_flag_ch;
                break;
            case "ivory coast":
                countryResource = R.drawable.ic_flag_ci;
                break;
            case "cook islands":
                countryResource = R.drawable.ic_flag_ck;
                break;
            case "chile":
                countryResource = R.drawable.ic_flag_cl;
                break;
            case "cameroon":
                countryResource = R.drawable.ic_flag_cm;
                break;
            case "china":
                countryResource = R.drawable.ic_flag_cn;
                break;
            case "colombia":
                countryResource = R.drawable.ic_flag_co;
                break;
            case "costa rica":
                countryResource = R.drawable.ic_flag_af;
                //problema svg
                break;
            case "cuba":
                countryResource = R.drawable.ic_flag_cu;
                break;
            case "cape verde":
                countryResource = R.drawable.ic_flag_cv;
                break;
            case "curacao":
                countryResource = R.drawable.ic_flag_cw;
                break;
            case "christmas island":
                countryResource = R.drawable.ic_flag_cx;
                break;
            case "cyprus":
                countryResource = R.drawable.ic_flag_cy;
                break;
            case "czech republic":
                countryResource = R.drawable.ic_flag_cz;
                break;
            case "germany":
                countryResource = R.drawable.ic_flag_de;
                break;
            case "djibouti":
                countryResource = R.drawable.ic_flag_dj;
                break;
            case "denmark":
                countryResource = R.drawable.ic_flag_dk;
                break;
            case "dominica":
                countryResource = R.drawable.ic_flag_af;
                //error svg
                break;
            case "dominican republic":
                countryResource = R.drawable.ic_flag_do;
                break;
            case "algeria":
                countryResource = R.drawable.ic_flag_dz;
                break;
            case "ecuador":
                countryResource = R.drawable.ic_flag_ec;
                break;
            case "estonia":
                countryResource = R.drawable.ic_flag_ee;
                break;
            case "egypt":
                countryResource = R.drawable.ic_flag_eg;
                break;
            case "western sahara":
                countryResource = R.drawable.ic_flag_eh;
                break;
            case "eritrea":
                countryResource = R.drawable.ic_flag_er;
                break;
            case "spain":
                countryResource = R.drawable.ic_flag_es;
                break;
            case "ethiopia":
                countryResource = R.drawable.ic_flag_et;
                break;
            case "europe":
                countryResource = R.drawable.ic_flag_eu;
                break;
            case "finland":
                countryResource = R.drawable.ic_flag_fi;
                break;
            case "fiji":
                countryResource = R.drawable.ic_flag_fj;
                break;
            case "falkland islands":
                countryResource = R.drawable.ic_flag_fk;
                break;
            case "micronesia":
                countryResource = R.drawable.ic_flag_fm;
                break;
            case "faroe islands":
                countryResource = R.drawable.ic_flag_fo;
                break;
            case "france":
                countryResource = R.drawable.ic_flag_fr;
                break;
            case "gabon":
                countryResource = R.drawable.ic_flag_ga;
                break;
            case "england":
                countryResource = R.drawable.ic_flag_gb_eng;
                break;
            case "northern ireland":
                countryResource = R.drawable.ic_flag_gb_nir;
                break;
            case "scotland":
                countryResource = R.drawable.ic_flag_gb_sct;
                break;
            case "wales":
                countryResource = R.drawable.ic_flag_gb_wls;
                break;
            case "united kingdom":
                countryResource = R.drawable.ic_flag_gb;
                break;
            case "grenada":
                countryResource = R.drawable.ic_flag_gd;
                break;
            case "georgia":
                countryResource = R.drawable.ic_flag_ge;
                break;
            case "french guiana":
                countryResource = R.drawable.ic_flag_gf;
                break;
            case "guernsey":
                countryResource = R.drawable.ic_flag_gg;
                break;
            case "ghana":
                countryResource = R.drawable.ic_flag_gh;
                break;
            case "gibraltar":
                countryResource = R.drawable.ic_flag_gi;
                break;
            case "greenland":
                countryResource = R.drawable.ic_flag_gl;
                break;
            case "gambia":
                countryResource = R.drawable.ic_flag_gm;
                break;
            case "guinea":
                countryResource = R.drawable.ic_flag_gn;
                break;
            case "guadeloupe":
                countryResource = R.drawable.ic_flag_gp;
                break;
            case "equatorial guinea":
                countryResource = R.drawable.ic_flag_gq;
                break;
            case "greece":
                countryResource = R.drawable.ic_flag_gr;
                break;
            case "south georgia and the south sandwich islands":
                countryResource = R.drawable.ic_flag_default;
                //gs no funciona
                break;
            case "guatemala":
                countryResource = R.drawable.ic_flag_gt;
                break;
            case "guam":
                countryResource = R.drawable.ic_flag_gu;
                break;
            case "guinea-bissau":
                countryResource = R.drawable.ic_flag_gw;
                break;
            case "guyana":
                countryResource = R.drawable.ic_flag_gy;
                break;
            case "hong kong":
                countryResource = R.drawable.ic_flag_hk;
                break;
            case "heard island and mcdonald islands":
                countryResource = R.drawable.ic_flag_hm;
                break;
            case "honduras":
                countryResource = R.drawable.ic_flag_hn;
                break;
            case "croatia":
                countryResource = R.drawable.ic_flag_hr;
                break;
            case "haiti":
                countryResource = R.drawable.ic_flag_ht;
                break;
            case "hungary":
                countryResource = R.drawable.ic_flag_hu;
                break;
            case "indonesia":
                countryResource = R.drawable.ic_flag_id;
                break;
            case "ireland":
                countryResource = R.drawable.ic_flag_ie;
                break;
            case "israel":
                countryResource = R.drawable.ic_flag_il;
                break;
            case "isle of man":
                countryResource = R.drawable.ic_flag_im;
                //problemas svg
                break;
            case "india":
                countryResource = R.drawable.ic_flag_in;
                break;
            case "british indian ocean territory":
                countryResource = R.drawable.ic_flag_io;
                break;
            case "iraq":
                countryResource = R.drawable.ic_flag_iq;
                break;
            case "iran":
                countryResource = R.drawable.ic_flag_ir;
                break;
            case "iceland":
                countryResource = R.drawable.ic_flag_is;
                break;
            case "italy":
                countryResource = R.drawable.ic_flag_it;
                break;
            case "jersey":
                countryResource = R.drawable.ic_flag_je;
                break;
            case "jamaica":
                countryResource = R.drawable.ic_flag_jm;
                break;
            case "jordan":
                countryResource = R.drawable.ic_flag_jo;
                break;
            case "japan":
                countryResource = R.drawable.ic_flag_jp;
                break;
            case "kenya":
                countryResource = R.drawable.ic_flag_ke;
                break;
            case "kyrgyzstan":
                countryResource = R.drawable.ic_flag_kg;
                break;
            case "cambodia":
                countryResource = R.drawable.ic_flag_kh;
                break;
            case "kiribati":
                countryResource = R.drawable.ic_flag_ki;
                break;
            case "comoros":
                countryResource = R.drawable.ic_flag_km;
                break;
            case "saint kitts and nevis":
                countryResource = R.drawable.ic_flag_kn;
                break;
            case "north korea":
                countryResource = R.drawable.ic_flag_kp;
                break;
            case "south korea":
                countryResource = R.drawable.ic_flag_kr;
                break;
            case "kuwait":
                countryResource = R.drawable.ic_flag_kw;
                break;
            case "cayman islands":
                countryResource = R.drawable.ic_flag_ky;
                break;
            case "kazakhstan":
                countryResource = R.drawable.ic_flag_kz;
                break;
            case "laos":
                countryResource = R.drawable.ic_flag_la;
                break;
            case "lebanon":
                countryResource = R.drawable.ic_flag_lb;
                break;
            case "saint lucia":
                countryResource = R.drawable.ic_flag_lc;
                break;
            case "liechtenstein":
                countryResource = R.drawable.ic_flag_li;
                break;
            case "sri lanka":
                countryResource = R.drawable.ic_flag_lk;
                break;
            case "liberia":
                countryResource = R.drawable.ic_flag_lr;
                break;
            case "lesotho":
                countryResource = R.drawable.ic_flag_ls;
                break;
            case "lithuania":
                countryResource = R.drawable.ic_flag_lt;
                break;
            case "luxembourg":
                countryResource = R.drawable.ic_flag_lu;
                break;
            case "latvia":
                countryResource = R.drawable.ic_flag_lv;
                break;
            case "libya":
                countryResource = R.drawable.ic_flag_ly;
                break;
            case "morocco":
                countryResource = R.drawable.ic_flag_ma;
                break;
            case "monaco":
                countryResource = R.drawable.ic_flag_mc;
                break;
            case "moldova":
                countryResource = R.drawable.ic_flag_md;
                break;
            case "montenegro":
                countryResource = R.drawable.ic_flag_me;
                break;
            case "saint martin":
                countryResource = R.drawable.ic_flag_mf;
                break;
            case "madagascar":
                countryResource = R.drawable.ic_flag_mg;
                break;
            case "marshall islands":
                countryResource = R.drawable.ic_flag_mh;
                break;
            case "macedonia":
                countryResource = R.drawable.ic_flag_mk;
                break;
            case "mali":
                countryResource = R.drawable.ic_flag_ml;
                break;
            case "myanmar":
                countryResource = R.drawable.ic_flag_mm;
                break;
            case "mongolia":
                countryResource = R.drawable.ic_flag_mn;
                break;
            case "macao":
                countryResource = R.drawable.ic_flag_mo;
                break;
            case "northern mariana islands":
                countryResource = R.drawable.ic_flag_mp;
                break;
            case "martinique":
                countryResource = R.drawable.ic_flag_mq;
                break;
            case "mauritania":
                countryResource = R.drawable.ic_flag_mr;
                break;
            case "montserrat":
                countryResource = R.drawable.ic_flag_ms;
                break;
            case "malta":
                countryResource = R.drawable.ic_flag_mt;
                break;
            case "mauritius":
                countryResource = R.drawable.ic_flag_mu;
                break;
            case "maldives":
                countryResource = R.drawable.ic_flag_mv;
                break;
            case "malawi":
                countryResource = R.drawable.ic_flag_mw;
                break;
            case "mexico":
                countryResource = R.drawable.ic_flag_mx;
                break;
            case "malaysia":
                countryResource = R.drawable.ic_flag_my;
                break;
            case "mozambique":
                countryResource = R.drawable.ic_flag_mz;
                break;
            case "namibia":
                countryResource = R.drawable.ic_flag_na;
                break;
            case "new caledonia":
                countryResource = R.drawable.ic_flag_nc;
                break;
            case "niger":
                countryResource = R.drawable.ic_flag_ne;
                break;
            case "norfolk island":
                countryResource = R.drawable.ic_flag_nf;
                break;
            case "nigeria":
                countryResource = R.drawable.ic_flag_ng;
                break;
            case "nicaragua":
                countryResource = R.drawable.ic_flag_ni;
                break;
            case "netherlands":
                countryResource = R.drawable.ic_flag_nl;
                break;
            case "norway":
                countryResource = R.drawable.ic_flag_no;
                break;
            case "nepal":
                countryResource = R.drawable.ic_flag_np;
                break;
            case "nauru":
                countryResource = R.drawable.ic_flag_nr;
                break;
            case "niue":
                countryResource = R.drawable.ic_flag_nu;
                break;
            case "new zealand":
                countryResource = R.drawable.ic_flag_nz;
                break;
            case "oman":
                countryResource = R.drawable.ic_flag_om;
                //problemas svg
                break;
            case "panama":
                countryResource = R.drawable.ic_flag_pa;
                break;
            case "peru":
                countryResource = R.drawable.ic_flag_pe;
                break;
            case "french polynesia":
                countryResource = R.drawable.ic_flag_pf;
                break;
            case "papua new guinea":
                countryResource = R.drawable.ic_flag_pg;
                break;
            case "philippines":
                countryResource = R.drawable.ic_flag_ph;
                break;
            case "pakistan":
                countryResource = R.drawable.ic_flag_pk;
                break;
            case "poland":
                countryResource = R.drawable.ic_flag_pl;
                break;
            case "saint pierre and miquelon":
                countryResource = R.drawable.ic_flag_pm;
                break;
            case "pitcairn":
                countryResource = R.drawable.ic_flag_pn;
                break;
            case "puerto rico":
                countryResource = R.drawable.ic_flag_pr;
                break;
            case "palestine":
                countryResource = R.drawable.ic_flag_ps;
                break;
            case "portugal":
                countryResource = R.drawable.ic_flag_pt;
                break;
            case "palau":
                countryResource = R.drawable.ic_flag_pw;
                break;
            case "paraguay":
                countryResource = R.drawable.ic_flag_py;
                break;
            case "qatar":
                countryResource = R.drawable.ic_flag_qa;
                break;
            case "reunion":
                countryResource = R.drawable.ic_flag_re;
                break;
            case "romania":
                countryResource = R.drawable.ic_flag_ro;
                break;
            case "serbia":
                countryResource = R.drawable.ic_flag_rs;
                break;
            case "russia":
                countryResource = R.drawable.ic_flag_ru;
                break;
            case "rwanda":
                countryResource = R.drawable.ic_flag_rw;
                break;
            case "saudi arabia":
                countryResource = R.drawable.ic_flag_sa;
                break;
            case "solomon islands":
                countryResource = R.drawable.ic_flag_sb;
                break;
            case "seychelles":
                countryResource = R.drawable.ic_flag_sc;
                break;
            case "sudan":
                countryResource = R.drawable.ic_flag_sd;
                break;
            case "sweden":
                countryResource = R.drawable.ic_flag_se;
                break;
            case "singapore":
                countryResource = R.drawable.ic_flag_sg;
                break;
            case "saint helena":
                countryResource = R.drawable.ic_flag_sh;
                break;
            case "slovenia":
                countryResource = R.drawable.ic_flag_af;
                //problemas svg
                break;
            case "svalbard and jan mayen":
                countryResource = R.drawable.ic_flag_sj;
                break;
            case "slovakia":
                countryResource = R.drawable.ic_flag_sk;
                break;
            case "sierra leone":
                countryResource = R.drawable.ic_flag_sl;
                break;
            case "san marino":
                countryResource = R.drawable.ic_flag_sm;
                break;
            case "senegal":
                countryResource = R.drawable.ic_flag_sn;
                break;
            case "somalia":
                countryResource = R.drawable.ic_flag_so;
                break;
            case "suriname":
                countryResource = R.drawable.ic_flag_sr;
                break;
            case "south sudan":
                countryResource = R.drawable.ic_flag_ss;
                break;
            case "sao tome and principe":
                countryResource = R.drawable.ic_flag_st;
                break;
            case "el salvador":
                countryResource = R.drawable.ic_flag_sv;
                break;
            case "saint maarten":
                countryResource = R.drawable.ic_flag_sx;
                break;
            case "syria":
                countryResource = R.drawable.ic_flag_sy;
                break;
            case "swaziland":
                countryResource = R.drawable.ic_flag_sz;
                break;
            case "turks and caicos islands":
                countryResource = R.drawable.ic_flag_tc;
                break;
            case "chad":
                countryResource = R.drawable.ic_flag_td;
                break;
            case "french southern territories":
                countryResource = R.drawable.ic_flag_tf;
                break;
            case "togo":
                countryResource = R.drawable.ic_flag_tg;
                break;
            case "thailand":
                countryResource = R.drawable.ic_flag_th;
                break;
            case "tajikistan":
                countryResource = R.drawable.ic_flag_tj;
                break;
            case "tokelau":
                countryResource = R.drawable.ic_flag_tk;
                break;
            case "east timor":
                countryResource = R.drawable.ic_flag_tl;
                break;
            case "turkmenistan":
                countryResource = R.drawable.ic_flag_tm;
                break;
            case "tunisia":
                countryResource = R.drawable.ic_flag_tn;
                break;
            case "tonga":
                countryResource = R.drawable.ic_flag_to;
                break;
            case "turkey":
                countryResource = R.drawable.ic_flag_tr;
                break;
            case "trinidad and tobago":
                countryResource = R.drawable.ic_flag_tt;
                break;
            case "tuvalu":
                countryResource = R.drawable.ic_flag_tv;
                break;
            case "taiwan":
                countryResource = R.drawable.ic_flag_af;
                //problemas svg
                break;
            case "tanzania":
                countryResource = R.drawable.ic_flag_tz;
                break;
            case "ukraine":
                countryResource = R.drawable.ic_flag_ua;
                break;
            case "uganda":
                countryResource = R.drawable.ic_flag_ug;
                break;
            case "us minor outlying islands":
                countryResource = R.drawable.ic_flag_um;
                break;
            case "united states":
                countryResource = R.drawable.ic_flag_us;
                break;
            case "uruguay":
                countryResource = R.drawable.ic_flag_uy;
                //problemas con svg
                break;
            case "uzbekistan":
                countryResource = R.drawable.ic_flag_uz;
                break;
            case "vatican":
                countryResource = R.drawable.ic_flag_va;
                break;
            case "saint vincent and the grenadines":
                countryResource = R.drawable.ic_flag_vc;
                break;
            case "venezuela":
                countryResource = R.drawable.ic_flag_ve;
                break;
            case "british virgin islands":
                countryResource = R.drawable.ic_flag_vg;
                break;
            case "u.s. virgin islands":
                countryResource = R.drawable.ic_flag_vi;
                break;
            case "vietnam":
                countryResource = R.drawable.ic_flag_vn;
                break;
            case "vanuatu":
                countryResource = R.drawable.ic_flag_vu;
                break;
            case "wallis and futuna":
                countryResource = R.drawable.ic_flag_wf;
                break;
            case "kosovo":
                countryResource = R.drawable.ic_flag_ws;
                break;
            case "samoa":
                countryResource = R.drawable.ic_flag_xk;
                break;
            case "yemen":
                countryResource = R.drawable.ic_flag_ye;
                break;
            case "mayotte":
                countryResource = R.drawable.ic_flag_yt;
                break;
            case "south africa":
                countryResource = R.drawable.ic_flag_za;
                break;
            case "zambia":
                countryResource = R.drawable.ic_flag_zm;
                break;
            case "zimbabwe":
                countryResource = R.drawable.ic_flag_zw;
                break;
            default:
                countryResource = R.drawable.ic_flag_default;
                break;

        }
        return countryResource;
    }
}
