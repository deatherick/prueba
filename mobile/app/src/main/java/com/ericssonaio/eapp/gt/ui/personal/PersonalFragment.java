package com.ericssonaio.eapp.gt.ui.personal;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;
import com.ericssonaio.eapp.gt.ui.jobs.CountryItemFlagResource;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static android.widget.Toast.LENGTH_LONG;

public class PersonalFragment extends Fragment implements View.OnClickListener {

    private PersonalViewModel personalViewModel;
    ProgressDialog progressDialog;
    final String url_police = "https://login2.ericsson.net/login/login.fcc?TYPE=33554433&REALMOID=06-e5f06c28-8f45-420f-b46d-e4d8c8fc0fdc&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=WXQcX1g2i6wdFIhKlrlaS6ByBwfYT1qNnMuH2DyA1OYBRzZjgm5ra7Bhw6YGxcke&TARGET=$SM$http://restapi.ericsson.net/iam/v1/peopleFinder.html";
    String url_restapi = "https://restapi.ericsson.net/iam/v1/people/SIGNUMERICSSON?format=json";
    final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
    String urlListRequest = "http://ongoing.com.gt/erick/Api/get_requests";

    String cookie = null;
    TextView textName, textMail, textId, textCountry, textTitle, textRequest;
    static String firstName, lastName, mail, id, city, title;
    static int intCountry;

    static String signum = null;
    static String pass = null;

    ImageView imgProfile, imgProfileCamera, imgCountry;
    static CountryItemFlagResource countryItemFlagResource;
    public static final int IMAGE_PICK_CODE = 1000;
    public static final int PERMISSION_CODE = 1001;
    Bitmap bmp;
    SharedPreferences prefs;
    private static final int PICK_FROM_GALLERY = 1;
    int resultSendRequest = 0;
    int pendingRequest = 0;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        personalViewModel =
                ViewModelProviders.of(this).get(PersonalViewModel.class);
        View root = inflater.inflate(R.layout.fragment_personal, container, false);

        NavigationView navigationView = (NavigationView)root.findViewById(R.id.nav_view);

        /*final TextView textView = root.findViewById(R.id.text_personal);
        personalViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("");
        progressDialog = new ProgressDialog(getContext());
        countryItemFlagResource = new CountryItemFlagResource();

        textName = (TextView) root.findViewById(R.id.text_profile_name);
        textName.setText("");

        textMail = (TextView) root.findViewById(R.id.text_profile_email);
        textMail.setText("");

        textId = (TextView) root.findViewById(R.id.text_profile_line_manager);
        textId.setText("");

        textCountry = (TextView) root.findViewById(R.id.text_profile_pais);
        textCountry.setText("");

        textTitle = (TextView) root.findViewById(R.id.text_profile_puesto);
        textTitle.setText("");

        textRequest = (TextView) root.findViewById(R.id.text_profile_solicitudes_pendientes);
        textRequest.setText("0");

        //Image profile
        imgProfile = (ImageView) root.findViewById(R.id.img_profile);
        imgProfileCamera = (ImageView) root.findViewById(R.id.img_profile_camera);
        imgCountry = (ImageView) root.findViewById(R.id.img_personal_flag);
        imgProfileCamera.setOnClickListener(this);

        prefs = getContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        //sp= getContext().getSharedPreferences("profilePicture",MODE_PRIVATE);

        if(!prefs.getString("dp","").equals("")){
            byte[] decodedString = Base64.decode(prefs.getString("dp", ""), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            imgProfile.setImageBitmap(decodedByte);
        }

        if(!prefs.getString("signume","").equals("")) {
            //byte[] data = Base64.decode(prefs.getString("signume", ""), Base64.DEFAULT);
            //signum = new String(data, StandardCharsets.UTF_8);
            signum = prefs.getString("signume", "");
        }else{signum="";}

        if(!prefs.getString("passe","").equals("")) {
            //byte[]data = Base64.decode(prefs.getString("passe", ""), Base64.DEFAULT);
            //pass = new String(data, StandardCharsets.UTF_8);
            pass = prefs.getString("passe", "");
        }else{pass="";}


        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {

            ParseURL buscarInformacion = new ParseURL();
            buscarInformacion.execute();
        } else {
            // No hay conexión a Internet en este momento
            Toast.makeText(getContext(),getString(R.string.msg_network), LENGTH_LONG).show();
        }

        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_profile_camera:
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if(getContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED){
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permissions, PERMISSION_CODE);
                    }else{
                        pickImageFromGallery();
                    }
                }else {
                    pickImageFromGallery();
                }
                break;
            default:
                break;
        }
    }

    private void pickImageFromGallery() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, IMAGE_PICK_CODE);
            }else{
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, IMAGE_PICK_CODE);
            }
        }else{
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, IMAGE_PICK_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        /*if(resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE){
            imgProfile.setImageURI(data.getData());
        }*/
        if (resultCode == RESULT_OK) {
            try { // When an Image is picked
                if (requestCode == IMAGE_PICK_CODE && resultCode == RESULT_OK
                        && null != data) {
                    // Get the Image from data

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    // Get the cursor
                    Cursor cursor = getContext().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();
                    // Set the Image in ImageView after decoding the String
                    bmp = BitmapFactory
                            .decodeFile(imgDecodableString);
                    imgProfile.setImageBitmap(bmp);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();

                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    String encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    prefs.edit().putString("dp", encodedImage).commit();
                } else {
                    Toast.makeText(getContext(), getString(R.string.msg_img_profile),
                            Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getContext(), getString(R.string.msg_internet_conetion_error), Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(getContext(), getString(R.string.msg_img_profile),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case IMAGE_PICK_CODE: //PERMISSION_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    pickImageFromGallery();
                }else {
                    Toast.makeText(getContext(), R.string.permission_denied, Toast.LENGTH_SHORT).show();
                }
        }
    }

    private class ParseURL extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialogWithTitle(getString(R.string.msg_information_title),getString(R.string.msg_information_load_detail));
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected Void doInBackground(Void... voids) {
            resultSendRequest = 0;
            pendingRequest = 0;
            String json = null; //"{\"key\":1}";;
            try {
                Document document = Jsoup.connect(url_police).get();

                Connection.Response loginPageResponse =
                        Jsoup.connect(url_police)
                                .referrer(url_police)
                                .userAgent("Mozilla/5.0")
                                .timeout(10 * 1000)
                                .followRedirects(true)
                                .execute();

                Map<String, String> mapLoginPageCookies = loginPageResponse.cookies();

                Connection.Response loginResponse = Jsoup.connect(url_police)
                        .userAgent(USER_AGENT)
                        .data("USER", signum)
                        .data("PASSWORD", pass)
                        .method(Connection.Method.POST)
                        .execute();

                Map<String, String> mapLoginPageCookies2 = loginResponse.cookies();

                //System.out.println("cookies: " + mapLoginPageCookies2.toString());
                //System.out.println("SMSESSION: " + loginResponse.cookie("SMSESSION"));

                cookie = loginResponse.cookie("SMSESSION");

                //System.out.println("Consulta datos...");

                url_restapi = url_restapi.replace("SIGNUMERICSSON", signum);

                String url_final = Jsoup.connect(url_restapi)
                        .ignoreContentType(true)
                        .cookies(mapLoginPageCookies2)
                        .followRedirects(true).execute().url().toExternalForm();

                System.out.println("Salida URL :" + url_final);

                Document doc = Jsoup.connect(url_restapi)
                        .ignoreContentType(true)
                        .cookies(mapLoginPageCookies2)
                        .get();

                //System.out.println(doc.outerHtml());

                //String data = doc.select("body").first().toString();

                //System.out.println(Jsoup.parse(doc.outerHtml(), "ISO-8859-1").select("body").text());

                JSONParser parse = new JSONParser();

                Object obj = parse.parse(Jsoup.parse(doc.outerHtml(), "ISO-8859-1").select("body").text());

                JSONArray employeeList = new JSONArray();
                employeeList.add(obj);
                //System.out.println(employeeList);

                employeeList.forEach( emp -> {
                    parseEmployeeObject((JSONObject) emp);
                });

                //Obtener request

                URL u = new URL(urlListRequest);

                org.json.JSONObject jsonObject = new org.json.JSONObject();
                jsonObject.accumulate("username", signum);
                json = jsonObject.toString();

                HttpURLConnection conn = (HttpURLConnection) u.openConnection();
                conn.setConnectTimeout(5000);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.setAllowUserInteraction(false);
                conn.setConnectTimeout(3000);
                conn.setReadTimeout(3000);
                conn.setRequestMethod("GET");

                OutputStream os = conn.getOutputStream();
                os.write(json.getBytes("UTF-8"));
                os.close();

                conn.connect();

                InputStream in = new BufferedInputStream(conn.getInputStream());

                JSONParser jsonParser = new JSONParser();
                JSONObject jsonObjects = (JSONObject)jsonParser.parse(
                        new InputStreamReader(in, "UTF-8"));

                String results = jsonObjects.toJSONString();

                Log.println(Log.INFO, "results: ", results);

                org.json.JSONObject objs = new org.json.JSONObject(results);
                org.json.JSONArray arr = objs.getJSONArray("data"); //new JSONArray(JSONResp);

                Log.println(Log.INFO, "arr.length(): ", String.valueOf(arr.length()));

                if(arr.length()>0) {
                    for (int i = 0; i < arr.length(); i++) {
                        Log.println(Log.INFO, "Request status: ", arr.getJSONObject(i).get("status").toString());
                        if(arr.getJSONObject(i).get("status").equals("Created")){
                            pendingRequest++;
                        };
                    }
                }

            } catch (IOException | ParseException e) {
                e.printStackTrace();
                resultSendRequest = 1;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgressDialogWithTitle();

            if (resultSendRequest == 0) {

                Toast.makeText(getContext(), getString(R.string.msg_updates_information_success), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
                builderOK.setTitle(getString(R.string.msg_information_title));
                builderOK.setMessage(getString(R.string.msg_updates_information_success));
                // add the buttons
                builderOK.setPositiveButton(getString(R.string.btn_ok), null);
                AlertDialog dialogOK = builderOK.create();
                dialogOK.show();

            } else {
                Toast.makeText(getContext(), getString(R.string.msg_error_update), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderError = new AlertDialog.Builder(getContext());
                builderError.setTitle(getString(R.string.msg_information_title));
                builderError.setMessage(getString(R.string.msg_error_update_try));
                // add the buttons
                builderError.setPositiveButton(getString(R.string.btn_ok), null);
                AlertDialog dialogError = builderError.create();
                dialogError.show();
                isCancelled();
            }


            if(firstName == ""){
                Toast.makeText(getContext(), getString(R.string.msg_information_not_found), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
                builderOK.setTitle(getString(R.string.msg_information_title));
                builderOK.setMessage(getString(R.string.msg_people_information_not_found));
                // add the buttons
                builderOK.setPositiveButton(getString(R.string.btn_ok), null);
                AlertDialog dialogOK = builderOK.create();
                dialogOK.show();
            }else {
                textName.setText(firstName + " " + lastName);
                textMail.setText(mail);
                textId.setText(id);
                textCountry.setText(city);
                textTitle.setText(title);
                textRequest.setText(String.valueOf(pendingRequest));
                imgCountry.setImageResource(intCountry);

                prefs = getContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("nameUser", firstName + " " + lastName);
                editor.commit();

                editor.putString("emailUser", mail);
                editor.commit();
            }


        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            hideProgressDialogWithTitle();
            resultSendRequest = 1;
        }
    }

    private static void parseEmployeeObject(JSONObject employee)
    {
        //Get employee object within list
        JSONObject employeeObject = (JSONObject) employee.get("result");

        //Get employee first name
        firstName = (String) employeeObject.get("eriGivenName");
        System.out.println(firstName);

        //Get employee last name
        lastName = (String) employeeObject.get("eriSn");
        System.out.println(lastName);

        //Get employee website name
        mail = (String) employeeObject.get("mail");
        System.out.println(mail);

        city = (String) employeeObject.get("eriCountry");
        System.out.println(city);

        title = (String) employeeObject.get("title");
        System.out.println(title);

        id = (String) employeeObject.get("employeeNumber");
        System.out.println(id);

        System.out.println((String)employeeObject.get("eriCountry"));

        intCountry = countryItemFlagResource.countryItemFlagResources((String)employeeObject.get("eriCountry"));
    }

    private void showProgressDialogWithTitle(String title,String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }
}