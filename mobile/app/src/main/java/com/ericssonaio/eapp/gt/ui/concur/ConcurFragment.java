package com.ericssonaio.eapp.gt.ui.concur;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;

import static android.widget.Toast.LENGTH_LONG;

public class ConcurFragment extends Fragment {

    private ConcurViewModel concurViewModel;
    ProgressDialog progressDialog;
    static String url = "https://www.concursolutions.com/nui/signin/v2";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        concurViewModel =
                ViewModelProviders.of(this).get(ConcurViewModel.class);
        View root = inflater.inflate(R.layout.fragment_concur, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.app_concur));
        progressDialog = new ProgressDialog(getContext());

        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            WebView mWebView = (WebView) root.findViewById(R.id.webview_concur);

            // Force links and redirects to open in the WebView instead of in a browser
            mWebView.setWebViewClient(new WebViewClient(){
                private volatile boolean timeout;
                private volatile String timeoutOnPageStartedURL;
                private volatile String timeoutCurrentURL;

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);

                    timeout = true;
                    timeoutOnPageStartedURL = url;
                    timeoutCurrentURL = view.getUrl();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (timeout) {
                                view.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        String currentURL = view.getUrl();
                                        if ((timeoutOnPageStartedURL.hashCode() == currentURL.hashCode()) ||
                                                (timeoutCurrentURL.hashCode() == currentURL.hashCode())) {
                                            // do what you want with UI
                                            showProgressDialogWithTitle(getString(R.string.msg_loading_title),getString(R.string.msg_loading_web));
                                        }
                                    }
                                });
                            }
                        }
                    }).start();
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    hideProgressDialogWithTitle();
                    timeout = false;
                }
            });

            // Enable Javascript
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            mWebView.loadUrl(url);
        } else {
            // No hay conexión a Internet en este momento
            Toast.makeText(getContext(),getString(R.string.msg_network), LENGTH_LONG).show();
        }

        return root;
    }

    private void showProgressDialogWithTitle(String title,String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }


    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }
}