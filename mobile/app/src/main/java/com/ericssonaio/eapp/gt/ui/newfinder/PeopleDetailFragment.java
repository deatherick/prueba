package com.ericssonaio.eapp.gt.ui.newfinder;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class PeopleDetailFragment extends Fragment {

    private PeopleDetailViewModel peopleDetailViewModel;
    ProgressDialog progressDialog;

    final String url = "https://login2.ericsson.net/login/login.fcc?TYPE=33554433&REALMOID=06-e5f06c28-8f45-420f-b46d-e4d8c8fc0fdc&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=WXQcX1g2i6wdFIhKlrlaS6ByBwfYT1qNnMuH2DyA1OYBRzZjgm5ra7Bhw6YGxcke&TARGET=$SM$http://restapi.ericsson.net/iam/v1/peopleFinder.html";
    String url2 = "https://restapi.ericsson.net/iam/v1/people/SIGNUMERICSSON?format=json&givenName=NAMEERICSSON&sn=APELLIDOERICSSON&mail=CORREOERICSSON&listing=full&limit=60&offset=0&sort=eriGivenName";

    final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
    SharedPreferences prefs;
    static String signum, pass, cookie = null;

    TextView sPeopleName, sPeopleLastName, sPeopleSignum, sPeopleJob, sPeopleEmail, sPeopleNumber, sPeoplePhone,
            sPeopleOrganization, sPeopleMobile, sPeopleStreet, sPeopleCountry;

    public static PeopleDetailFragment newInstance() {
        return new PeopleDetailFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        peopleDetailViewModel =
                ViewModelProviders.of(this).get(PeopleDetailViewModel.class);
        View root = inflater.inflate(R.layout.fragment_people_detail, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.msg_people_detail));

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        prefs = getContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        //sp= getContext().getSharedPreferences("profilePicture",MODE_PRIVATE);

        if (!prefs.getString("signume", "").equals("")) {
            //byte[] data = Base64.decode(prefs.getString("signume", ""), Base64.DEFAULT);
            //signum = new String(data, StandardCharsets.UTF_8);
            signum = prefs.getString("signume", "");
        } else {
            signum = "edejuli";
        }

        if (!prefs.getString("passe", "").equals("")) {
            //byte[]data = Base64.decode(prefs.getString("passe", ""), Base64.DEFAULT);
            //pass = new String(data, StandardCharsets.UTF_8);
            pass = prefs.getString("passe", "");
        } else {
            pass = "Usuario@123";
        }

        sPeopleName = (TextView) root.findViewById(R.id.people_item_detail_text_name);
        sPeopleLastName = (TextView) root.findViewById(R.id.people_item_detail_text_last_name);
        sPeopleSignum = (TextView) root.findViewById(R.id.people_item_detail_text_signum);
        sPeopleJob = (TextView) root.findViewById(R.id.people_item_detail_text_job);
        sPeopleEmail = (TextView) root.findViewById(R.id.people_item_detail_text_email);
        sPeopleNumber = (TextView) root.findViewById(R.id.people_item_detail_text_employeeNumber);
        sPeoplePhone = (TextView) root.findViewById(R.id.people_item_detail_text_phone_ericsson);
        sPeopleOrganization = (TextView) root.findViewById(R.id.people_item_detail_text_organization);
        sPeopleMobile = (TextView) root.findViewById(R.id.people_item_detail_text_mobile);
        sPeopleStreet = (TextView) root.findViewById(R.id.people_item_detail_text_street);
        sPeopleCountry = (TextView) root.findViewById(R.id.people_item_detail_text_country);

        String signumBundle = PeopleListFragment.EXTRA_UID_APPLY;
        System.out.println("Ericsson UID Apply: " + signumBundle);

        progressDialog = new ProgressDialog(getContext());

        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            showProgressDialogWithTitle(getString(R.string.msg_information_title), getString(R.string.msg_loading_information));
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    parseJSON(signumBundle);
                    hideProgressDialogWithTitle();
                }
            });
        } else {
            // No hay conexión a Internet en este momento
            Toast.makeText(getContext(), getString(R.string.msg_network), Toast.LENGTH_LONG).show();
        }

        return root;
    }

    public void parseJSON(String signumSearch) {

        try {
            Document document = Jsoup.connect(url).get();

            Connection.Response loginPageResponse =
                    Jsoup.connect(url)
                            .referrer(url)
                            .userAgent("Mozilla/5.0")
                            .timeout(10 * 1000)
                            .followRedirects(true)
                            .execute();

            Map<String, String> mapLoginPageCookies = loginPageResponse.cookies();

            Connection.Response loginResponse = Jsoup.connect(url)
                    .userAgent(USER_AGENT)
                    .data("USER", signum)
                    .data("PASSWORD", pass)
                    .method(Connection.Method.POST)
                    .execute();

            Map<String, String> mapLoginPageCookies2 = loginResponse.cookies();

            cookie = loginResponse.cookie("SMSESSION");

            String urlSearch = url2.replace("SIGNUMERICSSON", signumSearch);
            urlSearch = urlSearch.replace("NAMEERICSSON", "");
            urlSearch = urlSearch.replace("APELLIDOERICSSON", "");
            urlSearch = urlSearch.replace("CORREOERICSSON", "");

            System.out.println("URL search: " + urlSearch);

            Document doc = Jsoup.connect(urlSearch)
                    .ignoreContentType(true)
                    .cookies(mapLoginPageCookies2)
                    .get();

            JSONParser parse = new JSONParser();

            Object obj = parse.parse(Jsoup.parse(doc.outerHtml(), "ISO-8859-1").select("body").text());

            JSONObject jsonObject = new JSONObject(obj.toString());
            System.out.println("x-jsonObject: " + jsonObject);

            if (jsonObject.length() < 2) {
                JSONObject jsonObjectJob = jsonObject.getJSONObject("result");
                fillPeopleItemArray(jsonObjectJob);
            } else {
                JSONArray jsonArray = (JSONArray) jsonObject.get("result");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectList = jsonArray.getJSONObject(i);
                    fillPeopleItemArray(jsonObjectList);
                }
            }
            sendMessageInformationSearch(getString(R.string.msg_information_title),getString(R.string.msg_loading_informatino_success));
        } catch (JSONException | IOException | ParseException e) {
            e.printStackTrace();
        }

    }

    private void fillPeopleItemArray(JSONObject objt) throws JSONException {
        sPeopleName.setText(objt.getString("eriGivenName"));
        sPeopleLastName.setText(objt.getString("eriSn"));
        sPeopleSignum.setText(objt.getString("uid"));
        sPeopleJob.setText(objt.getString("title"));
        sPeopleEmail.setText(getString(R.string.msg_people_email)+objt.getString("mail"));
        sPeopleCountry.setText(getString(R.string.msg_people_country)+objt.getString("eriCountry"));
        sPeopleMobile.setText(getString(R.string.msg_people_mobile)+objt.getString("mobile"));
        sPeoplePhone.setText(getString(R.string.msg_people_phone)+objt.getString("eriTelephoneNumber"));
        sPeopleOrganization.setText(getString(R.string.msg_people_orga)+objt.getString("eriOpOrgUnitAbbreviation"));
        sPeopleStreet.setText(getString(R.string.msg_people_addres)+objt.getString("street"));
        sPeopleNumber.setText(getString(R.string.msg_people_id)+objt.getString("employeeNumber"));
    }

    private void showProgressDialogWithTitle(String title, String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    private void sendMessageInformationSearch(String title, String message) {
        AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
        builderOK.setTitle(title);
        builderOK.setMessage(message);
        // add the buttons
        builderOK.setPositiveButton(getString(R.string.btn_ok), null);
        AlertDialog dialogOK = builderOK.create();
        dialogOK.show();
    }
}
