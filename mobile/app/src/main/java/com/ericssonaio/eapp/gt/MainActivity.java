package com.ericssonaio.eapp.gt;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.ericssonaio.eapp.gt.ui.certifications.CertificationFragment;
import com.ericssonaio.eapp.gt.ui.concur.ConcurFragment;
import com.ericssonaio.eapp.gt.ui.dashboard.DashBoardFragment;
import com.ericssonaio.eapp.gt.ui.elearning.ElearningFragment;
import com.ericssonaio.eapp.gt.ui.finder.FinderFragment;
import com.ericssonaio.eapp.gt.ui.fiori.FioriFragment;
import com.ericssonaio.eapp.gt.ui.personal.PersonalFragment;
import com.ericssonaio.eapp.gt.ui.request.RequestFragment;
import com.ericssonaio.eapp.gt.ui.salas.SalasFragment;
import com.ericssonaio.eapp.gt.ui.settings.SettingsFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    SharedPreferences prefs, sp;
    FloatingActionButton fab;
    Bitmap bmp;
    ImageView imgLittleProfile;
    TextView navUserName, navUserEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
                Fragment fragment = null;
                fragment = new DashBoardFragment();
                replaceFragment(fragment);
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_dashboard, R.id.nav_personal, R.id.nav_request,
                R.id.nav_certifications, R.id.nav_fiori, R.id.nav_concur, R.id.nav_salas, R.id.nav_elearnig,
                R.id.nav_settings, R.id.nav_poweroff)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        View hView =  navigationView.getHeaderView(0);
        //TextView nav_user = (TextView)hView.findViewById(R.id.text_nav);
        //        //nav_user.setText("Usuario");

        imgLittleProfile = (ImageView) hView.findViewById(R.id.img_profile_little);
        navUserName = (TextView)hView.findViewById(R.id.text_nav_name);
        navUserEmail = (TextView)hView.findViewById(R.id.text_nav_email);

        prefs = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        //sp= getApplicationContext().getSharedPreferences("profilePicture",MODE_PRIVATE);

        if(prefs.getBoolean("isCheckedNightMode", false)){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            //startActivity(new Intent(getApplicationContext(), MainActivity.class));
            //SharedPreferences.Editor editor = prefs.edit();

            //editor.putBoolean("isCheckedNightMode", switch_change.isChecked());
            //editor.commit();

            //getActivity().finish();
        }else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            //startActivity(new Intent(getApplicationContext(), MainActivity.class));
            //SharedPreferences.Editor editor = prefs.edit();

            //editor.putBoolean("isCheckedNightMode", switch_change.isChecked());
            //editor.commit();

            //getActivity().finish();
        }

        if(!prefs.getString("dp","").equals("")){
            byte[] decodedString = Base64.decode(prefs.getString("dp", ""), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            imgLittleProfile.setImageBitmap(decodedByte);
        }

        if(!prefs.getString("nameUser","").equals("")){
            navUserName.setText(prefs.getString("nameUser", ""));
        }

        if(!prefs.getString("emailUser","").equals("")){
            navUserEmail.setText(prefs.getString("emailUser", ""));
        }

        Intent i = new Intent(this, RegistrationService.class);
        startService(i);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()){
            case R.id.action_settings:
                //Toast.makeText(this, "Item selectd", Toast.LENGTH_SHORT).show();
                //return true;

                fragment = new SettingsFragment();
                replaceFragment(fragment);
                //fragment.onOptionsItemSelected(item);
                return true;
            case R.id.action_poweroof:
                finishAndRemoveTask();
                //appExit();
                return true;
            default:
                //return false;
                return super.onOptionsItemSelected(item);

        }
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction  transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void appExit () {
        this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }  //close method

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Fragment fragment;
        switch (id) {
            case R.id.nav_dashboard:

                fragment = new DashBoardFragment();
                replaceFragment(fragment);
                break;
            case  R.id.nav_personal:

                fragment = new PersonalFragment();
                replaceFragment(fragment);
                break;
            case R.id.nav_request:

                fragment = new RequestFragment();
                replaceFragment(fragment);
                break;
            case  R.id.nav_certifications:

                fragment = new CertificationFragment();
                replaceFragment(fragment);
                break;
            case R.id.nav_fiori:

                fragment = new FioriFragment();
                replaceFragment(fragment);
                break;
            case R.id.nav_concur:

                fragment = new ConcurFragment();
                replaceFragment(fragment);
                break;
            case R.id.nav_salas:

                fragment = new SalasFragment();
                replaceFragment(fragment);
                break;
            case R.id.nav_elearnig:

                fragment = new ElearningFragment();
                replaceFragment(fragment);
                break;
            case  R.id.nav_finder:

                fragment = new FinderFragment();
                replaceFragment(fragment);
                break;
            case  R.id.nav_settings:

                fragment = new SettingsFragment();
                replaceFragment(fragment);
                break;
            case  R.id.nav_poweroff:
                finishAndRemoveTask();
                break;
            default:
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void clearStack() {
        //Here we are clearing back stack fragment entries
        /*int backStackEntry = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntry > 0) {
            for (int i = 0; i < backStackEntry; i++) {
                getSupportFragmentManager().popBackStackImmediate();
            }
        }

        //Here we are removing all the fragment that are shown here
        if (getSupportFragmentManager().getFragments() != null && getSupportFragmentManager().getFragments().size() > 0) {
            for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {
                Fragment mFragment = getSupportFragmentManager().getFragments().get(i);
                if (mFragment != null) {
                    getSupportFragmentManager().beginTransaction().remove(mFragment).commit();
                }
            }
        }*/
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

}
