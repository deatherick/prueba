package com.ericssonaio.eapp.gt.ui.request;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static android.widget.Toast.LENGTH_LONG;

public class RequestSearchFragment extends Fragment {

    private RequestSearchViewModel requestSearchViewModel;
    private SimpleAdapterRequest adpt;
    SharedPreferences prefs;
    static String signum = null;
    String url = "http://ongoing.com.gt/erick/Api/get_requests";
    TextView namePage;
    ProgressDialog progressDialog;
    ListView lView;
    int resultSendRequest = 0;
    int totalRequest = 0;

    ArrayList<Request> arrayList = new ArrayList<Request>();

    public static RequestSearchFragment newInstance() {
        return new RequestSearchFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        requestSearchViewModel =
                ViewModelProviders.of(this).get(RequestSearchViewModel.class);
        View root = inflater.inflate(R.layout.request_search_fragment, container, false);

        final TextView textView = root.findViewById(R.id.text_search_request);
        requestSearchViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.msg_request_search));
        progressDialog = new ProgressDialog(getContext());

        namePage = root.findViewById(R.id.text_search_request);
        namePage.setText(getString(R.string.msg_request_find));

        prefs = getContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        //byte[] data = Base64.decode(prefs.getString("signume", ""), Base64.DEFAULT);
        //signum = new String(data, StandardCharsets.UTF_8);
        signum = prefs.getString("signume", "");

        adpt  = new SimpleAdapterRequest(new ArrayList<Request>(), getContext());
        lView = (ListView) root.findViewById(R.id.listview);
        lView.setClickable(true);
        lView.setAdapter(adpt);

        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            // Exec async load task
            AsyncListViewLoader enviandoInformacion = new AsyncListViewLoader();
            showProgressDialogWithTitle(getString(R.string.msg_information_title),getString(R.string.msg_search_information));
            enviandoInformacion.execute(url);

        } else {
            // No hay conexión a Internet en este momento
            Toast.makeText(getContext(),getString(R.string.msg_network), LENGTH_LONG).show();
        }

        return root;
    }

    private class AsyncListViewLoader extends AsyncTask<String, Void, List<Request>> {


        @Override
        protected void onPostExecute(List<Request> result) {
            super.onPostExecute(result);
            adpt.setItemList(result);
            adpt.notifyDataSetChanged();
            arrayList.addAll(result);
            hideProgressDialogWithTitle();

            if (resultSendRequest == 0) {

                Toast.makeText(getContext(), getString(R.string.msg_search_success), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
                builderOK.setTitle(getString(R.string.msg_information_title));
                builderOK.setMessage(getString(R.string.msg_search_success));
                // add the buttons
                builderOK.setPositiveButton(getString(R.string.btn_ok), null);
                AlertDialog dialogOK = builderOK.create();
                dialogOK.show();

                namePage.setText(getString(R.string.msg_request_find) + String.valueOf(totalRequest));
            } else {
                Toast.makeText(getContext(), getString(R.string.msg_search_error_detail), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderError = new AlertDialog.Builder(getContext());
                builderError.setTitle(getString(R.string.msg_search_error_title));
                builderError.setMessage(getString(R.string.msg_search_error_detail));
                // add the buttons
                builderError.setPositiveButton(getString(R.string.btn_ok), null);
                AlertDialog dialogError = builderError.create();
                dialogError.show();
                namePage.setText(getString(R.string.msg_request_find) + "0");
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialogWithTitle(getString(R.string.msg_information_title),getString(R.string.msg_search_information));
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            hideProgressDialogWithTitle();
            resultSendRequest = 1;
        }

        @Override
        protected List<Request> doInBackground(String... params) {
            List<Request> result = new ArrayList<Request>();
            String json = null; //"{\"key\":1}";;
            resultSendRequest = 0;
            try {

                URL u = new URL(params[0]);

                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("username", "edejuli");
                json = jsonObject.toString();

                HttpURLConnection conn = (HttpURLConnection) u.openConnection();
                conn.setConnectTimeout(5000);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.setAllowUserInteraction(false);
                conn.setConnectTimeout(3000);
                conn.setReadTimeout(3000);
                conn.setRequestMethod("GET");

                OutputStream os = conn.getOutputStream();
                os.write(json.getBytes("UTF-8"));
                os.close();

                conn.connect();
                /*InputStream is = new BufferedInputStream(conn.getInputStream());

                // Read the stream
                byte[] b = new byte[1024];
                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                while ( is.read(b) != -1)
                    baos.write(b);

                String JSONResp = new String(baos.toByteArray());
                JSONResp = JSONResp.replaceAll("<.*?>|\u00a0", "");

                JSONObject obj = new JSONObject(JSONResp);
                //JSONObject start = new JSONObject(String.valueOf(obj));

                Log.println(Log.INFO, "JSON 1: ", JSONResp);*/

                InputStream in = new BufferedInputStream(conn.getInputStream());
                String results = inputStreamToString(in);

                JSONObject obj = new JSONObject(results);
                JSONArray arr = obj.getJSONArray("data"); //new JSONArray(JSONResp);
                Log.println(Log.INFO, "Array: ", arr.toString(2));
                Log.println(Log.INFO, "Array lenght: ", String.valueOf(arr.length()));

                totalRequest = arr.length();

                if(arr.length()>0) {
                    for (int i = 0; i < arr.length(); i++) {
                        result.add(convertRequest(arr.getJSONObject(i)));
                    }
                }

                return result;
            }
            catch(Throwable t) {
                t.printStackTrace();
                resultSendRequest = 1;
            }
            return null;
        }

        private Request convertRequest(JSONObject obj) throws JSONException {
            String id = obj.getString("id");
            String status = obj.getString("status");
            String title = obj.getString("title");
            String created_timestamp = obj.getString("created_timestamp");
            String total_days = obj.getString("total_days");
            String img = null;

            Request request = new Request(getString(R.string.msg_item_request_id) +id, status, getString(R.string.msg_item_request_type)+title, getString(R.string.msg_item_request_success)+created_timestamp, getString(R.string.msg_selected_days)+total_days, img);

            //arrayList.add(request);

            //return new Request("Solicitud °: " +id, status, "Tipo: "+title, "Ingresada: "+created_timestamp, "Días solicitados: "+total_days, img);
            return request;
        }

    }

    private String inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();

        InputStreamReader isr = new InputStreamReader(is);

        BufferedReader rd = new BufferedReader(isr);

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer.toString();
    }

    private void showProgressDialogWithTitle(String title,String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }


    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.main, menu);

        MenuItem itemSearch = menu.findItem(R.id.action_search);
        //itemSearch.setVisible(true);

        SearchView searchView = (SearchView) itemSearch.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)){
                    adpt.filter("");
                    lView.clearTextFilter();
                }else {
                    adpt.filter(newText);
                }
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if(id == R.id.action_search){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
