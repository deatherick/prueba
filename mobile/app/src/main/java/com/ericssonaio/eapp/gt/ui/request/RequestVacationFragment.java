package com.ericssonaio.eapp.gt.ui.request;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.transition.Fade;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;
import com.ericssonaio.eapp.gt.notification.model.NotificationClass;
import com.squareup.timessquare.CalendarPickerView;
import com.thekhaeng.pushdownanim.PushDownAnim;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class RequestVacationFragment extends Fragment {

    private RequestVacationViewModel requestVacationViewModel;
    TextView selectedDays;
    TextView totalDays;
    Button confirmSelectedDates;
    Button sendRequest;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    int days = 0;
    SharedPreferences prefs;
    static String signum = null;
    String url = "http://ongoing.com.gt/erick/Api/create_request";
    String textDays;
    String tittleRequest = null;
    int requestType = 1;
    String notes = null;
    int resultSendRequest = 0;
    ProgressDialog progressDialog;
    NotificationClass notificationClass;
    CalendarPickerView datePicker;
    CheckBox chbPaternity, chbCompensatory, chbFamily, chbMarriege, chbLeave;

    public static RequestVacationFragment newInstance() {
        return new RequestVacationFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        requestVacationViewModel =
                ViewModelProviders.of(this).get(RequestVacationViewModel.class);
        View root = inflater.inflate(R.layout.request_vacation_fragment, container, false);

        /*final TextView textView = root.findViewById(R.id.text_vacation_request);
        requestVacationViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.msg_vacation_title));
        progressDialog = new ProgressDialog(getContext());

        selectedDays = (TextView) root.findViewById(R.id.text_vacation_days);
        totalDays = (TextView) root.findViewById(R.id.text_vacation_resumen);
        confirmSelectedDates = (Button) root.findViewById(R.id.btn_aceptar_dias);
        sendRequest = (Button) root.findViewById(R.id.btn_sendRequestVacation);
        chbPaternity = (CheckBox) root.findViewById(R.id.chb_paternidad);
        chbCompensatory = (CheckBox) root.findViewById(R.id.chb_compensatorio);
        chbFamily = (CheckBox) root.findViewById(R.id.chb_deceso);
        chbMarriege = (CheckBox) root.findViewById(R.id.chb_matrimonio);
        chbLeave = (CheckBox) root.findViewById(R.id.chb_sin_salario);

        prefs = getContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        byte[] data = Base64.decode(prefs.getString("signume", ""), Base64.DEFAULT);
        signum = new String(data, StandardCharsets.UTF_8);

        tittleRequest = getString(R.string.msg_vacation_title);
        notes = getString(R.string.msg_vacation_detail);

        Date today = new Date();

        Calendar nextMonth = Calendar.getInstance();
        nextMonth.add(Calendar.MONTH, 1);

        datePicker = root.findViewById(R.id.calendar_vacation);
        datePicker.init(today, nextMonth.getTime())
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                .withSelectedDate(today);

        datePicker.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                //String selectedDate = DateFormat.getDateInstance(DateFormat.FULL).format(date);

                Calendar calSelected = Calendar.getInstance();
                calSelected.setTime(date);

                String selectedDate = "" + calSelected.get(Calendar.DAY_OF_MONTH)
                        + "/" + (calSelected.get(Calendar.MONTH) + 1)
                        + "/" + calSelected.get(Calendar.YEAR);

                Toast.makeText(getContext(), selectedDate, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here
        }

        final List<Date> dates = datePicker.getSelectedDates();

        /*confirmSelectedDates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int j = 0;
                Date tempDate;
                String formattedDate;

                days = datePicker.getSelectedDates().size();

                totalDays.setText("Total días: " + datePicker.getSelectedDates().size());
                selectedDays.setText("Días seleccionados: ");
                textDays = null;

                for (int i = 0; i < datePicker.getSelectedDates().size(); i++) {
                    //here you can fetch all dates
                    if (i == 0) {
                        selectedDays.setText("Días seleccionados: " + sdf.format(datePicker.getSelectedDates().get(i)));
                        textDays = sdf.format(datePicker.getSelectedDates().get(i));
                    } else {
                        selectedDays.setText(selectedDays.getText() + "," + sdf.format(datePicker.getSelectedDates().get(i)));
                        textDays = textDays + "," + sdf.format(datePicker.getSelectedDates().get(i));
                    }
                }
            }
        });*/

        /*sendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
                    if (days >= 1) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Enviar solicitud");
                        builder.setMessage("Está seguro que desea enviar está solicitud?");
                        // add the buttons
                        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // do something like...

                                AsyncListViewLoader enviandoInformacion = new AsyncListViewLoader();
                                enviandoInformacion.execute(url);
                            }
                        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // do something like...
                                Toast.makeText(getContext(), "Solicitud cancelada.", Toast.LENGTH_SHORT).show();
                            }
                        });
                        // create and show the alert dialog
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Información");
                        builder.setMessage("Debe seleccionar al menos un día.");
                        // add the buttons
                        builder.setPositiveButton("Aceptar", null);
                        // create and show the alert dialog
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                } else {
                    // No hay conexión a Internet en este momento
                    Toast.makeText(getContext(), "Revisa tú conexión a internet e intenta nuevamente.", LENGTH_LONG).show();
                }
            }
        });*/

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionSet set = new TransitionSet()
                            //.addTransition(new Scale())
                            .addTransition(new Fade())
                            .setDuration(200)
                            .setInterpolator(new OvershootInterpolator(1.0f));
                    TransitionManager.beginDelayedTransition(container, set);
                    TransitionManager.beginDelayedTransition(container);
                    sendRequest.setVisibility(View.VISIBLE);
                    confirmSelectedDates.setVisibility(View.VISIBLE);
                }
            }
        }, 1000);

        PushDownAnim.setPushDownAnimTo(sendRequest, confirmSelectedDates)
                .setScale(PushDownAnim.MODE_STATIC_DP, 2)
                .setDurationPush(PushDownAnim.DEFAULT_PUSH_DURATION)
                .setDurationRelease(PushDownAnim.DEFAULT_RELEASE_DURATION)
                .setInterpolatorPush(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setInterpolatorRelease(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setOnClickListener(getClickListener())
                .setOnLongClickListener(getLongClickListener());

        return root;
    }

    private class AsyncListViewLoader extends AsyncTask<String, Void, List<Request>> {

        @Override
        protected void onPostExecute(List<Request> result) {
            super.onPostExecute(result);
            hideProgressDialogWithTitle();
            notificationClass = new NotificationClass();

            selectedDays.setText(getString(R.string.msg_selected_days));
            totalDays.setText(getString(R.string.msg_total_days));

            uncheckItems();
            if (resultSendRequest == 0) {

                Toast.makeText(getContext(), getString(R.string.msg_success_send_request), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
                builderOK.setTitle(getString(R.string.msg_information_title));
                builderOK.setMessage(getString(R.string.msg_success_send_request));
                // add the buttons
                builderOK.setPositiveButton(getString(R.string.btn_ok), null);
                AlertDialog dialogOK = builderOK.create();
                dialogOK.show();

                //notificationClass.sendNotification("", getString(R.string.msg_request_enter), getContext());
                notificationClass.sendEmail(getString(R.string.email_user), getString(R.string.email_pass), getString(R.string.pdf_email_notification_title), getString(R.string.msg_request_enter), getString(R.string.email_to));
            } else {
                Toast.makeText(getContext(), getString(R.string.msg_error_sending_request_detail), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderError = new AlertDialog.Builder(getContext());
                builderError.setTitle(getString(R.string.msg_error_sending_request_title));
                builderError.setMessage(getString(R.string.msg_error_sending_request_detail));
                // add the buttons
                builderError.setPositiveButton(getString(R.string.btn_ok), null);
                AlertDialog dialogError = builderError.create();
                dialogError.show();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialogWithTitle(getString(R.string.msg_information_title), getString(R.string.msg_sending_request));
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            hideProgressDialogWithTitle();
            resultSendRequest = 1;
        }

        @Override
        protected List<Request> doInBackground(String... params) {
            List<Request> result = new ArrayList<Request>();
            String json = null; //"{\"key\":1}";;
            resultSendRequest = 0;
            try {

                URL u = new URL(params[0]);

                JSONObject jsonObjectDays = new JSONObject();
                jsonObjectDays.accumulate("days", textDays);

                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("created_by", "edejuli");
                jsonObject.accumulate("title", tittleRequest);
                jsonObject.accumulate("request_type", requestType);
                jsonObject.accumulate("total_days", days);
                jsonObject.accumulate("calendar", jsonObjectDays);
                jsonObject.accumulate("observations", notes);
                json = jsonObject.toString();

                Log.println(Log.INFO, "JSON request: ", json);

                HttpURLConnection conn = (HttpURLConnection) u.openConnection();
                conn.setConnectTimeout(5000);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.setAllowUserInteraction(false);
                conn.setConnectTimeout(3000);
                conn.setReadTimeout(3000);
                conn.setRequestMethod("GET");

                OutputStream os = conn.getOutputStream();
                os.write(json.getBytes("UTF-8"));
                os.close();

                conn.connect();

                //Log.println(Log.INFO, "JSON 1: ", JSONResp);

                InputStream in = new BufferedInputStream(conn.getInputStream());
                String results = inputStreamToString(in);

                Log.println(Log.INFO, "Result send request: ", results);

                JSONObject obj = new JSONObject(results);

                Log.println(Log.INFO, "Code result: ", obj.getString("code"));

                return result;
            } catch (Throwable t) {
                t.printStackTrace();
                resultSendRequest = 1;
            }
            return null;
        }
    }

    private String inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();

        InputStreamReader isr = new InputStreamReader(is);

        BufferedReader rd = new BufferedReader(isr);

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer.toString();
    }

    private void showProgressDialogWithTitle(String title, String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @NonNull
    private View.OnClickListener getClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == confirmSelectedDates) {
                    Toast.makeText(getContext(), getString(R.string.msg_confirmed_days), Toast.LENGTH_SHORT).show();

                    int j = 0;
                    Date tempDate;
                    String formattedDate;

                    days = datePicker.getSelectedDates().size();

                    totalDays.setText(getString(R.string.msg_total_days) + datePicker.getSelectedDates().size());
                    selectedDays.setText(getString(R.string.msg_selected_days));
                    textDays = null;

                    for (int i = 0; i < datePicker.getSelectedDates().size(); i++) {
                        //here you can fetch all dates
                        if (i == 0) {
                            selectedDays.setText(getString(R.string.msg_selected_days) + sdf.format(datePicker.getSelectedDates().get(i)));
                            textDays = sdf.format(datePicker.getSelectedDates().get(i));
                        } else {
                            selectedDays.setText(selectedDays.getText() + "," + sdf.format(datePicker.getSelectedDates().get(i)));
                            textDays = textDays + "," + sdf.format(datePicker.getSelectedDates().get(i));
                        }
                    }

                } else if (view == sendRequest) {
                    if (validateNetwork()) {
                        Toast.makeText(getContext(), getString(R.string.msg_sending_request), Toast.LENGTH_SHORT).show();
                        if (days >= 1) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle(getString(R.string.msg_error_sending_request_title));
                            builder.setMessage(getString(R.string.msg_request_question));
                            // add the buttons
                            builder.setPositiveButton(getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // do something like...

                                    AsyncListViewLoader enviandoInformacion = new AsyncListViewLoader();
                                    enviandoInformacion.execute(url);
                                }
                            }).setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // do something like...
                                    Toast.makeText(getContext(), getString(R.string.msg_cancel_request_detail), Toast.LENGTH_SHORT).show();
                                }
                            });
                            // create and show the alert dialog
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle(getString(R.string.msg_information_title));
                            builder.setMessage(getString(R.string.msg_question_days));
                            // add the buttons
                            builder.setPositiveButton(getString(R.string.btn_ok), null);
                            // create and show the alert dialog
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.msg_network), Toast.LENGTH_SHORT).show();
                        sendMessageInformationNull(getString(R.string.msg_information_title), getString(R.string.msg_network));
                    }
                }
            }

        };
    }

    private boolean validateNetwork() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }


    private void sendMessageInformationNull(String title, String message) {
        AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
        builderOK.setTitle(title);
        builderOK.setMessage(message);
        // add the buttons
        builderOK.setPositiveButton(getString(R.string.btn_ok), null);
        AlertDialog dialogOK = builderOK.create();
        dialogOK.show();
    }

    private void uncheckItems(){
        chbPaternity.setChecked(false);
        chbCompensatory.setChecked(false);
        chbFamily.setChecked(false);
        chbMarriege.setChecked(false);
        chbLeave.setChecked(false);
    }

    @NonNull
    private View.OnLongClickListener getLongClickListener() {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (view == confirmSelectedDates) {
                    Toast.makeText(getContext(), getString(R.string.msg_confirmed_days), Toast.LENGTH_SHORT).show();

                    int j = 0;
                    Date tempDate;
                    String formattedDate;

                    days = datePicker.getSelectedDates().size();

                    totalDays.setText(getString(R.string.msg_total_days) + datePicker.getSelectedDates().size());
                    selectedDays.setText(getString(R.string.msg_selected_days));
                    textDays = null;

                    for (int i = 0; i < datePicker.getSelectedDates().size(); i++) {
                        //here you can fetch all dates
                        if (i == 0) {
                            selectedDays.setText(getString(R.string.msg_selected_days) + sdf.format(datePicker.getSelectedDates().get(i)));
                            textDays = sdf.format(datePicker.getSelectedDates().get(i));
                        } else {
                            selectedDays.setText(selectedDays.getText() + "," + sdf.format(datePicker.getSelectedDates().get(i)));
                            textDays = textDays + "," + sdf.format(datePicker.getSelectedDates().get(i));
                        }
                    }

                } else if (view == sendRequest) {
                    if (validateNetwork()) {
                        Toast.makeText(getContext(), getString(R.string.msg_sending_request), Toast.LENGTH_SHORT).show();

                        if (days >= 1) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle(getString(R.string.msg_send_request_detail));
                            builder.setMessage(getString(R.string.msg_question_send_request));
                            // add the buttons
                            builder.setPositiveButton(getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // do something like...

                                    AsyncListViewLoader enviandoInformacion = new AsyncListViewLoader();
                                    enviandoInformacion.execute(url);
                                }
                            }).setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // do something like...
                                    Toast.makeText(getContext(), getString(R.string.msg_cancel_request_detail), Toast.LENGTH_SHORT).show();
                                }
                            });
                            // create and show the alert dialog
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle(getString(R.string.msg_information_title));
                            builder.setMessage(getString(R.string.msg_question_days));
                            // add the buttons
                            builder.setPositiveButton(getString(R.string.btn_ok), null);
                            // create and show the alert dialog
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.msg_network), Toast.LENGTH_SHORT).show();
                        sendMessageInformationNull(getString(R.string.msg_information_title), getString(R.string.msg_network));
                    }

                }
                return true; // true: not effect to single click
            }
        };
    }

}
