package com.ericssonaio.eapp.gt.ui.jobs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ericssonaio.eapp.gt.R;

import java.util.ArrayList;

public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.JobsViewHolder> {

    private Context mContext;
    private ArrayList<JobsItem> mJobsList;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }

    public JobsAdapter(Context context, ArrayList<JobsItem> jobsList){
        mContext = context;
        mJobsList = jobsList;
    }

    @NonNull
    @Override
    public JobsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.jobs_item, parent, false);
        return new JobsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JobsViewHolder holder, int position) {
        JobsItem jobsItem = mJobsList.get(position);

        String jobName = jobsItem.getJobName();
        String jobCountry = jobsItem.getJobCountry();
        String jobId = jobsItem.getJobId();
        String jobPostedDate = jobsItem.getJobPostedDate();
        String jobUrlApply = jobsItem.getJobUrlApply();
        int img = jobsItem.getImg();

        holder.mJobName.setText(jobName);
        holder.mJobCountry.setText(jobCountry);
        holder.mJobPostedDate.setText(jobPostedDate);
        holder.mJobId.setText(jobId);
        holder.mJobUrlApply.setText(jobUrlApply);
        holder.mImage.setImageResource(img);
    }

    @Override
    public int getItemCount() {
        return mJobsList.size();
    }

    public class JobsViewHolder extends RecyclerView.ViewHolder{

        public TextView mJobName;
        public TextView mJobCountry;
        public TextView mJobPostedDate;
        public TextView mJobId;
        public TextView mJobUrlApply;
        public ImageView mImage;

        public JobsViewHolder(@NonNull View itemView) {
            super(itemView);
            mJobName = itemView.findViewById(R.id.jobs_item_text_job);
            mJobCountry = itemView.findViewById(R.id.jobs_item_text_country);
            mJobId = itemView.findViewById(R.id.jobs_item_text_id);
            mJobPostedDate = itemView.findViewById(R.id.jobs_item_text_posted_date);
            mJobUrlApply = itemView.findViewById(R.id.jobs_item_text_url);
            mImage = itemView.findViewById(R.id.img_jobs_flag);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            onItemClickListener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

}
