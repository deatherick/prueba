package com.ericssonaio.eapp.gt;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ericssonaio.eapp.gt.introscreen.Onboarding;

import java.util.Locale;

public class LanguageSelection extends AppCompatActivity implements View.OnClickListener {
    private static Button english, spanish;
    private static TextView chooseText, ericssonText, textAIO;
    private static Locale myLocale;

    //Shared Preferences Variables
    private static final String Locale_Preference = "Locale Preference";
    private static final String Locale_KeyValue = "Saved Locale";
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_language);


        if (restorePrefData()){
            Intent onBoarding = new Intent(getApplicationContext(), Onboarding.class);
            startActivity(onBoarding);
        }else{
            initViews();
            setListeners();
            loadLocale();
        }
    }

    //Initiate all views
    private void initViews() {
        sharedPreferences = getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        Typeface customEricssonFont = Typeface.createFromAsset(getAssets(), "fonts/ericsson.ttf");

        chooseText = (TextView) findViewById(R.id.choose_text);
        ericssonText = (TextView) findViewById(R.id.ericsson_text);
        textAIO = (TextView) findViewById(R.id.ericsson_aio);
        english = (Button) findViewById(R.id.english);
        spanish = (Button) findViewById(R.id.spanish);

        ericssonText.setTypeface(customEricssonFont);
        textAIO.setTypeface(customEricssonFont);
    }

    //Set Click Listener
    private void setListeners() {
        english.setOnClickListener(this);
        spanish.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String lang = "en";//Default Language

        switch (view.getId()) {
            case R.id.english:
                lang = "en";
                break;
            case R.id.spanish:
                lang = "es";
                break;
        }

        changeLocale(lang);//Change Locale on selection basis
        startActivity(new Intent(getApplication(), Onboarding.class));
        savePrefsData();
        finish();
    }

    //Change Locale
    public void changeLocale(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);//Set Selected Locale
        saveLocale(lang);//Save the selected locale
        Locale.setDefault(myLocale);//set new locale as default
        Configuration config = new Configuration();//get Configuration
        config.locale = myLocale;//set config locale as selected locale
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());//Update the config
        updateTexts();//Update texts according to locale
    }

    //Save locale method in preferences
    public void saveLocale(String lang) {
        editor.putString(Locale_KeyValue, lang);
        editor.commit();
    }

    //Get locale method in preferences
    public void loadLocale() {
        String language = sharedPreferences.getString(Locale_KeyValue, "");
        changeLocale(language);
    }

    //Update text methods
    private void updateTexts() {
        chooseText.setText(R.string.tap_text);
        english.setText(R.string.btn_en);
        spanish.setText(R.string.btn_es);
    }

    private boolean restorePrefData() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        Boolean isIntroActivityOpenedBefore = pref.getBoolean("isLanguageOpened", false);
        return isIntroActivityOpenedBefore;
    }

    private void savePrefsData() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putBoolean("isLanguageOpened", true);
        editor.commit();

        /*editor.putString("signume", encodedSignum);
        editor.commit();

        editor.putString("passe", encodedPass);
        editor.commit();*/
    }
}
