package com.ericssonaio.eapp.gt.introscreen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;
import com.ericssonaio.eapp.gt.login;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class Onboarding extends AppCompatActivity {

    private ViewPager screenPager;
    IntroViewPagerAdapter introViewPagerAdapter;
    TabLayout tabIndicator;
    Button btnNext;
    int position = 0;
    Button btnGetStarted;
    Animation btnAnim;
    //String encodedSignum = Base64.encodeToString("edejuli".getBytes(), Base64.DEFAULT);
    //String encodedPass = Base64.encodeToString("Usuario@123".getBytes(), Base64.DEFAULT);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (restorePrefData()){
            Intent mainActivity = new Intent(getApplicationContext(), login.class);
            startActivity(mainActivity);
            finish();
        }

        setContentView(R.layout.activity_onboarding);

        //getSupportActionBar().hide();

        //ini views
        btnNext = findViewById(R.id.btn_next);
        btnGetStarted = findViewById(R.id.btn_get_started);
        tabIndicator = findViewById(R.id.tab_indicator);
        btnAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.button_animation);

        //setup viewpager
        final List<ScreenItem> mList = new ArrayList<>();
        mList.add(new ScreenItem(getString(R.string.on__title_concur), getString(R.string.on__detail_concur), R.drawable.concur));
        mList.add(new ScreenItem(getString(R.string.on__title_fiori), getString(R.string.on__detail_finder), R.drawable.fiori));
        mList.add(new ScreenItem(getString(R.string.on__title_meeting), getString(R.string.on__detail_meeting), R.drawable.meeting));
        mList.add(new ScreenItem(getString(R.string.on__title_learning), getString(R.string.on__detail_learning), R.drawable.elearning));
        mList.add(new ScreenItem(getString(R.string.on__title_finder), getString(R.string.on__detail_finder), R.drawable.peoplefinderericsson));
        mList.add(new ScreenItem(getString(R.string.on__title_rrhh), getString(R.string.on__detail_rrhh), R.drawable.requestrrhh));
        mList.add(new ScreenItem(getString(R.string.on__title_job), getString(R.string.on__detail_job), R.drawable.joboffer));

        screenPager = findViewById(R.id.viewpager_screen);
        introViewPagerAdapter = new IntroViewPagerAdapter(this, mList);
        screenPager.setAdapter(introViewPagerAdapter);

        tabIndicator.setupWithViewPager(screenPager);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = screenPager.getCurrentItem();
                if(position < mList.size()){
                    position++;
                    screenPager.setCurrentItem(position);
                }
                if(position == mList.size()-1){
                    loadLastScreen();
                }
            }
        });

        tabIndicator.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==mList.size()-1){
                    loadLastScreen();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivity = new Intent(getApplicationContext(), login.class);
                startActivity(mainActivity);

                savePrefsData();
                finish();
            }
        });
    }

    private boolean restorePrefData() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        Boolean isIntroActivityOpenedBefore = pref.getBoolean("isIntroOpened", false);
        return isIntroActivityOpenedBefore;
    }

    private void savePrefsData() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putBoolean("isIntroOpened", true);
        editor.commit();

        /*editor.putString("signume", encodedSignum);
        editor.commit();

        editor.putString("passe", encodedPass);
        editor.commit();*/
    }

    private void loadLastScreen() {
        btnNext.setVisibility(View.INVISIBLE);
        btnGetStarted.setVisibility(View.VISIBLE);
        tabIndicator.setVisibility(View.INVISIBLE);

        btnGetStarted.setAnimation(btnAnim);
    }
}
