package com.ericssonaio.eapp.gt.ui.newfinder;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.transition.Fade;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;
import com.google.android.material.textfield.TextInputEditText;
import com.thekhaeng.pushdownanim.PushDownAnim;

public class PeopleFragment extends Fragment {

    ProgressDialog progressDialog;

    Button searchButtonPeople, clearButtonPeople;

    TextInputEditText peopleName, peopleLastName, peopleSignum, peopleEmail;

    private PeopleViewModel peopleViewModel;

    public static PeopleFragment newInstance() {
        return new PeopleFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        peopleViewModel =
                ViewModelProviders.of(this).get(PeopleViewModel.class);
        View root = inflater.inflate(R.layout.fragment_people, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("");

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        peopleName = (TextInputEditText) root.findViewById(R.id.input_people_name);
        peopleLastName = (TextInputEditText) root.findViewById(R.id.input_people_last_name);
        peopleSignum = (TextInputEditText) root.findViewById(R.id.input_people_signum);
        peopleEmail = (TextInputEditText) root.findViewById(R.id.input_people_email);

        searchButtonPeople = (Button) root.findViewById(R.id.btn_search_people);
        clearButtonPeople = (Button) root.findViewById(R.id.btn_clear_people);

        /*searchButtonPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("name", peopleName.getText().toString());
                bundle.putString("last_name", peopleLastName.getText().toString());
                bundle.putString("signum", peopleSignum.getText().toString());
                bundle.putString("email", peopleEmail.getText().toString());

                Fragment fragment = new PeopleListFragment();
                fragment.setArguments(bundle);
                replaceFragment(fragment);
            }
        });*/

        /*clearButtonPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                peopleName.setText("");
                peopleLastName.setText("");
                peopleSignum.setText("");
                peopleEmail.setText("");
            }
        });*/

        clearButtonPeople.setVisibility(View.GONE);
        searchButtonPeople.setVisibility(View.GONE);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionSet set = new TransitionSet()
                            //.addTransition(new Scale())
                            .addTransition(new Fade())
                            .setDuration(200)
                            .setInterpolator(new OvershootInterpolator(1.0f));
                    TransitionManager.beginDelayedTransition(container, set);
                    TransitionManager.beginDelayedTransition(container);
                    clearButtonPeople.setVisibility(View.VISIBLE);
                    searchButtonPeople.setVisibility(View.VISIBLE);
                }
            }
        }, 1000);

        PushDownAnim.setPushDownAnimTo( clearButtonPeople, searchButtonPeople )
                .setScale( PushDownAnim.MODE_STATIC_DP , 2 )
                .setDurationPush( PushDownAnim.DEFAULT_PUSH_DURATION )
                .setDurationRelease( PushDownAnim.DEFAULT_RELEASE_DURATION )
                .setInterpolatorPush( PushDownAnim.DEFAULT_INTERPOLATOR )
                .setInterpolatorRelease( PushDownAnim.DEFAULT_INTERPOLATOR )
                .setOnClickListener( getClickListener() )
                .setOnLongClickListener( getLongClickListener() );

        return root;
    }

    @NonNull
    private View.OnLongClickListener getLongClickListener(){
        return new View.OnLongClickListener(){
            @Override
            public boolean onLongClick( View view ){
                if( view == clearButtonPeople ){
                    Toast.makeText( getContext(), getString(R.string.msg_job_clean_text), Toast.LENGTH_SHORT ).show();
                    cleanControls();
                }else if( view == searchButtonPeople ){
                    if(nullInformation()) {
                        if(validateNetwork()) {
                            Toast.makeText(getContext(), getString(R.string.msg_search_information), Toast.LENGTH_SHORT).show();
                            sendBundleFragment();
                        }else{
                            Toast.makeText(getContext(), getString(R.string.msg_network), Toast.LENGTH_SHORT).show();
                            sendMessageInformationNull(getString(R.string.msg_information_title),getString(R.string.msg_network));
                        }
                    }else{
                        sendMessageInformationNull(getString(R.string.msg_information_title),getString(R.string.msg_people_validate_input));
                    }
                }
                return true; // true: not effect to single click
            }
        };
    }

    @NonNull
    private View.OnClickListener getClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == clearButtonPeople) {
                    Toast.makeText(getContext(), getString(R.string.msg_job_clean_text), Toast.LENGTH_SHORT).show();
                    cleanControls();
                }else if( view == searchButtonPeople ){
                    if(nullInformation()) {
                        if(validateNetwork()) {
                            Toast.makeText(getContext(), getString(R.string.msg_search_information), Toast.LENGTH_SHORT).show();
                            sendBundleFragment();
                        }else{
                            Toast.makeText(getContext(), getString(R.string.msg_network), Toast.LENGTH_SHORT).show();
                            sendMessageInformationNull(getString(R.string.msg_information_title),getString(R.string.msg_network));
                        }
                    }else{
                        sendMessageInformationNull(getString(R.string.msg_information_title),getString(R.string.msg_people_validate_input));
                    }
                }
            }

        };
    }

    private void sendMessageInformationNull(String title, String message) {
        AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
        builderOK.setTitle(title);
        builderOK.setMessage(message);
        // add the buttons
        builderOK.setPositiveButton(getString(R.string.btn_ok), null);
        AlertDialog dialogOK = builderOK.create();
        dialogOK.show();
    }

    private void sendBundleFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("name", peopleName.getText().toString());
        bundle.putString("last_name", peopleLastName.getText().toString());
        bundle.putString("signum", peopleSignum.getText().toString());
        bundle.putString("email", peopleEmail.getText().toString());

        Fragment fragment = new PeopleListFragment();
        fragment.setArguments(bundle);
        replaceFragment(fragment);
    }

    private boolean nullInformation(){
        if (peopleName.getText().toString().equals("") && peopleLastName.getText().toString().equals("")
                && peopleSignum.getText().toString().equals("") && peopleEmail.getText().toString().equals("")){
            return false;
        }else{
            return true;
        }
    }

    private void cleanControls(){
        peopleName.getText().clear();
        peopleLastName.getText().clear();
        peopleSignum.getText().clear();
        peopleEmail.getText().clear();
    }

    private void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void showProgressDialogWithTitle(String title,String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    private boolean validateNetwork(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            return true;
        }else {
            return false;
        }
    }

}
