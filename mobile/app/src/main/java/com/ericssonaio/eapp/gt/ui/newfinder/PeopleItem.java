package com.ericssonaio.eapp.gt.ui.newfinder;

public class PeopleItem {

    private String mPeopleName;
    private String mPeopleLastName;
    private String mPeopleSignum;
    private String mPeopleJob;
    private String mPeopleEmail;

    public int getImgPeople() {
        return imgPeople;
    }

    public void setImgPeople(int imgPeople) {
        this.imgPeople = imgPeople;
    }

    private int imgPeople;

    public PeopleItem(String peopleName, String peopleLastName, String peopleSignum, String peopleJob, String peopleEmail, String peopleCountry, int peopleImg) {
        this.mPeopleName = peopleName;
        this.mPeopleLastName = peopleLastName;
        this.mPeopleSignum = peopleSignum;
        this.mPeopleJob = peopleJob;
        this.mPeopleEmail = peopleEmail;
        this.mPeopleCountry = peopleCountry;
        this.imgPeople = peopleImg;
    }

    private String mPeopleCountry;

    public String getPeopleName() {
        return mPeopleName;
    }

    public void setPeopleName(String mPeopleName) {
        this.mPeopleName = mPeopleName;
    }

    public String getPeopleLastName() {
        return mPeopleLastName;
    }

    public void setPeopleLastName(String mPeopleLastName) {
        this.mPeopleLastName = mPeopleLastName;
    }

    public String getPeopleSignum() {
        return mPeopleSignum;
    }

    public void setPeopleSignum(String mPeopleSignum) {
        this.mPeopleSignum = mPeopleSignum;
    }

    public String getPeopleJob() {
        return mPeopleJob;
    }

    public void setPeopleJob(String mPeopleJob) {
        this.mPeopleJob = mPeopleJob;
    }

    public String getPeopleEmail() {
        return mPeopleEmail;
    }

    public void setPeopleEmail(String mPeopleEmail) {
        this.mPeopleEmail = mPeopleEmail;
    }

    public String getPeopleCountry() {
        return mPeopleCountry;
    }

    public void setPeopleCountry(String mPeopleCountry) {
        this.mPeopleCountry = mPeopleCountry;
    }
}
