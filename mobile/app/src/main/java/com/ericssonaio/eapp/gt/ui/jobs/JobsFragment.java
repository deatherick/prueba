package com.ericssonaio.eapp.gt.ui.jobs;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.transition.Fade;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;
import com.thekhaeng.pushdownanim.PushDownAnim;

import java.util.ArrayList;
import java.util.List;

public class JobsFragment extends Fragment {

    private JobsViewModel jobsViewModel;
    ProgressDialog progressDialog;

    Button searchButtonJobs, clearButtonJobs;

    AutoCompleteTextView inputTitle;
    AutoCompleteTextView inputCountry;

    private List<CountryItem> countryItemList;
    private List<TitleItem> titleItemList;

    public static JobsFragment newInstance() {
        return new JobsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        jobsViewModel =
                ViewModelProviders.of(this).get(JobsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_jobs, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("");
        progressDialog = new ProgressDialog(getContext());

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        fillCountryList();
        fillTitleList();

        inputTitle = (AutoCompleteTextView) root.findViewById(R.id.text_job_title);
        inputCountry = (AutoCompleteTextView) root.findViewById(R.id.text_job_location);

        AutoCompleteTitleAdapter adapterTitle = new AutoCompleteTitleAdapter(getContext(), titleItemList);
        inputTitle.setAdapter(adapterTitle);

        AutoCompleteCountryAdapter adapterCountry = new AutoCompleteCountryAdapter(getContext(), countryItemList);
        inputCountry.setAdapter(adapterCountry);

        searchButtonJobs = (Button) root.findViewById(R.id.btn_search_jobs);
        clearButtonJobs = (Button) root.findViewById(R.id.btn_clear_jobs);

        /*searchButtonJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("country", inputCountry.getText().toString());
                bundle.putString("title", inputTitle.getText().toString());

                Fragment fragment = new JobListFragment();
                fragment.setArguments(bundle);
                replaceFragment(fragment);
            }
        });*/

        /*clearButtonJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputCountry.setText("");
                inputTitle.setText("");
            }
        });*/

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionSet set = new TransitionSet()
                            //.addTransition(new Scale())
                            .addTransition(new Fade())
                            .setDuration(200)
                            .setInterpolator(new OvershootInterpolator(1.0f));
                    TransitionManager.beginDelayedTransition(container, set);
                    TransitionManager.beginDelayedTransition(container);
                    searchButtonJobs.setVisibility(View.VISIBLE);
                    clearButtonJobs.setVisibility(View.VISIBLE);
                }
            }
        }, 1000);

        PushDownAnim.setPushDownAnimTo(searchButtonJobs, clearButtonJobs)
                .setScale(PushDownAnim.MODE_STATIC_DP, 2)
                .setDurationPush(PushDownAnim.DEFAULT_PUSH_DURATION)
                .setDurationRelease(PushDownAnim.DEFAULT_RELEASE_DURATION)
                .setInterpolatorPush(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setInterpolatorRelease(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setOnClickListener(getClickListener())
                .setOnLongClickListener(getLongClickListener());

        return root;
    }

    private void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void showProgressDialogWithTitle(String title, String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    private void fillCountryList() {
        String[] itemCountriesName = getResources().getStringArray(R.array.countries_array);
        countryItemList = new ArrayList<>();
        for (int i = 0; i < itemCountriesName.length; i++) {
            countryItemList.add(new CountryItem(itemCountriesName[i]));
        }
    }

    private void fillTitleList() {
        String[] itemTitleName = getResources().getStringArray(R.array.titles_array);
        titleItemList = new ArrayList<>();
        for (int i = 0; i < itemTitleName.length; i++) {
            titleItemList.add(new TitleItem(itemTitleName[i]));
        }
    }

    @NonNull
    private View.OnLongClickListener getLongClickListener() {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (view == clearButtonJobs) {
                    Toast.makeText(getContext(), getString(R.string.msg_job_clean_text), Toast.LENGTH_SHORT).show();
                    cleanControls();
                } else if (view == searchButtonJobs) {

                    if (validateNetwork()) {
                        Toast.makeText(getContext(), getString(R.string.msg_search_information), Toast.LENGTH_SHORT).show();
                        sendBundleFragment();
                    } else {
                        Toast.makeText(getContext(), getString(R.string.msg_network), Toast.LENGTH_SHORT).show();
                        sendMessageInformationNull(getString(R.string.msg_information_title), getString(R.string.msg_network));
                    }

                }
                return true; // true: not effect to single click
            }
        };
    }

    private void sendBundleFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("country", inputCountry.getText().toString());
        bundle.putString("title", inputTitle.getText().toString());

        Fragment fragment = new JobListFragment();
        fragment.setArguments(bundle);
        replaceFragment(fragment);
    }

    @NonNull
    private View.OnClickListener getClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == clearButtonJobs) {
                    Toast.makeText(getContext(), getString(R.string.msg_job_clean_text), Toast.LENGTH_SHORT).show();
                    cleanControls();
                } else if (view == searchButtonJobs) {
                    if (validateNetwork()) {
                        Toast.makeText(getContext(), getString(R.string.msg_search_information), Toast.LENGTH_SHORT).show();
                        sendBundleFragment();
                    } else {
                        Toast.makeText(getContext(), getString(R.string.msg_network), Toast.LENGTH_SHORT).show();
                        sendMessageInformationNull(getString(R.string.msg_information_title), getString(R.string.msg_network));
                    }
                }
            }

        };
    }

    private void cleanControls() {
        inputCountry.getText().clear();
        inputTitle.getText().clear();
    }

    private boolean validateNetwork() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    private void sendMessageInformationNull(String title, String message) {
        AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
        builderOK.setTitle(title);
        builderOK.setMessage(message);
        // add the buttons
        builderOK.setPositiveButton(getString(R.string.btn_ok), null);
        AlertDialog dialogOK = builderOK.create();
        dialogOK.show();
    }
}
