package com.ericssonaio.eapp.gt;

import android.app.Application;
import android.content.res.Configuration;

import java.util.Locale;

public class Language extends Application {
    private Locale locale = null;

    @Override
    public void onCreate() {
        super.onCreate();

        Locale locale = new Locale("en_US");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getApplicationContext().getResources().updateConfiguration(config, null);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Locale locale = new Locale("es_ES");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getApplicationContext().getResources().updateConfiguration(config, null);

    }
}
