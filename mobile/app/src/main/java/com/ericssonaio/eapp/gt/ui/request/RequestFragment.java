package com.ericssonaio.eapp.gt.ui.request;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;

public class RequestFragment extends Fragment implements View.OnClickListener {

    private RequestViewModel requestViewModel;
    private CardView newRequestCard, searchRequestCard;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        requestViewModel =
                ViewModelProviders.of(this).get(RequestViewModel.class);
        View root = inflater.inflate(R.layout.fragment_request, container, false);

        /*final TextView textView = root.findViewById(R.id.text_request);
        requestViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("");

        // defining Cards
        newRequestCard = (CardView) root.findViewById(R.id.request_new);
        searchRequestCard = (CardView) root.findViewById(R.id.request_search);

        // Add Click listener to the cards
        newRequestCard.setOnClickListener(this);
        searchRequestCard.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        switch (v.getId()) {
            case R.id.request_new:
                fragment = new RequestNewFragment();
                replaceFragment(fragment);
                break;
            case R.id.request_search:
                fragment = new RequestSearchFragment();
                replaceFragment(fragment);
                break;
            default:
                break;
        }
    }

    private void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}