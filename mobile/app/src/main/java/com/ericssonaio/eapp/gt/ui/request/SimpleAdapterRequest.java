package com.ericssonaio.eapp.gt.ui.request;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

import com.ericssonaio.eapp.gt.R;
import com.ericssonaio.eapp.gt.notification.model.NotificationClass;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class SimpleAdapterRequest extends ArrayAdapter<Request> {

    private List<Request> itemList;
    private Context context;
    private int lastPosition = -1;
    private ArrayList<Request> arrayList;
    ProgressDialog progressDialog;
    int resultSendRequest = 0;
    String requestId = null;
    String estatus = null;
    NotificationClass notificationClass;

    private static class ViewHolder {
        TextView id;
        TextView status;
        TextView tittle;
        TextView created_timestamp;
        TextView total_days;
        ImageView imgUrl;
    }

    public SimpleAdapterRequest(List<Request> itemList, Context ctx) {
        super(ctx, android.R.layout.simple_list_item_1, itemList);
        this.itemList = itemList;
        this.context = ctx;
        this.arrayList = new ArrayList<Request>();
        this.arrayList.addAll(itemList);
    }

    public int getCount() {
        if (itemList != null)
            return itemList.size();
        return 0;
    }

    public Request getItem(int position) {
        if (itemList != null)
            return itemList.get(position);
        return null;
    }

    public long getItemId(int position) {
        if (itemList != null)
            return itemList.get(position).hashCode();
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        setupImageLoader();
        View v = convertView;
        ViewHolder holder;

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here
        }

        String id = getItem(position).getId();
        String status = getItem(position).getStatus();
        String tittle = getItem(position).getTitle();
        String created_timestamp = getItem(position).getCreated_timestamp();
        String total_days = getItem(position).getTotal_days();
        String img = getItem(position).getImg();

        Request request = new Request(id, status, tittle, created_timestamp, total_days, img);

        final View result;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_item, null);

            holder = new ViewHolder();
            holder.id = (TextView) v.findViewById(R.id.id);
            holder.status = (TextView) v.findViewById(R.id.status);
            holder.tittle = (TextView) v.findViewById(R.id.title);
            holder.created_timestamp = (TextView) v.findViewById(R.id.created_timestamp);
            holder.total_days = (TextView) v.findViewById(R.id.total_days);
            holder.imgUrl = (ImageView) v.findViewById(R.id.imageR);

            result = v;

            v.setTag(holder);
        }
        else {
            holder = (ViewHolder) v.getTag();
            result = v;
        }

        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.load_down_anim : R.anim.load_up_anim);
        result.startAnimation(animation);
        lastPosition = position;

        int defaultImage;

        Log.println(Log.INFO, "Estado: ", status);

        switch (status) {
            case "Created": 
                defaultImage = getContext().getResources().getIdentifier("@drawable/ic_created", null, getContext().getPackageName());
                break;
            case "Rejected":
                defaultImage = getContext().getResources().getIdentifier("@drawable/ic_rejected", null, getContext().getPackageName());
                break;
            case "Approved":
                defaultImage = getContext().getResources().getIdentifier("@drawable/ic_aproved", null, getContext().getPackageName());
                break;
            default:
                defaultImage = getContext().getResources().getIdentifier("@drawable/ic_question", null, getContext().getPackageName());
                break;
        }

        BitmapFactory.Options resizeOptions = new BitmapFactory.Options();
        resizeOptions.inSampleSize = 7; // decrease size 3 times
        resizeOptions.inScaled = true;

        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(defaultImage)
                .showImageOnFail(defaultImage)
                .decodingOptions(resizeOptions)
                .postProcessor(new BitmapProcessor() {
                    @Override
                    public Bitmap process(Bitmap bmp) {
                        return Bitmap.createScaledBitmap(bmp, 5, 5, false);
                    }
                })
                .showImageOnLoading(defaultImage).build();

        imageLoader.displayImage(img, holder.imgUrl, options);

        holder.id.setText(request.getId());
        holder.status.setText(request.getStatus());
        holder.tittle.setText(request.getTitle());
        holder.created_timestamp.setText(request.getCreated_timestamp());
        holder.total_days.setText(request.getTotal_days());

        v.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                //code later
                StringBuilder sb = new StringBuilder();
                String signum = null;
                SharedPreferences prefs;
                prefs = getContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
                byte[] data = Base64.decode(prefs.getString("signume", ""), Base64.DEFAULT);
                signum = new String(data, StandardCharsets.UTF_8);

                sb.append("Usuario: " + signum);
                sb.append("\n");
                sb.append(getItem(position).getStatus());
                sb.append("\n");
                sb.append(getItem(position).getId());
                sb.append("\n");
                sb.append(getItem(position).getTitle());
                sb.append("\n");
                sb.append(getItem(position).getCreated_timestamp());
                sb.append("\n");
                sb.append(getItem(position).getTotal_days());

                // add the buttons
                if (getItem(position).getStatus().equals(String.valueOf(R.string.status_request_created))) {
                    AlertDialog.Builder builderOK = new AlertDialog.Builder(getContext());
                    builderOK.setTitle(String.valueOf(R.string.msg_request_information_title));
                    builderOK.setMessage(sb.toString());
                    builderOK.setPositiveButton(String.valueOf(R.string.btn_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestId = getItem(position).getId().toString();
                            estatus = String.valueOf(R.string.status_request_approved);
                            AsyncListViewLoader sendRequestApproved = new AsyncListViewLoader();
                            sendRequestApproved.execute();
                        }
                    });
                    builderOK.setNegativeButton(String.valueOf(R.string.btn_rejected), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestId = getItem(position).getId().toString();
                            estatus = String.valueOf(R.string.status_request_rejected);
                            AsyncListViewLoader sendRequestApproved = new AsyncListViewLoader();
                            sendRequestApproved.execute();
                        }
                    });
                    builderOK.setNeutralButton(String.valueOf(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getContext(), String.valueOf(R.string.msg_cancel_request_detail), Toast.LENGTH_SHORT).show();
                        }
                    });
                    AlertDialog dialogOK = builderOK.create();
                    dialogOK.show();
                }else{
                    AlertDialog.Builder builderClose = new AlertDialog.Builder(getContext());
                    builderClose.setTitle(String.valueOf(R.string.msg_request_information_title));
                    builderClose.setMessage(sb.toString());
                    builderClose.setNeutralButton(String.valueOf(R.string.btn_close), null);
                    AlertDialog dialogClose = builderClose.create();
                    dialogClose.show();
                }
            }
        });

        return v;

    }

    public List<Request> getItemList() {
        return itemList;
    }

    public void setItemList(List<Request> itemList) {
        this.itemList = itemList;
    }

    private void setupImageLoader(){
        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                //.discCacheSize(100 * 1024 * 1024).build();
                .discCacheSize(5 * 51 * 51).build();

        ImageLoader.getInstance().init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP
    }

    public void filter(String charText){
        charText = charText.toLowerCase(Locale.getDefault());
        itemList.clear();
        if (charText.length()==0){
            itemList.addAll(arrayList);
        }else {
            for (Request request: arrayList){
                if(request.getTitle().toLowerCase(Locale.getDefault())
                    .contains(charText)){
                    itemList.add(request);
                }
            }
        }
        notifyDataSetChanged();
    }


    private class AsyncListViewLoader extends AsyncTask<String, Void, List<Request>> {

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(List<Request> result) {
            super.onPostExecute(result);
            hideProgressDialogWithTitle();
            notificationClass = new NotificationClass();
            SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH:mm:ss");
            Date today = new Date();

            if (resultSendRequest == 0) {
                Toast.makeText(getContext(), String.valueOf(R.string.msg_success_send_request), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderSend = new AlertDialog.Builder(getContext());
                builderSend.setTitle(String.valueOf(R.string.msg_information_title));
                builderSend.setMessage(String.valueOf(R.string.msg_status_request_detail) + estatus);
                builderSend.setNeutralButton(String.valueOf(R.string.btn_close), null);
                AlertDialog dialogSend = builderSend.create();
                dialogSend.show();

                try {
                    notificationClass.sendNotification("", String.valueOf(R.string.msg_request_number) + requestId + String.valueOf(R.string.msg_request_estatus) + estatus, getContext());
                    notificationClass.sendEmail(String.valueOf(R.string.email_user), String.valueOf(R.string.email_pass), String.valueOf(R.string.pdf_email_notification_title), String.valueOf(R.string.msg_request_number) + requestId + String.valueOf(R.string.msg_request_estatus) + estatus, String.valueOf(R.string.email_to));
                    notificationClass.createPDF(requestId,estatus,sdf.format(today), getContext());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getContext(), String.valueOf(R.string.msg_error_sending_request_detail), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builderError = new AlertDialog.Builder(getContext());
                builderError.setTitle(String.valueOf(R.string.msg_error_sending_request_title));
                builderError.setMessage(String.valueOf(R.string.msg_error_sending_request_detail));
                // add the buttons
                builderError.setPositiveButton(String.valueOf(R.string.btn_ok), null);
                AlertDialog dialogError = builderError.create();
                dialogError.show();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialogWithTitle(String.valueOf(R.string.msg_information_title),String.valueOf(R.string.msg_sending_request));
        }


        @Override
        protected List<Request> doInBackground(String... params) {
            List<Request> result = new ArrayList<Request>();
            String url = "http://ongoing.com.gt/erick/Api/approve_reject_request";
            String json = null;
            requestId = requestId.substring(3).trim();
            resultSendRequest = 0;

            try {
                    URL u = new URL(url);

                    org.json.JSONObject jsonObject = new org.json.JSONObject();
                    jsonObject.accumulate("username", "edejuli");
                    jsonObject.accumulate("request_id", requestId);
                    jsonObject.accumulate("status", estatus);
                    jsonObject.accumulate("observations", "Test approved request");
                    json = jsonObject.toString();

                    Log.println(Log.INFO, "json: ", json);

                    HttpURLConnection conn = (HttpURLConnection) u.openConnection();
                    conn.setConnectTimeout(5000);
                    conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);
                    conn.setUseCaches(false);
                    conn.setAllowUserInteraction(false);
                    conn.setConnectTimeout(3000);
                    conn.setReadTimeout(3000);
                    conn.setRequestMethod("GET");

                    OutputStream os = conn.getOutputStream();
                    os.write(json.getBytes("UTF-8"));
                    os.close();

                    conn.connect();

                    InputStream in = new BufferedInputStream(conn.getInputStream());

                    JSONParser jsonParser = new JSONParser();
                    JSONObject jsonObjects = (JSONObject) jsonParser.parse(
                            new InputStreamReader(in, "UTF-8"));

                    String results = jsonObjects.toJSONString();

                    Log.println(Log.INFO, "results: ", results);
                    resultSendRequest = 0;
                return result;
            }
            catch(Throwable t) {
                t.printStackTrace();
                resultSendRequest = 1;
            }
            return null;
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
            hideProgressDialogWithTitle();

            Toast.makeText(getContext(), String.valueOf(R.string.msg_error_sending_request_detail), Toast.LENGTH_SHORT).show();
            AlertDialog.Builder builderError = new AlertDialog.Builder(getContext());
            builderError.setTitle(String.valueOf(R.string.msg_error_sending_request_title));
            builderError.setMessage(String.valueOf(R.string.msg_error_sending_request_detail));
            // add the buttons
            builderError.setPositiveButton(String.valueOf(R.string.btn_ok), null);
            AlertDialog dialogError = builderError.create();
            dialogError.show();
        }
    }

    private String inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();

        InputStreamReader isr = new InputStreamReader(is);

        BufferedReader rd = new BufferedReader(isr);

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer.toString();
    }

    private void showProgressDialogWithTitle(String title,String substring) {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

}