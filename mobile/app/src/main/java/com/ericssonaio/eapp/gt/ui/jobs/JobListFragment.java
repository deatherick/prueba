package com.ericssonaio.eapp.gt.ui.jobs;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;

public class JobListFragment extends Fragment implements JobsAdapter.OnItemClickListener {
    public static String EXTRA_URL_APPLY = "jobUrlApply";
    private JobListViewModel jobListViewModel;

    private String URL_JOBS = "https://jobs.ericsson.com/api/jobs?location=COUNTRY&internal=false&format=json&keywords=JOBTITLE";

    ProgressDialog progressDialog;

    private RecyclerView recyclerView;
    private JobsAdapter jobsAdapter;
    private ArrayList<JobsItem> jobsItemArrayList;
    private RequestQueue requestQueue;
    private String locationBundle, titleBundle;

    CountryItemFlagResource countryItemFlagResource;

    public static JobListFragment newInstance() {
        return new JobListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        jobListViewModel =
                ViewModelProviders.of(this).get(JobListViewModel.class);
        View root = inflater.inflate(R.layout.fragment_job_list, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.app_jobs));
        progressDialog = new ProgressDialog(getContext());
        countryItemFlagResource = new CountryItemFlagResource();

        int resId = R.anim.layout_animation_right_to_left;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), resId);

        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_animation_right_to_left);


        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutAnimation(animation);

        recyclerView.setLayoutAnimation(controller);
        //recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        jobsItemArrayList = new ArrayList<>();

        requestQueue = Volley.newRequestQueue(getContext());

        Bundle bundle = this.getArguments();
        if(bundle != null){
            //parseJSON(bundle.getString("country"), bundle.getString("title"));
            locationBundle = bundle.getString("country");
            titleBundle = bundle.getString("title");
            JobListFragmentAsyncTask jobListFragmentAsyncTask = new JobListFragmentAsyncTask();
            jobListFragmentAsyncTask.execute();
        }

        return root;
    }

    /*private void parseJSON(String location, String title) {
        String json = null;
        String url = URL_JOBS;

        url = url.replace("COUNTRY", location);
        url = url.replace("JOBTITLE", title);
        System.out.println("x-url: "+url);

        try {
            json = Jsoup.connect(url).ignoreContentType(true).execute().body();

            System.out.println("x-json: "+json);
        } catch (IOException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("jobs");
                    System.out.println("x-jsonObject: "+jsonArray);

                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject job = jsonArray.getJSONObject(i);
                        System.out.println("x-job: "+job);

                        JSONObject jsonObjectJob = job.getJSONObject("data");
                        System.out.println("x-jsonObjectJob: "+jsonObjectJob);

                        String creatorName = jsonObjectJob.getString("title");
                        String creatorCountry = jsonObjectJob.getString("country");
                        String creatorId = jsonObjectJob.getString("req_id");
                        String creatorPostedDate = jsonObjectJob.getString("posted_date");
                        String creatorUrlApply = jsonObjectJob.getString("apply_url");


                        jobsItemArrayList.add(new JobsItem(creatorName, creatorCountry, creatorId, creatorPostedDate, creatorUrlApply));
                    }

                    jobsAdapter = new JobsAdapter(getContext(), jobsItemArrayList);
                    recyclerView.setAdapter(jobsAdapter);
                    jobsAdapter.setOnItemClickListener(JobListFragment.this);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(request);
    }*/

    @Override
    public void onItemClick(int position) {
        JobsItem clickedItem = jobsItemArrayList.get(position);
        EXTRA_URL_APPLY = clickedItem.getJobUrlApply();

        Fragment fragment = new JobsDetailFragment();
        replaceFragment(fragment);
    }

    private void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void showProgressDialogWithTitle(String title,String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    private class JobListFragmentAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String json = null;
            String url = URL_JOBS;

            url = url.replace("COUNTRY", locationBundle);
            url = url.replace("JOBTITLE", titleBundle);
            System.out.println("x-url: "+url);

            try {
                json = Jsoup.connect(url).ignoreContentType(true).execute().body();

                System.out.println("x-json: "+json);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url,
                    null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray jsonArray = response.getJSONArray("jobs");
                        System.out.println("x-jsonObject: "+jsonArray);

                        for(int i = 0; i < jsonArray.length(); i++){
                            JSONObject job = jsonArray.getJSONObject(i);
                            System.out.println("x-job: "+job);

                            JSONObject jsonObjectJob = job.getJSONObject("data");
                            System.out.println("x-jsonObjectJob: "+jsonObjectJob);

                            String creatorName = jsonObjectJob.getString("title");
                            String creatorCountry = jsonObjectJob.getString("country");
                            String creatorId = jsonObjectJob.getString("req_id");
                            String creatorPostedDate = jsonObjectJob.getString("posted_date");
                            String creatorUrlApply = jsonObjectJob.getString("apply_url");
                            int creatorImg = countryItemFlagResource.countryItemFlagResources(creatorCountry);

                            jobsItemArrayList.add(new JobsItem(creatorName, creatorCountry, creatorId, creatorPostedDate, creatorUrlApply, creatorImg));
                        }

                        jobsAdapter = new JobsAdapter(getContext(), jobsItemArrayList);
                        recyclerView.setAdapter(jobsAdapter);
                        jobsAdapter.setOnItemClickListener(JobListFragment.this);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            requestQueue.add(request);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialogWithTitle(getString(R.string.msg_information_title),getString(R.string.msg_loading_information));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgressDialogWithTitle();
        }
    }
}
