package com.ericssonaio.eapp.gt.ui.fiori;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.ericssonaio.eapp.gt.MainActivity;
import com.ericssonaio.eapp.gt.R;

import static android.widget.Toast.LENGTH_LONG;

public class FioriFragment extends Fragment {

    private FioriViewModel fioriViewModel;
    //static String url_police = "https://login2.ericsson.net/login/V2_0/login.fcc?TYPE=33619969&REALMOID=06-4c364e66-9cf9-432d-bb4e-c6a7afa762c6&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=$SM$YiR2AGlQm4jXTxhTQ7E+iQYDWDSXODSQRv+c8vwbOECDkj7OU+5jdSCPeuaGov3v&TARGET=$SM$HTTPS://fss.ericsson.net/siteminderagent/redirectjsp/globallogon-net.jsp?SPID=sapnwgateway&RelayState=fiori&SMPORTALURL=https$:$/$/fss.ericsson.net$/affwebservices$/public$/saml2sso&SAMLTRANSACTIONID=df0d2479-8c13bf03-15876b5b-998cddb8-fe45b839-fd5";
    static String url_police = "https://fss.ericsson.com/affwebservices/public/saml2sso?SPID=sapnwgateway&RelayState=fiori";
    final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
    String cookie = null;
    static String url = "https://fss.ericsson.com/affwebservices/public/saml2sso?SPID=sapnwgateway&RelayState=fiori";
    String url_external = null;
    ProgressDialog progressDialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        fioriViewModel =
                ViewModelProviders.of(this).get(FioriViewModel.class);
        View root = inflater.inflate(R.layout.fragment_fiori, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.on__title_fiori));
        progressDialog = new ProgressDialog(getContext());

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            WebView mWebView = (WebView) root.findViewById(R.id.webview_fiori);

            // Force links and redirects to open in the WebView instead of in a browser
            mWebView.setWebViewClient(new WebViewClient(){
                private volatile boolean timeout;
                private volatile String timeoutOnPageStartedURL;
                private volatile String timeoutCurrentURL;

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);

                    timeout = true;
                    timeoutOnPageStartedURL = url;
                    timeoutCurrentURL = view.getUrl();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (timeout) {
                                view.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        String currentURL = view.getUrl();
                                        if ((timeoutOnPageStartedURL.hashCode() == currentURL.hashCode()) ||
                                                (timeoutCurrentURL.hashCode() == currentURL.hashCode())) {
                                            // do what you want with UI
                                            showProgressDialogWithTitle(getString(R.string.msg_loading_title),getString(R.string.msg_loading_web));
                                        }
                                    }
                                });
                            }
                        }
                    }).start();
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    hideProgressDialogWithTitle();
                    timeout = false;
                }
            });

            // Enable Javascript
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            mWebView.getSettings().setDomStorageEnabled(true);

            /******
             * Loading Fiori pre autorizado
             */
            //Inicio
            /**String json = null; //"{\"key\":1}";;
            try {
                Document document = Jsoup.connect(url_police).get();

                Connection.Response loginPageResponse =
                        Jsoup.connect(url_police)
                                .referrer(url_police)
                                .userAgent("Mozilla/5.0")
                                .timeout(10 * 1000)
                                .followRedirects(true)
                                .execute();

                System.out.println("Salida Page Response URL :" + loginPageResponse.url());

                Map<String, String> mapLoginPageCookies = loginPageResponse.cookies();

                Connection.Response loginResponse = Jsoup.connect(url_police)
                        .userAgent(USER_AGENT)
                        .data("USER", "edejuli")
                        .data("PASSWORD", "Usuario@123")
                        .method(Connection.Method.GET)
                        .execute();

                Map<String, String> mapLoginPageCookies2 = loginResponse.cookies();

                //System.out.println("cookies: " + mapLoginPageCookies2.toString());
                //System.out.println("SMSESSION: " + loginResponse.cookie("SMSESSION"));

                cookie = loginResponse.cookie("SMSESSION");


                /*String url_final = Jsoup.connect(url2)
                        .ignoreContentType(true)
                        .cookies(mapLoginPageCookies2)
                        .followRedirects(true).execute().url().toExternalForm();*/

                /**System.out.println("Salida URL :" + loginResponse.url());

                url_police = loginResponse.url().toString();

                Document doc = Jsoup.connect(loginResponse.url().toString())
                        .ignoreContentType(true)
                        .cookies(mapLoginPageCookies2)
                        .get();

                System.out.println(doc.outerHtml());

            } catch (IOException e) {
                e.printStackTrace();
            }***/

            //Fin
            //mWebView.loadUrl(url);
            mWebView.loadUrl(url);
        } else {
            // No hay conexión a Internet en este momento
            Toast.makeText(getContext(),getString(R.string.msg_network), LENGTH_LONG).show();
        }
        return root;
    }

    private void showProgressDialogWithTitle(String title,String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setCancelable(false);

        progressDialog.setTitle(title);
        progressDialog.setMessage(substring);
        progressDialog.show();

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }
}