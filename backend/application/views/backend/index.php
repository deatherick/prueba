<?php $this->load->view('backend/templates/header.php'); ?>
<body>
<div class="wrapper ">
	<div class="sidebar" data-color="purple" data-background-color="white">
		<!--
		Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

		Tip 2: you can also add an image using data-image tag
	-->
		<div class="logo">

			<div class="row">
				<div class="col-md-12" style="text-align: center;">
					<?php echo image_asset("logo.png","", array("width" => "72", "height" => "72")); ?>
					<h1 class="text-center"><?php echo APP_NAME; ?></h1>
				</div>
			</div>
		</div>
		<?php $this->load->view('backend/templates/sidebar'); ?>
	</div>
	<div class="main-panel">
		<!-- Navbar -->
		<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
			<div class="container-fluid">
				<div class="navbar-wrapper">
					<a class="navbar-brand" href="#"><?php echo $title; ?></a>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
				</button>
				<div class="collapse navbar-collapse justify-content-end">
					<?php echo form_open("tools/search", array("class" => "navbar-form", "method"=>"GET"));?>
						<div class="input-group no-border">
							<input type="text" value="" class="form-control" name="filter" placeholder="Search...">
							<button type="submit" class="btn btn-white btn-round btn-just-icon">
								<i class="material-icons">search</i>
								<div class="ripple-container"></div>
							</button>
						</div>
					<?php echo form_close();?>
					<ul class="navbar-nav">
						<li class="nav-item dropdown">
							<a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="material-icons">notifications</i>
								<?php if (isset($notifications) && count($notifications) > 0): ?>
									<span class="notification"><?php echo count($notifications); ?></span>
								<?php endif; ?>
								<p class="d-lg-none d-md-block">
									Pending Requests
								</p>
							</a>
							<?php if (isset($notifications) && count($notifications) > 0): ?>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
								<!-- <a class="dropdown-item" href="#">Mike John responded to your email</a>
								<a class="dropdown-item" href="#">You have 5 new tasks</a>
								<a class="dropdown-item" href="#">You're now friend with Andrew</a>
								<a class="dropdown-item" href="#">Another Notification</a>
								<a class="dropdown-item" href="#">Another One</a> -->
							</div>
							<?php endif; ?>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link" href="#" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="material-icons">person</i>
								<p class="d-lg-none d-md-block">
									Account
								</p>
							</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
								<a class="dropdown-item" href="#">Profile</a>
								<a class="dropdown-item" href="#">Settings</a>
								<div class="dropdown-divider"></div>
								<?php echo anchor('auth/logout', 'Log out', array('class' => 'dropdown-item')); ?>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End Navbar -->
		<div class="content">
			<div class="container-fluid">
				<?php $this->load->view($request_page); ?>
			</div>
		</div>
		<!-- BEGIN FOOTER -->
		<footer class="footer">
			<div class="container">
				<span class="text-muted">Trademark and Copyright Notice™ &copy; <?php echo date('Y'); ?> ERICSSON and its related entities. All rights reserved.</span>
			</div>
		</footer>
	</div>
</div>

<?php $this->enqueue->load_files_footer(); ?>
</body>
</html>
