<div class="sidebar-wrapper">
	<ul class="nav">
		<li class="nav-item <?php if(strtolower($title) == strtolower(_('Dashboard'))) echo "active" ?> ">
			<?php echo anchor("/", '
					<i class="material-icons">dashboard</i>
					<p>Dashboard</p>'
				, array("class" => "nav-link")) ;?>
		</li>
		<li class="nav-item <?php if(strtolower($title) == strtolower(_('Profile'))) echo "active" ?> ">
			<?php echo anchor("manage/profile", '
					<i class="material-icons">person</i>
					<p>User Profile</p>'
				, array("class" => "nav-link")) ;?>
		</li>
		<li class="nav-item <?php if(strtolower($title)  == strtolower(_('Requests'))) echo "active" ?> ">
			<?php echo anchor("manage/requests", '
					<i class="material-icons">content_paste</i>
					<p>Request List</p>'
				, array("class" => "nav-link")) ;?>
		</li>
	</ul>
</div>
