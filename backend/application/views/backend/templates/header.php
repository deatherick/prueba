<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
	<meta charset="utf-8" />
	<title><?php echo APP_NAME; ?> | <?php echo $title; ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

	<?php echo css_asset('material-dashboard.css?v=2.1.1'); ?>
	<!-- END GLOBAL MANDATORY STYLES -->

	<?php $this->enqueue->load_files_header(); ?>


	<!--   Core JS Files   -->
	<?php echo js_asset("core/jquery.min.js"); ?>
	<?php echo js_asset("core/popper.min.js"); ?>
	<?php echo js_asset("core/bootstrap-material-design.min.js"); ?>
	<?php echo js_asset("plugins/perfect-scrollbar.jquery.min.js"); ?>
	<!-- Plugin for the momentJs  -->
	<?php echo js_asset("plugins/moment.min.js"); ?>
	<!--  Plugin for Sweet Alert -->
	<?php echo js_asset("plugins/sweetalert2.js"); ?>
	<!-- Forms Validations Plugin -->
	<?php echo js_asset("plugins/jquery.validate.min.js"); ?>
	<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
	<?php echo js_asset("plugins/jquery.bootstrap-wizard.js"); ?>
	<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
	<?php echo js_asset("plugins/bootstrap-selectpicker.js"); ?>
	<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
	<?php echo js_asset("plugins/bootstrap-datetimepicker.min.js"); ?>
	<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
	<?php echo js_asset("plugins/jquery.dataTables.min.js"); ?>
	<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
	<?php echo js_asset("plugins/bootstrap-tagsinput.js"); ?>
	<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
	<?php echo js_asset("plugins/jasny-bootstrap.min.js"); ?>
	<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
	<?php echo js_asset("plugins/fullcalendar.min.js"); ?>
	<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
	<?php echo js_asset("plugins/jquery-jvectormap.js"); ?>
	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<?php echo js_asset("plugins/nouislider.min.js"); ?>
	<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
	<!-- Library for adding dinamically elements -->
	<?php echo js_asset("plugins/arrive.min.js"); ?>
	<!-- Chartist JS -->
	<?php echo js_asset("plugins/chartist.min.js"); ?>
	<!--  Notifications Plugin    -->
	<?php echo js_asset("plugins/bootstrap-notify.js"); ?>
	<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
	<?php echo js_asset("material-dashboard.js?v=2.1.1"); ?>
	<!-- MomentJS for date -->
	<?php echo js_asset("moment-with-locales.js"); ?>
	<script>
		$(document).ready(function() {
			// Javascript method's body can be found in assets/js/demos.js
			md.initDashboardPageCharts();

		});
	</script>

	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<!-- END PAGE LEVEL PLUGINS -->
	<link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->
