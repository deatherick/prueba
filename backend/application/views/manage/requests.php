<div id="infoMessage"><?php echo $message;?></div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-warning">
				<h4 class="card-title"><?php echo $title; ?> table</h4>
				<p class="card-category">All requests on <?php echo APP_NAME; ?></p>
			</div>

			<div class="card-body table-responsive">
				<table class="table table-hover" style="width:100%">
					<thead class="text-warning">
					<tr>
						<th>Type</th>
						<th>Status</th>
						<th>Created By</th>
						<th>Title</th>
						<th>Created Date</th>
						<th >Actions</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($requests as $request):?>
						<tr>
							<td><?php echo htmlspecialchars($request->request_type,ENT_QUOTES,'UTF-8');?></td>
							<td>
								<?php
								switch ($request->status) {
									case "Created" :
										echo '<span class="badge badge-primary">';
										break;
									case "Approved" :
										echo '<span class="badge badge-success">';
										break;
									case "Rejected" :
										echo '<span class="badge badge-danger">';
										break;
								}
								echo htmlspecialchars($request->status,ENT_QUOTES,'UTF-8');
								echo "</span>";?>
							</td>
							<td><?php echo htmlspecialchars($request->created_by,ENT_QUOTES,'UTF-8');?></td>
							<td><?php echo htmlspecialchars($request->title,ENT_QUOTES,'UTF-8');?></td>
							<td><?php echo htmlspecialchars($request->created_timestamp,ENT_QUOTES,'UTF-8');?></td>
							<td>
								<?php if ($request->status == "Created") : ?>
									<?php if ($request->created_by == $this->session->userdata('username')) : ?>
										<?php echo anchor("manage/request/".$request->id, '<i class="material-icons warning" data-toggle="tooltip" title="Edit">create</i>') ;?>
									<?php endif; ?>
									<?php if ($this->ion_auth_model->in_group(array('admin', 'Line Manager', $this->session->userdata('user_id'))) ) : ?>
										<?php echo anchor("manage/request/".$request->id, '<i class="material-icons" data-toggle="tooltip" title="View">visibility</i>') ;?>
										<?php echo anchor("manage/requests/#", '<i class="material-icons" data-toggle="tooltip" title="Accept">done</i>', array('class' => 'btn-accept', 'data-id' => $request->id, 'data-title' => $request->title, 'data-user' => $request->created_by)) ;?>
										<?php echo anchor("manage/requests/#", '<i class="material-icons" data-toggle="tooltip" title="Reject">block</i>', array('class' => 'btn-reject', 'data-id' => $request->id, 'data-title' => $request->title, 'data-user' => $request->created_by)) ;?>
									<?php endif; ?>
								<?php else : ?>
									<?php echo anchor("manage/request/".$request->id, '<i class="material-icons" data-toggle="tooltip" title="View">visibility</i>') ;?>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach;?>
					</tbody>
				</table>

				<button type="submit" class="btn btn-warning pull-right" onclick="window.location.href='<?php echo site_url('manage/request/'); ?>'">New Request</button>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="approveRejectModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<?php echo form_open("/manage/", array('method'=> 'POST', 'id' => 'form-acceptReject'));?>
				<div class="modal-header">
					<h5 class="modal-title">Request</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

						<div class="row">
							<input type="hidden" id="text-id" name="request_id">
							<div class="col-md-4">
								<label for="text-user" class="col-form-label">User:</label>
								<input type="text" class="form-control" disabled id="text-user">
							</div>
							<div class="col-md-8">
								<label for="text-title" class="col-form-label">Title:</label>
								<input type="text" class="form-control" disabled id="text-title">
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<label for="text-observations" class="col-form-label">Observations:</label>
								<textarea class="form-control" name="observations" id="text-observations"></textarea>
							</div>
						</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			<?php echo form_close();?>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('table').DataTable( {
			responsive: true
		} );
	});

	$('.btn-reject').on('click', function (e) {
		$item = $(e.currentTarget);

		$('#form-acceptReject').attr('action', '<?php echo site_url('/manage/reject') ?>');
		$('.modal-title').html("Reject request");
		$('#approveRejectModal #text-id').val($item.data("id"));
		$('#approveRejectModal #text-user').val($item.data("user"));
		$('#approveRejectModal #text-title').val($item.data("title"));
		$('#approveRejectModal').modal('show');
		e.preventDefault();
	});

	$('.btn-accept').on('click', function (e) {
		$item = $(e.currentTarget);

		$('.modal-title').html("Approve request");
		$('#form-acceptReject').attr('action', '<?php echo site_url('/manage/accept') ?>');
		$('#approveRejectModal #text-id').val($item.data("id"));
		$('#approveRejectModal #text-user').val($item.data("user"));
		$('#approveRejectModal #text-title').val($item.data("title"));
		$('#approveRejectModal').modal('show');
		e.preventDefault();
	});

	$("#form-acceptReject").submit(function(e){
		e.preventDefault(); //prevent default action
		var post_url = $(this).attr("action"); //get form action url
		var request_method = $(this).attr("method"); //get form GET/POST method
		var form_data = $(this).serialize(); //Encode form elements for submission

		$.ajax({
			url : post_url,
			type: request_method,
			data : form_data
		}).done(function(response){
			data = JSON.parse(response);
			if(data.error){
				swal("Error!", data.message, "error")
			} else {
				swal("Success!", data.message, "success")
				.then((value) => {
					location.reload();
				});
			}
		});
	})

</script>
