<div class="row">
	<div class="col-md-12">

		<div class="card">
			<div class="card-header card-header-primary">
				<h4 class="card-title">Create/Edit Request</h4>
				<p class="card-category">Complete your request</p>
			</div>

			<div class="card-body">
				<?php echo form_open("/manage/request", array('method'=> 'POST'));?>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label class="bmd-label-floating">Title</label>
								<?php if ($disabled == TRUE) : ?>
									<?php echo form_input('title', isset($request->title) ? $request->title : "", array('class' => 'form-control', 'disabled' => '')) ?>
								<?php else: ?>
									<?php echo form_input('title', isset($request->title) ? $request->title : "", array('class' => 'form-control')) ?>
								<?php endif; ?>

							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="bmd-label-floating">Status (disabled)</label>
								<?php echo form_input('status', isset($request->status) ? $request->status : "Created", array('class' => 'form-control', 'disabled' => '')) ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="bmd-label-floating">Observations</label>
								<?php if ($disabled == TRUE) : ?>
									<?php echo form_input('observations', isset($request->observations) ? $request->observations : "", array('class' => 'form-control', 'disabled' => '')) ?>
								<?php else: ?>
									<?php echo form_input('observations', isset($request->observations) ? $request->observations : "", array('class' => 'form-control')) ?>
								<?php endif; ?>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="bmd-label-floating">Calendar</label>
								<input type="hidden" name="calendar" id="calendar"/>
							</div>
						</div>
						<div class="col-md-8">
							<div class="error"><?php echo $message;?></div>
						</div>
					</div>
					<?php if ($disabled != TRUE) : ?>
						<button type="submit" class="btn btn-primary pull-right"><?php echo isset($request->id) ? "Update" : "Create" ?> Request</button>
					<?php endif; ?>
					<div class="clearfix"></div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>
<script>

	function shakeError(){
		var $error = $('.error');
		if($error.html() !== ''){
			$error.addClass('shake');
			$error.addClass('alert alert-danger');
			setTimeout( function(){
				$error.removeClass('shake');
			}, 1000 );
		}
	}

	function dateSelector(algo){
		<?php
		if(isset($request->calendar)){
			$calendar = json_decode($request->calendar, true);
			if(isset($calendar['days'])){
				$days = '"'.implode('", "', json_decode($request->calendar, true)['days']).'"';
			}
		}
		?>
		let backendDates = [<?php echo isset($days) ? $days : ""; ?>];
		let selectedDates = [];
		backendDates.forEach(addSelectDate);

		function addSelectDate(value) {
			let date = moment(value, 'DD/MM/YYYY').toDate();
			selectedDates.push(date);
		}

		algo(selectedDates);
	}

	function createDateComponent(selectedDates) {
		let today = moment();
		let datepicker = $('#calendar').datepicker({
			language: 'en',
			<?php if (!isset($request->id)) : ?>
			minDate: moment(today).add(1, 'days').toDate(), // Now can select only dates, which goes after tomorrow
			multipleDates:  parseInt(<?php echo $pending_days; ?>),
			<?php else: ?>
			<?php $calendar = json_decode($request->calendar, true)['days']; ?>
			multipleDates:  parseInt(<?php echo floatval($request->pending_days) > 0.00 ? $request->pending_days : count($calendar); ?>),
			<?php endif; ?>
			multipleDatesSeparator: ', ',
			inline: 'true'
		});

		datepicker.data('datepicker').selectDate(selectedDates);
	}

	$(document).ready(function() {
		dateSelector(createDateComponent);
		shakeError();
	});

</script>
