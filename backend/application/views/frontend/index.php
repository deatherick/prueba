<?php $this->load->view('frontend/templates/header.php'); ?>
<body>
<div class="container">
	<?php $this->load->view($request_page); ?>
</div>
<!-- BEGIN FOOTER -->
<footer class="footer">
	<div class="container">
		<span class="text-muted">Trademark and Copyright Notice™ &copy; <?php echo date('Y'); ?> ERICSSON and its related entities. All rights reserved.</span>
	</div>
</footer>
<?php $this->enqueue->load_files_footer(); ?>
</body>
</html>
