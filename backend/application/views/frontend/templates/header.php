<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
	<meta charset="utf-8" />
	<title><?php echo APP_NAME; ?> | <?php echo $title; ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<?php echo css_asset('bootstrap.css'); ?>
	<?php echo css_asset('sticky-footer.css'); ?>
	<!-- END GLOBAL MANDATORY STYLES -->

	<?php $this->enqueue->load_files_header(); ?>

	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<?php echo js_asset('jquery-1.10.2.js'); ?>
	<?php echo js_asset('bootstrap.js'); ?>
	<?php echo js_asset('login-register.js'); ?>
	<!-- END PAGE LEVEL PLUGINS -->
	<link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->
