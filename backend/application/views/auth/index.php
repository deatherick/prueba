<div id="infoMessage"><?php echo $message;?></div>

<!-- SECCION DE FICHAS DE INFORMACION -->
<div class="row">
	<div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card card-stats">
			<div class="card-header card-header-warning card-header-icon">
				<div class="card-icon">
					<i class="fa fa-clipboard"></i>
				</div>
				<p class="card-category">Pending Requests</p>
				<h3 class="card-title">100
					<small>+</small>
				</h3>
			</div>
			<div class="card-footer">
				<div class="stats">
					<i class="material-icons text-danger">warning</i>
					<a href="#">Resolve</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card card-stats">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="fa fa-check"></i>
				</div>
				<p class="card-category">Approved Requests</p>
				<h3 class="card-title">245</h3>
			</div>
			<div class="card-footer">
				<div class="stats">
					<i class="material-icons">date_range</i> Last 4 Weeks
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card card-stats">
			<div class="card-header card-header-danger card-header-icon">
				<div class="card-icon">
					<i class="material-icons">info_outline</i>
				</div>
				<p class="card-category">Rejected Requests</p>
				<h3 class="card-title">15</h3>
			</div>
			<div class="card-footer">
				<div class="stats">
					<i class="material-icons">date_range</i> Last 4 Weeks
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card card-stats">
			<div class="card-header card-header-info card-header-icon">
				<div class="card-icon">
					<i class="fa fa-users"></i>
				</div>
				<p class="card-category">New Users</p>
				<h3 class="card-title">+245</h3>
			</div>
			<div class="card-footer">
				<div class="stats">
					<i class="material-icons">update</i> Just Updated
				</div>
			</div>
		</div>
	</div>
</div>

<!-- SECCION DE ESTADISTICAS -->
<div class="row">
	<div class="col-md-4">
		<div class="card card-chart">
			<div class="card-header card-header-success">
				<div class="ct-chart" id="dailySalesChart"></div>
			</div>
			<div class="card-body">
				<h4 class="card-title">Daily Requests</h4>
				<p class="card-category">
					<span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today requests.</p>
			</div>
			<div class="card-footer">
				<div class="stats">
					<i class="material-icons">access_time</i> updated 4 minutes ago
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-chart">
			<div class="card-header card-header-warning">
				<div class="ct-chart" id="websiteViewsChart"></div>
			</div>
			<div class="card-body">
				<h4 class="card-title">Vacation Requests</h4>
				<p class="card-category">Last Month analysis</p>
			</div>
			<div class="card-footer">
				<div class="stats">
					<i class="material-icons">access_time</i> Last month finished 3 days ago
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-chart">
			<div class="card-header card-header-danger">
				<div class="ct-chart" id="completedTasksChart"></div>
			</div>
			<div class="card-body">
				<h4 class="card-title">Accepted Requests</h4>
				<p class="card-category">Last Month analysis</p>
			</div>
			<div class="card-footer">
				<div class="stats">
					<i class="material-icons">access_time</i> Last month finished 3 days ago
				</div>
			</div>
		</div>
	</div>
</div>
