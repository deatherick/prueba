<div class="container">
<div class="login" id="loginModal">
	<div class="modal-dialog login animated">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Login with</h4>
			</div>
			<div class="modal-body">
				<div class="box">
					<div class="content">
						<div class="error"><?php echo $message;?></div>
						<div class="form loginBox">
							<?php echo form_open("auth/login");?>
							<div class="row">
								<div class="col-md-12">
									<?php echo image_asset("logo.png","", array("class" => "center-block", "width" => "72", "height" => "72")); ?>
								</div>
							</div>
							<div class="division">
								<div class="line l"></div>
							</div>
								<input id="email" class="form-control" type="text" placeholder="Email" name="identity" value="<?php echo $identity; ?>">
								<input id="password" class="form-control" type="password" placeholder="Password" name="password">
								<?php echo lang('login_remember_label', 'remember');?>
								<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
								<input class="btn btn-default btn-login" type="submit" value="Login" >
							<?php echo form_close();?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="forgot login-footer">
					<span>
						<a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
