<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property CI_DB_query_builder db
 * @property CI_Upload upload
 * @property Ion_auth ion_auth
 * @property Ion_auth_model ion_auth_model
 */
class Request_model extends CI_Model
{
	private $requests = 'requests';
	private $request_types = 'request_types';
	private $request_states = 'request_states';
	private $request_history = 'request_history';

	/**
	 * @param string $table
	 * @param array  $data
	 *
	 * @return array
	 */
	protected function _filter_data($table, $data)
	{
		$filtered_data = [];
		$columns = $this->db->list_fields($table);

		if (is_array($data))
		{
			foreach ($columns as $column)
			{
				if (array_key_exists($column, $data))
					$filtered_data[$column] = $data[$column];
			}
		}

		return $filtered_data;
	}

	public function get()
	{
		//Get all data if an admin is logged
		if ($this->ion_auth->is_admin()) {
			return $this->db
				->select('requests.id, requests.title, requests.status, requests.pending_days, requests.total_days, requests.calendar, requests.observations, requests.assigned_to, requests.created_by, requests.created_timestamp, requests.attachments, request_types.request_group request_type')
				->join($this->request_types, 'request_types.id = requests.request_type')
				->order_by('requests.created_timestamp', 'ASC')
				->get_where($this->requests);
		}

		if ($this->ion_auth_model->in_group('Line Manager')) {
			//Get only the data that is associated to a manager
			return $this->db
				->select('requests.id, requests.title, requests.status, requests.pending_days, requests.total_days, requests.calendar, requests.observations, requests.assigned_to, requests.created_by, requests.created_timestamp, requests.attachments, request_types.request_group request_type')
				->join($this->request_types, 'request_types.id = requests.request_type')
				->order_by('requests.created_timestamp', 'ASC')
				->where('requests.created_by', $this->session->userdata('username'))
				->or_where('requests.assigned_to', $this->session->userdata('username'))->get($this->requests);
		}

		//Get the data that is associated to a specific user
		return $this->db
			->select('requests.id, requests.title, requests.status, requests.pending_days, requests.total_days, requests.calendar, requests.observations, requests.assigned_to, requests.created_by, requests.created_timestamp, requests.attachments, request_types.request_group request_type')
			->join($this->request_types, 'request_types.id = requests.request_type')
			->order_by('requests.created_timestamp', 'ASC')
			->get_where($this->requests, array('requests.created_by' => $this->session->userdata('username')));

	}

	public function getRequestHistory($username, $request_id){
		$user = $this->ion_auth_model->get_user_data_by_username($username);
		if($user){
			if($this->ion_auth_model->in_group('admin', $user->id)) {
				return $this->db
					->order_by('created_timestamp', 'ASC')
					->get_where($this->request_history, array('request_id' => $request_id));
			}

			if($this->ion_auth_model->in_group('Line Manager', $user->id)) {
				//Get only the data that is associated to a manager
				return $this->db->select('request_history.*')
					->from($this->request_history)
					->join($this->requests, 'requests.id = request_history.request_id')
					->order_by('requests.created_timestamp', 'ASC')
					->where(array('requests.id' => $request_id))
					->where(array('requests.assigned_to' => $username))
					->or_where(array('requests.created_by' => $username))
					->get()->result();
			}

			//Get only the data that is associated to a username
			return $this->db->select('request_history.*')
				->from($this->request_history)
				->join($this->requests, 'requests.id = request_history.request_id')
				->order_by('requests.created_timestamp', 'ASC')
				->where(array('requests.id' => $request_id))
				->where(array('requests.created_by' => $username))
				->get()->result();
		} else {
			return array();
		}
	}

	public function getByUsername($username) {
		$user = $this->ion_auth_model->get_user_data_by_username($username);
		if($user){
			if($this->ion_auth_model->in_group('admin', $user->id)) {
				return $this->db->select('requests.id, requests.title, requests.status, requests.pending_days, requests.total_days, requests.calendar, requests.observations, requests.assigned_to, requests.created_by, requests.created_timestamp, request_history.approved_by, request_history.created_timestamp approved_rejected_timestamp, request_history.observations approved_rejected_observations ')
					->from($this->requests)
					->join($this->request_history, "requests.id = request_history.request_id AND request_history.status IN('Approved', 'Rejected')", 'LEFT OUTER')
					->order_by('requests.created_timestamp', 'ASC')
					->get()->result();
			}

			if($this->ion_auth_model->in_group('Line Manager', $user->id)) {
				//Get only the data that is associated to a manager
				return $this->db->select('requests.id, requests.title, requests.status, requests.pending_days, requests.total_days, requests.calendar, requests.observations, requests.assigned_to, requests.created_by, requests.created_timestamp, request_history.approved_by, request_history.created_timestamp approved_rejected_timestamp, request_history.observations approved_rejected_observations ')
					->from($this->requests)
					->join($this->request_history, "requests.id = request_history.request_id AND request_history.status IN('Approved', 'Rejected')", 'LEFT OUTER')
					->order_by('requests.created_timestamp', 'ASC')
					->where(array('requests.assigned_to' => $username))
					->or_where(array('requests.created_by' => $username))
					->get()->result();
			}

			//Get only the data that is associated to a username
			return $this->db->select('requests.id, requests.title, requests.status, requests.pending_days, requests.total_days, requests.calendar, requests.observations, requests.assigned_to, requests.created_by, requests.created_timestamp, request_history.approved_by, request_history.created_timestamp approved_rejected_timestamp, request_history.observations approved_rejected_observations ')
				->from($this->requests)
				->join($this->request_history, "requests.id = request_history.request_id AND request_history.status IN('Approved', 'Rejected')", 'LEFT OUTER')
				->order_by('requests.created_timestamp', 'ASC')
				->where(array('requests.created_by' => $username))
				->get()->result();
		} else {
			return array();
		}
	}

	public function getById($id) {
		if ($this->ion_auth->is_admin()) {
			return $this->db
				->get_where($this->requests, array('id' => $id));
		} else {
			return $this->db
				->get_where($this->requests, array('created_by' => $this->session->userdata('username'), 'id' => $id));
		}
	}

	public function getById_API($username, $id) {
		$user = $this->ion_auth_model->get_user_data_by_username($username);
		if($user) {
			if ($this->ion_auth_model->in_group(array('admin', 'Line Manager'), $user->id)) {
				return $this->db
					->get_where($this->requests, array('id' => $id));
			} else {
				return $this->db
					->get_where($this->requests, array('created_by' => $username, 'id' => $id));
			}
		} else {
			return null;
		}
	}

	private function insert_history($request_id, $status, $approved_by, $observations, $additional_data = []){
		$user = $this->ion_auth_model->get_user_data_by_username($approved_by);
		if($user) {
			$data = [
				'request_id' => $request_id,
				'status' => $status,
				'approved_by' => $approved_by,
				'observations' => $observations
			];

			// filter out any data passed that doesnt have a matching column in the users table
			// and merge the set user data and the additional data
			$request_data = array_merge($this->_filter_data($this->request_history, $additional_data), $data);

			$this->db->insert($this->request_history, $request_data);

			$id = $this->db->insert_id($this->request_history . '_id_seq');

			return (isset($id)) ? $id : FALSE;
		}
		return FALSE;
	}

	public function insert($title, $request_type, $total_days, $calendar, $observations, $created_by, $additional_data = [])
	{
		$user = $this->ion_auth_model->get_user_data_by_username($created_by);
		if($user){
			$data = [
				'title'=> $title,
				'request_type' => $request_type,
				'status' => 'Created',
				'pending_days' => $user->pending_days,
				'total_days' => $total_days,
				'calendar' => $calendar,
				'observations' => $observations,
				'assigned_to' => $user->parent_user,
				'created_by' => $created_by
			];

			// filter out any data passed that doesnt have a matching column in the users table
			// and merge the set user data and the additional data
			$request_data = array_merge($this->_filter_data($this->requests, $additional_data), $data);

			$this->db->insert($this->requests, $request_data);

			$id = $this->db->insert_id($this->requests . '_id_seq');

			if(isset($id)) {
				$this->insert_history($id, 'Created', $created_by, $observations);
			}

			return (isset($id)) ? $id : FALSE;
		}
		return FALSE;
	}

	public function approve_or_reject($request_id, $status, $observations, $approved_by, $additional_data = [])
	{
		$user = $this->ion_auth_model->get_user_data_by_username($approved_by);
		if($user){
			$data = [
				'status' => $status,
				'pending_days' => ($status == 'Approved') ? 'pending_days - total_days' : 'pending_days',
				'observations' => $observations
			];

			// filter out any data passed that doesnt have a matching column in the users table
			// and merge the set user data and the additional data
			$request_data = array_merge($this->_filter_data($this->requests, $additional_data), $data);

			$update = $this->db->update($this->requests, $request_data, array('id' => $request_id));

			if($update) {
				$this->insert_history($request_id, $status, $approved_by, $observations);
			}

			return ($update) ? $update : FALSE;
		}
		return FALSE;
	}

	public function validate_approved_rejected($request_id){
		$qres = $this->db
			->where_in('status', array('Approved', 'Rejected'))
			->where('id', $request_id)
			->get($this->requests);

		return ($qres->num_rows() == 0);
	}
}
