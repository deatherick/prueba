<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property CI_Form_validation form_validation
 * @property array data
 * @property Ion_auth ion_auth
 * @property Ion_auth_model ion_auth_model
 * @property CI_Session|ArrayObject session
 * @property Enqueue enqueue
 * @property CI_Config config
 * @property CI_DB_query_builder db
 * @property CI_Lang lang
 * @property CI_Input input
 * @property CI_Security security
 */
class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url', 'language']);


		$this->lang->load('auth');
		$this->load->model('ion_auth_model');
	}

	function _remap($method)
	{
		if (method_exists($this, $method))
		{
			$this->$method();
		}
		else {
			$this->index();
		}
	}

	public function index()
	{
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		//list the users
		$this->data['users'] = $this->ion_auth_model->users()->result();

		//USAGE NOTE - you can do more complicated queries like this
		//$this->data['users'] = $this->ion_auth->where('field', 'value')->users()->result();

		foreach ($this->data['users'] as $k => $user)
		{
			$this->data['users'][$k]->groups = $this->ion_auth_model->get_users_groups($user->id)->result();
		}

		// redirect them to the login page
		$this->data['title'] = _('Users');
		$this->data['request_page'] = "admin/users";
		$js_files = array();
		$css_files = array();
		$this->enqueue->_render_page('backend/index', $this->data, false, $js_files, $css_files);
	}
}
