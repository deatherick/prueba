<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

/**
 * @property CI_Form_validation form_validation
 * @property array data
 * @property Ion_auth ion_auth
 * @property Ion_auth_model ion_auth_model
 * @property CI_Session|ArrayObject session
 * @property Enqueue enqueue
 * @property CI_Config config
 * @property CI_DB_query_builder db
 * @property Language|CI_Lang lang
 * @property CI_Input input
 * @property CI_Security security
 */
class ApiJwt extends REST_Controller
{
	/**
	 * Constantes de errores
	 */
	const NO_ERROR = 0;
	const ERROR_PARAMETER = -1;
	const ERROR_SECRET_KEY = -2;
	const ERROR_INCORRECT_LOGIN = -3;
	const ERROR_RETRIEVING_DATA = -4;
	const ERROR_UNEXPECTED_TOKEN = -5;
	const ERROR_INSERTING_DATA  = -6;
	const ERROR_UPDATING_DATA = -7;
	const ERROR_DELETING_DATA = -8;
	const ERROR_INCONSISTENT_INSERT = -9;
	const ERROR_LOGOUT = -10;
	const ERROR_UNREGISTERED_USER = -11;
	const ERROR_INEXISTENT_VIDEO = -12;
	const ERROR_INCORRECT_PASSWORD = -13;
	const ERROR_EDITING_PROFILE = -14;
	const ERROR_PERFORMING_ACTION = -15;
	const ERROR_CREDENTIALS_FORGOT = -16;
	const ERROR_SENDING_MESSAGE = -17;
	const ERROR_INEXISTENT_FILE = -18;
	const ERROR_INEXISTENT_LINK = -19;
	const ERROR_INVALID_KEY = -20;
	const ERROR_NO_USER = -21;

	/**
	 * Funcion que valida los parametros enviados por JSON
	 * @param $array
	 * @param $params
	 * @return bool
	 * @author Erick Morales
	 */
	public function verify_parameters($array, $params) {
		if(!is_array($array)){
			$array = (array)$array;
		}
		foreach($params as $param){
			if(!isset($param['optional']))
				if (!array_key_exists($param, $array)) {
					return false;
				}
		}
		return true;
	}

	/*public function __construct()
	{
		//parent::__construct();
		$this->load->model('ion_auth_model');
	}*/

	/*function _remap($method)
	{
		if (method_exists($this, $method))
		{
			$this->$method();
		}
		else {
			$this->index();
		}
	}*/

	public function index()
	{
		show_error('Silence is gold.');
	}

	/**
	 * URL: http://localhost/CodeIgniter-JWT-Sample/auth/token
	 * Method: GET
	 */
	public function token_get()
	{
		$tokenData = array();
		$tokenData['user_data'] = array('user'=> 'Erick Morales', 'code'=> 1);
		$tokenData['timestamp'] = now();
		$output['token'] = AUTHORIZATION::generateToken($tokenData);
		$this->set_response($output, REST_Controller::HTTP_OK);
	}
	/**
	 * URL: http://localhost/CodeIgniter-JWT-Sample/auth/token
	 * Method: POST
	 * Header Key: Authorization
	 * Value: Auth token generated in GET call
	 */
	public function token_post()
	{
		$headers = $this->input->request_headers();
		if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
			$decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);
			if ($decodedToken != false) {
				$this->set_response($decodedToken, REST_Controller::HTTP_OK);
				return;
			}
		}
		$this->set_response("Unauthorised", REST_Controller::HTTP_UNAUTHORIZED);
	}
}
