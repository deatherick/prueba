<?php

/**
 * @property CI_Form_validation form_validation
 * @property array data
 * @property Ion_auth ion_auth
 * @property Ion_auth_model ion_auth_model
 * @property CI_Session|ArrayObject session
 * @property Enqueue enqueue
 * @property CI_Config config
 * @property CI_DB_query_builder db
 * @property CI_Lang lang
 * @property CI_Input input
 * @property CI_Security security
 */
class Tools extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url', 'language']);


		$this->lang->load('auth');
		$this->load->model('ion_auth_model');
	}

	function _remap($method)
	{
		if (method_exists($this, $method))
		{
			$this->$method();
		}
		else {
			$this->index();
		}
	}

	public function index()
	{
		$this->search();
	}

	function search($filter){
		// set the flash data error message if there is one
		$this->data['message'] = '';

		// redirect them to the login page
		$this->data['title'] = _('Search');
		$this->data['request_page'] = "tools/search";
		$js_files = array();
		$css_files = array();
		$this->enqueue->_render_page('backend/index', $this->data, false, $js_files, $css_files);
	}
}
