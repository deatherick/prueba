<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

/**
 * @property CI_Form_validation form_validation
 * @property array data
 * @property Ion_auth ion_auth
 * @property Ion_auth_model ion_auth_model
 * @property CI_Session|ArrayObject session
 * @property Enqueue enqueue
 * @property CI_Config config
 * @property CI_DB_query_builder db
 * @property Language|CI_Lang lang
 * @property CI_Input input
 * @property CI_Security security
 * @property Request_model request_model
 */
class Api extends CI_Controller
{
	/**
	 * Constantes de errores
	 */
	const NO_ERROR = 0;
	const ERROR_PARAMETER = -1;
	const ERROR_SECRET_KEY = -2;
	const ERROR_INCORRECT_LOGIN = -3;
	const ERROR_RETRIEVING_DATA = -4;
	const ERROR_UNEXPECTED_TOKEN = -5;
	const ERROR_INSERTING_DATA  = -6;
	const ERROR_UPDATING_DATA = -7;
	const ERROR_DELETING_DATA = -8;
	const ERROR_INCONSISTENT_INSERT = -9;
	const ERROR_LOGOUT = -10;
	const ERROR_UNREGISTERED_USER = -11;
	const ERROR_INEXISTENT_VIDEO = -12;
	const ERROR_INCORRECT_PASSWORD = -13;
	const ERROR_EDITING_PROFILE = -14;
	const ERROR_PERFORMING_ACTION = -15;
	const ERROR_CREDENTIALS_FORGOT = -16;
	const ERROR_SENDING_MESSAGE = -17;
	const ERROR_INEXISTENT_FILE = -18;
	const ERROR_INEXISTENT_LINK = -19;
	const ERROR_INVALID_KEY = -20;
	const ERROR_NO_USER = -21;

	/**
	 * Funcion que valida los parametros enviados por JSON
	 * @param $array
	 * @param $params
	 * @return bool
	 * @author Erick Morales
	 */
	public function verify_parameters($array, $params) {
		if(!is_array($array)){
			$array = (array)$array;
		}
		foreach($params as $param){
			if(!isset($param['optional']))
				if (!array_key_exists($param, $array)) {
					return false;
				}
		}
		return true;
	}

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ion_auth_model');
	}

	function _remap($method)
	{
		if (method_exists($this, $method))
		{
			$this->$method();
		}
		else {
			$this->index();
		}
	}

	public function index()
	{
		show_error('Silence is gold.');
	}

	function health(){
		header('Content-Type: text/json; charset=utf-8');
		$data = json_decode(trim(file_get_contents('php://input')), true);

		$params = array(
			'secret_key',
			'login_key',
			'lang' => array('optional' => true)
		);

		if ($this->verify_parameters($data, $params)){
			$response = array(
				'error' => false,
				'code' => $this::NO_ERROR,
				'message' => _('Logged In Successfully'),
				'user' => 'Erick'
			);
		} else {
			$response = array(
				'error' => true,
				'code' => $this::ERROR_UNREGISTERED_USER,
				'message' => _('Please register and set a password first')
			);
		}

		echo json_encode($response);
	}

	function login(){
		header('Content-Type: text/json; charset=utf-8');
		$data = json_decode(trim(file_get_contents('php://input')), true);

		$params = array(
			'user',
			'pass',
			'lang' => array('optional' => true)
		);

		if ($this->verify_parameters($data, $params)){

			$username = $data['user'];
			$password = $data['pass'];
			$response = $this->ion_auth->performLoginRequest($username, $password);
			if ($response && isset($response->result)) {
				$user_data = $response->result;

				$additional_data = array(
					'first_name' => $user_data->eriGivenName,
					'last_name' => $user_data->eriSn,
					'phone' => $user_data->mobile,

				);
				$this->ion_auth_model->register($user_data->uid, 'password', $user_data->mail, $additional_data);

				$response = array(
					'error' => false,
					'code' => $this::NO_ERROR,
					'data' => $user_data,
					'message' => _('Logged In Successfully')
				);
			}
		} else {
			$response = array(
				'error' => true,
				'code' => $this::ERROR_PARAMETER,
				'message' => _('Error in Informed Data')
			);
		}

		echo json_encode($response);
	}

	function create_request(){
		header('Content-Type: text/json; charset=utf-8');
		$data = json_decode(trim(file_get_contents('php://input')), true);

		$params = array(
			'title',
			'request_type',
			'total_days',
			'calendar',
			'observations',
			'created_by',
			'lang' => array('optional' => true)
		);

		if ($this->verify_parameters($data, $params)) {
			$inserted_response = $this->request_model->insert(
				$data['title'],
				$data['request_type'],
				$data['total_days'],
				json_encode($data['calendar']),
				$data['observations'],
				$data['created_by']
			);
			if ($inserted_response) {
				$data['id'] = $inserted_response;
				$response = array(
					'error' => false,
					'code' => $this::NO_ERROR,
					'message' => _('Request created Successfully'),
					'data' => $data
				);
			} else {
				$response = array(
					'error' => true,
					'code' => $this::ERROR_INCORRECT_LOGIN,
					'message' => _('Error creating request')
				);
			}
		} else {
			$response = array(
				'error' => true,
				'code' => $this::ERROR_PARAMETER,
				'message' => _('Error in Informed Data')
			);
		}

		echo json_encode($response);
	}

	function get_requests(){
		header('Content-Type: text/json; charset=utf-8');
		$data = json_decode(trim(file_get_contents('php://input')), true);

		$params = array(
			'username',
			'lang' => array('optional' => true)
		);

		if ($this->verify_parameters($data, $params)){
			$requests = $this->request_model->getByUsername($data['username']);
			foreach ($requests as $request){
				$request->calendar = json_decode($request->calendar);
			}

			$response = array(
				'error' => false,
				'code' => $this::NO_ERROR,
				'message' => _('Requests consulted Successfully'),
				'data' => $requests
			);
		} else {
			$response = array(
				'error' => true,
				'code' => $this::ERROR_PARAMETER,
				'message' => _('Error in Informed Data')
			);
		}

		echo json_encode($response);
	}

	function get_request_history(){
		header('Content-Type: text/json; charset=utf-8');
		$data = json_decode(trim(file_get_contents('php://input')), true);

		$params = array(
			'username',
			'request_id',
			'lang' => array('optional' => true)
		);

		if ($this->verify_parameters($data, $params)){
			$request = $this->request_model->getById_API($data['username'], intval($data['request_id']))->row();
			if($request){
				$request->calendar = json_decode($request->calendar);

				$request_history = $this->request_model->getRequestHistory($data['username'], $data['request_id']);
				$request->history = $request_history;
				$response = array(
					'error' => false,
					'code' => $this::NO_ERROR,
					'message' => _('Request history consulted Successfully'),
					'data' => $request
				);
			} else {
				$response = array(
					'error' => true,
					'code' => $this::ERROR_RETRIEVING_DATA,
					'message' => _('Error retrieving the data')
				);
			}
		} else {
			$response = array(
				'error' => true,
				'code' => $this::ERROR_PARAMETER,
				'message' => _('Error in Informed Data')
			);
		}

		echo json_encode($response);
	}

	function approve_reject_request(){
		header('Content-Type: text/json; charset=utf-8');
		$data = json_decode(trim(file_get_contents('php://input')), true);

		$params = array(
			'username',
			'request_id',
			'observations',
			'status',
			'lang' => array('optional' => true)
		);

		if ($this->verify_parameters($data, $params)){
			if(!in_array($data['status'], array('Approved', 'Rejected'))){
				$response = array(
					'error' => true,
					'code' => $this::ERROR_UPDATING_DATA,
					'message' => _('Error only Approved o Rejected values are allowed')
				);
				echo json_encode($response);
				return;
			}
			if($this->request_model->validate_approved_rejected($data['request_id'])){
				if($this->request_model->approve_or_reject($data['request_id'], $data['status'], $data['observations'], $data['username'])){
					$response = array(
						'error' => false,
						'code' => $this::NO_ERROR,
						'message' => _('Request '.$data['status'].' Successfully')
					);
				} else {
					$response = array(
						'error' => true,
						'code' => $this::ERROR_UPDATING_DATA,
						'message' => _('Error approving or rejecting your request')
					);
				}
			} else {
				$response = array(
					'error' => true,
					'code' => $this::ERROR_UPDATING_DATA,
					'message' => _('Error request already approved or rejected')
				);
			}

		} else {
			$response = array(
				'error' => true,
				'code' => $this::ERROR_PARAMETER,
				'message' => _('Error in Informed Data')
			);
		}
		echo json_encode($response);
	}
}
