<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property CI_Form_validation form_validation
 * @property array data
 * @property Ion_auth ion_auth
 * @property Ion_auth_model ion_auth_model
 * @property CI_Session|ArrayObject session
 * @property Enqueue enqueue
 * @property CI_Config config
 * @property CI_DB_query_builder db
 * @property CI_Lang lang
 * @property CI_Input input
 * @property CI_Security security
 * @property Request_model request_model
 */
class Manage extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);
		$this->lang->load('auth');
		$this->load->model('ion_auth_model');
	}

	public function index(){
		show_error('Silence is gold.');
	}

	public function requests()
	{
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		//list the requests
		$this->data['requests'] = $this->request_model->get()->result();

		// redirect them to the page
		$this->data['title'] = _('Requests');
		$this->data['request_page'] = "manage/requests";
		$js_files = array();
		$css_files = array();
		$this->enqueue->_render_page('backend/index', $this->data, false, $js_files, $css_files);
	}

	public function request($id = 0) {

		// validate form input
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('observations', 'Observations', 'required');
		$this->form_validation->set_rules('calendar', 'Calendar', 'required');

		if ($this->form_validation->run() === TRUE)
		{
			$calendar = explode(', ', $this->input->post('calendar'));
			$data['days'] = $calendar;
			$request_id = $this->request_model->insert(
				$this->input->post('title'),
				'1', //TODO: Vacations
				count($calendar),
				json_encode($data),
				$this->input->post('observations'),
				$this->session->userdata('username')
			);

			if($request_id){
				$this->data['message'] = 'Request created successfully';
				redirect('manage/request/'.$request_id, 'refresh');
			} else {
				$this->data['request'] = array();
				$this->data['message'] = 'Error creating your request';
			}
		} else {
			if ($id != "") {
				$request = $this->request_model->getById(intval($id))->row();
				if($request == null){
					redirect('manage/requests', 'refresh');
					return;
				}
				$this->data['request'] = $request;
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			} else {
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->data['request'] = array();
			}
		}

		// redirect them to the page
		$this->data['title'] = _('Create/Edit Request');
		$this->data['request_page'] = "manage/request_form";
		$this->data['pending_days'] = 15; //TODO: Buscar de datos de usuario
		$this->data['disabled'] = isset($request->status) && $request->status != "Created";

		$js_files = array(
			'plugins/air-datepicker/datepicker.min.js',
			'plugins/air-datepicker/i18n/datepicker.en.js'
		);
		$css_files = array(
			'plugins/air-datepicker/datepicker.min.css'
		);
		$this->enqueue->_render_page('backend/index', $this->data, false, $js_files, $css_files);
	}

	public function reject(){
		echo json_encode($this->approve_reject('Rejected'));
	}

	public function accept(){
		echo json_encode($this->approve_reject('Approved'));
	}

	private function approve_reject($status) {
		// validate form input
		$this->form_validation->set_rules('request_id', 'ID', 'required');
		$this->form_validation->set_rules('observations', 'Observations', 'required');

		if ($this->form_validation->run() === TRUE) {
			if($this->request_model->validate_approved_rejected($this->input->post('request_id'))){
				if($this->request_model->approve_or_reject($this->input->post('request_id'), $status, $this->input->post('observations'), $this->session->userdata('username'))){
					$response = array(
						'error' => false,
						'message' => _('Request '.$status.' Successfully')
					);
				} else {
					$response = array(
						'error' => true,
						'message' => _('Error approving/rejecting your request')
					);
				}
			} else {
				$response = array(
					'error' => true,
					'message' => sprintf(_('The request is already %s'), $status)
				);
			}
		} else {
			$response = array(
				'error' => true,
				'message' => _('Error in Informed Data')
			);
		}

		return $response;
	}

	public function profile()
	{
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		// redirect them to the login page
		$this->data['title'] = _('Profile');
		$this->data['request_page'] = "manage/profile";
		$js_files = array();
		$css_files = array();
		$this->enqueue->_render_page('backend/index', $this->data, false, $js_files, $css_files);
	}
}
