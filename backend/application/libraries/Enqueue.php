<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Enqueue
{

    public $scripts;
    public $styles;
    public $path_js;
    public $path_css;
    private $asset_root;
    private $obj;

    public function __construct($asset_root = 'assets', $path_js = 'js', $path_css = 'css')
    {
        $this->obj = &get_instance();
        $base_url = $this->obj->config->item('base_url');
        $this->scripts = array();
        $this->styles = array();
        $this->path_js = $base_url . DIRECTORY_SEPARATOR . $asset_root . DIRECTORY_SEPARATOR . $path_js . DIRECTORY_SEPARATOR;
        $this->path_css = $base_url . DIRECTORY_SEPARATOR . $asset_root . DIRECTORY_SEPARATOR . $path_css . DIRECTORY_SEPARATOR;
    }

    public function enqueue_js($file, $ver = false, $async = false, $in_footer = true)
    {
        $this->scripts[] = array("file" => (string)$file, "in_footer" => $in_footer, "ver" => $ver, "async" => $async);
    }

    public function enqueue_css($file, $ver = false, $media = "all")
    {
        $this->styles[] = array("file" => (string)$file, "media" => (string)$media, "ver" => $ver);
    }

    public function load_files_header()
    {
        $enqueued = "";
        foreach ($this->styles as $style) {
            $file = $this->path_css . $style["file"];
            $enqueued .= '<link rel="stylesheet" type="text/css" media="' . $style["media"] . '" href="' . $file . ($style["ver"] ? "?ver=" . sha1(file_get_contents($this->path_css . $style["file"])) : '') . '" />' . "\n";
        }
        foreach ($this->scripts as $script) {
            if (!$script["in_footer"]) {
                $file = $this->path_js . $script["file"];
                $enqueued .= '<script src="' . $file . ($style["ver"] ? "?ver=" . sha1(file_get_contents($this->path_js . $script["file"])) : '') . '" ' . ($script["async"] ? 'async' : '') . '></script>' . "\n";
            }
        }
        echo $enqueued;
    }

    public function load_files_footer()
    {
        $enqueued = "";
        foreach ($this->scripts as $script) {
            if ($script["in_footer"]) {
                $file = $this->path_js . $script["file"];
                $enqueued .= '<script src="' . $file . ($script["ver"] ? "?ver=" . sha1(file_get_contents($this->path_js . $script["file"])) : '') . '" ' . ($script["async"] ? 'async' : '') . '></script>' . "\n";
            }
        }
        echo $enqueued;
    }

    function _render_page($view, $data = null, $returnhtml = false, $js_files = array(), $css_files = array())
    {
        $this->viewdata = (empty($data)) ? $this->data : $data;

        /* GLOBAL CSS */
        //BEGIN GLOBAL MANDATORY STYLES

        //END GLOBAL MANDATORY STYLES
        //BEGIN PAGE LEVEL PLUGINS
        foreach ($css_files as $css_file) {
            $this->enqueue_css($css_file);
        }
        //END PAGE LEVEL PLUGINS
        //BEGIN THEME GLOBAL STYLES

        //BEGIN THEME LAYOUT STYLES

        //END THEME LAYOUT STYLES
        /* GLOBAL CSS */

        /* GLOBAL JS */
        //BEGIN CORE PLUGINS

        //END CORE PLUGINS
        //BEGIN PAGE LEVEL PLUGINS

        //END PAGE LEVEL PLUGINS
        //BEGIN THEME GLOBAL SCRIPTS

        //END THEME GLOBAL SCRIPTS
        //BEGIN PAGE LEVEL SCRIPTS
        foreach ($js_files as $js_file) {
            $this->enqueue_js($js_file);
        }
        //END PAGE LEVEL SCRIPTS
        //BEGIN THEME LAYOUT SCRIPTS

        //END THEME LAYOUT SCRIPTS
        /* GLOBAL JS */

        $view_html = $this->obj->load->view($view, $this->viewdata, $returnhtml);

        if ($returnhtml) return $view_html;
    }
}
