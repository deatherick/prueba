<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Renderer
{
  function _render_page($view, $data=null, $returnhtml=false, $js_files = array(), $css_files = array())
  {
    $CI =& get_instance();
    $viewdata = (empty($data)) ? $this->data: $data;
    /* GLOBAL CSS */
    //BEGIN GLOBAL MANDATORY STYLES
    $CI->$enqueue->enqueue_css('global/plugins/font-awesome/css/font-awesome.min.css');
    $CI->$enqueue->enqueue_css('global/plugins/simple-line-icons/simple-line-icons.min.css');
    $CI->$enqueue->enqueue_css('global/plugins/bootstrap/css/bootstrap.min.css');
    $CI->$enqueue->enqueue_css('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
    //END GLOBAL MANDATORY STYLES
    //BEGIN PAGE LEVEL PLUGINS
    $CI->$enqueue->enqueue_css('global/plugins/bootstrap-daterangepicker/daterangepicker.min.css');
    $CI->$enqueue->enqueue_css('global/plugins/morris/morris.css');
    $CI->$enqueue->enqueue_css('global/plugins/fullcalendar/fullcalendar.min.css');
    $CI->$enqueue->enqueue_css('global/plugins/jqvmap/jqvmap/jqvmap.css');
    //END PAGE LEVEL PLUGINS
    //BEGIN THEME GLOBAL STYLES
    $CI->$enqueue->enqueue_css('global/css/components-md.min.css');
    $CI->$enqueue->enqueue_css('global/css/plugins-md.min.css');
    //BEGIN THEME LAYOUT STYLES
    $CI->$enqueue->enqueue_css('layouts/layout/css/layout.min.css');
    $CI->$enqueue->enqueue_css('layouts/layout/css/themes/darkblue.min.css');
    $CI->$enqueue->enqueue_css('layouts/layout/css/custom.min.css');
    //END THEME LAYOUT STYLES
    /* GLOBAL CSS */
    /* GLOBAL JS */
    //BEGIN CORE PLUGINS
    $CI->$enqueue->enqueue_js('global/plugins/jquery.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/bootstrap/js/bootstrap.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/js.cookie.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/jquery.blockui.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
    //END CORE PLUGINS
    //BEGIN PAGE LEVEL PLUGINS
    $CI->$enqueue->enqueue_js('global/plugins/moment.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/bootstrap-daterangepicker/daterangepicker.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/morris/morris.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/morris/raphael-min.js');
    $CI->$enqueue->enqueue_js('global/plugins/counterup/jquery.waypoints.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/counterup/jquery.counterup.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/amcharts/amcharts/amcharts.js');
    $CI->$enqueue->enqueue_js('global/plugins/amcharts/amcharts/serial.js');
    $CI->$enqueue->enqueue_js('global/plugins/amcharts/amcharts/pie.js');
    $CI->$enqueue->enqueue_js('global/plugins/amcharts/amcharts/radar.js');
    $CI->$enqueue->enqueue_js('global/plugins/amcharts/amcharts/themes/light.js');
    $CI->$enqueue->enqueue_js('global/plugins/amcharts/amcharts/themes/patterns.js');
    $CI->$enqueue->enqueue_js('global/plugins/amcharts/amcharts/themes/chalk.js');
    $CI->$enqueue->enqueue_js('global/plugins/amcharts/ammap/ammap.js');
    $CI->$enqueue->enqueue_js('global/plugins/amcharts/ammap/maps/js/worldLow.js');
    $CI->$enqueue->enqueue_js('global/plugins/amcharts/amstockcharts/amstock.js');
    $CI->$enqueue->enqueue_js('global/plugins/fullcalendar/fullcalendar.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/horizontal-timeline/horozontal-timeline.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/flot/jquery.flot.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/flot/jquery.flot.resize.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/flot/jquery.flot.categories.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/jquery-easypiechart/jquery.easypiechart.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/jquery.sparkline.min.js');
    $CI->$enqueue->enqueue_js('global/plugins/jqvmap/jqvmap/jquery.vmap.js');
    $CI->$enqueue->enqueue_js('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js');
    $CI->$enqueue->enqueue_js('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js');
    $CI->$enqueue->enqueue_js('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js');
    $CI->$enqueue->enqueue_js('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js');
    $CI->$enqueue->enqueue_js('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js');
    $CI->$enqueue->enqueue_js('global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js');
    //END PAGE LEVEL PLUGINS
    //BEGIN THEME GLOBAL SCRIPTS
    $CI->$enqueue->enqueue_js('global/scripts/app.min.js');
    //END THEME GLOBAL SCRIPTS
    //BEGIN PAGE LEVEL SCRIPTS
    $CI->$enqueue->enqueue_js('pages/scripts/dashboard.min.js');
    //END PAGE LEVEL SCRIPTS
    //BEGIN THEME LAYOUT SCRIPTS
    $CI->$enqueue->enqueue_js('layouts/layout/scripts/layout.min.js');
    $CI->$enqueue->enqueue_js('layouts/layout/scripts/demo.min.js');
    $CI->$enqueue->enqueue_js('layouts/global/scripts/quick-sidebar.min.js');
    //END THEME LAYOUT SCRIPTS
    /* GLOBAL JS */
    $view_html = $CI->load->view($view, $viewdata, $returnhtml);

    if ($returnhtml) return $view_html;
  }
}
