/*
 *
 * login-register modal
 * Autor: Creative Tim
 * Web-autor: creative.tim
 * Web script: http://creative-tim.com
 * 
 */
function shakeError(){
	if($('.error').html() !== ''){
		$('#loginModal .modal-dialog').addClass('shake');
		$('.error').addClass('alert alert-danger');
		$('input[type="password"]').val('');
		setTimeout( function(){
			$('#loginModal .modal-dialog').removeClass('shake');
		}, 1000 );
	}
}

$( document ).ready(function() {
	shakeError();
});
